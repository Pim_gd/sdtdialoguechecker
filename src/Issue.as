package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class Issue {
		private var severity:Severity; //See the Severity Enum Class for more details. 
		private var snippet:String;
		private var message:String;
		private var type:String;
		private var relatedLine:Line;
		
		public function Issue(issueSeverity:Severity, concernedLine:Line, issueSnippet:String, issueMessage:String, issueType:String) {
			severity = issueSeverity;
			relatedLine = concernedLine;
			message = issueMessage;
			type = issueType;
			snippet = issueSnippet;
		}
		
		//No print function needed, since the DialogueChecker class is responsible for translating issues to text.
		//For tracing, however, use the following:
		//severity.getID()+type+" Issue relating to Line "+getRelatedLineNumber()+"\n"+"Line "+getRelatedLineNumber()+": "+relatedLine.getLineAsString();
		
		public function getRelatedLineNumber():uint {
			return relatedLine.getLineNumber();
		}
		
		public function getSeverity():Severity {
			return severity;
		}
		
		public function getSnippet():String {
			return snippet;
		}
		
		public function getType():String {
			return type;
		}
		
		public function getMessage():String {
			return message;
		}
		
		public function printMessage():String {
			return type + " - " + severity.getIDStandardCapitalization() + ": " + message;
		}
		
		/**
		 * For tracing purposes. It works, but it's not nice.
		 * @return output regarding the issue.
		 */
		public function _debugPrint():String {
			return severity.getID() + " " + type + " Issue relating to Line " + getRelatedLineNumber() + "\n" + "Line " + getRelatedLineNumber() + ": " + relatedLine.getLineAsString() + "\nIssue entails: " + message;
		}
	}

}