package {
	
	/**
	 * Represents a single line in a Dialogue for SDT.
	 * Example:
	 * intro:"hi!". {"style":"Him"}
	 * - linenumber would be the line's linenumber
	 * - name would be "intro"
	 * - content would be "hi!"
	 * - excess would be "", ". ", ""
	 * - lineattributes would be an array of one, containing a LineAttribute object, name "style", value "Him"
	 * - lineattriubtesString would be "{"style":"Him"}"
	 * @author Pimgd
	 */
	public class Line {
		/**
		 * Normally, variables are not named by classVariableName, since that's unnecessary (e.g. lineWidth, lineLength).
		 * In this case, however, lineNumber references not to the number of the line but the actual "linenumber" instead.
		 */
		private var lineNumber:uint;
		
		/**
		 * The name of the line, the trigger.
		 */
		private var name:String;
		
		/**
		 * The actual content of the line. Everything between the two double quotes.
		 */
		private var content:Array;
		
		private var raw:String;
		
		private var contentString:String;
		
		/**
		 * The remaining text...
		 * Index 0: After the lineName, but before the content.
		 * Index 1: After the content, prior to the lineAttributes.
		 * Index 2: After the lineAttributes.
		 * For checkers: calling trim on this should give an empty string as result.
		 */
		private var excess:Array;
		
		/**
		 * An Array of LineAttribute.
		 */
		private var attributes:Array;
		
		private var attributesByString:Array;
		
		private var attributesString:String;
		
		private var commentLine:Boolean;
		
		public function Line(number:uint, lineName:String, rawLine:String, lineContent:Array, lineContentString:String, lineExcess:Array, lineAttributes:Array, lineAttributesString:String) {
			lineNumber = number;
			name = lineName;
			content = lineContent;
			excess = lineExcess;
			attributes = lineAttributes;
			attributesString = lineAttributesString;
			raw = rawLine;
			contentString = lineContentString;
			commentLine = determineCommentLine();
			setupAttributesViaStringIndex();
		}
		
		private function setupAttributesViaStringIndex():void {
			attributesByString = new Array();
			for (var i:uint = 0, isize:uint = attributes.length; i < isize; i++) {
				attributesByString[attributes[i].getName()] = attributes[i];
			}
		}
		
		private function determineCommentLine():Boolean {
			if (raw.indexOf("-") == 0) {
				return true;
			}
			var quoteIndex:int = raw.indexOf("\"");
			if (quoteIndex == -1) {
				return true;
			}
			var thirdSpaceIndex:int = StringFunctions.nthIndexOf(3, raw, " ");
			return thirdSpaceIndex != -1 && (thirdSpaceIndex < quoteIndex);
		}
		
		public function getLineNumber():uint {
			return lineNumber;
		}
		
		public function getLineName():String {
			return name;
		}
		
		public function getExcess():Array {
			return excess;
		}
		
		public function getContent():Array {
			return content;
		}
		
		public function getRaw():String {
			return raw;
		}
		
		public function getLineAttributes():Array {
			return attributes;
		}
		
		public function getLineAttributesRaw():String {
			return attributesString;
		}
		
		public function getLineAttribute(name:String):LineAttribute {
			return attributesByString[name];
		}
		
		public function hasLineAttributes():Boolean {
			if (attributes != null) {
				return attributes.length != 0;
			}
			return false;
		}
		
		public function isIndexInSubstitution(index:uint):Boolean {
			var contentIndex:int = raw.indexOf(contentString);
			if ((contentIndex + contentString.length) > index && index > name.length) {
				var preString:String = contentString.substring(0, index - contentIndex);
				return StringFunctions.countOccurances(preString, "*")%2 == 1;
			}
			return false;
		}
		
		public function isCommentLine():Boolean {
			return commentLine;
		}
		
		public function printLineMessage(lineBreak:String):String {
			return "Line " + getLineNumber() + ": " + getRaw() + lineBreak;
		}
		
		public function getTriggers():Array {
			var triggers:Array = new Array();
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				if (content[i] is Trigger) {
					triggers.push(content[i]);
				}
			}
			return triggers;
		}
		
		public function getLineStripped(delay:Boolean, triggers:Boolean, substitutions:Boolean):String {
			var result:String = "";
			result += getLineName() + ":" + getExcess()[0];
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var arrObject:Object = content[i];
				if (arrObject is String) {
					var stringObject:String = arrObject as String;
					if (!delay || stringObject.indexOf("Б") == -1) { //(stripDelay && no delay chars) || !stripDelay ~ (p && q) || !p ~ !p || q
						result += arrObject;
					}
				} else if (arrObject is LineElement) {
					var lineElement:LineElement = arrObject as LineElement;
					if (lineElement is Trigger) {
						var contentTrigger:Trigger = lineElement as Trigger;
						if (!triggers) {
							result += contentTrigger.getLineSegment();
						}
					} else if (lineElement is VariableInsertion) {
						var contentVariableInsertion:VariableInsertion = lineElement as VariableInsertion;
						result += contentVariableInsertion.getLineSegment();
					} else if (lineElement is VariableSubstitution) {
						var contentVariableSubstitution:VariableSubstitution = lineElement as VariableSubstitution;
						if (!substitutions || contentVariableSubstitution.getLineSegment() == "*FINISHES*") {
							result += contentVariableSubstitution.getLineSegment();
						}
					} else {
						result += lineElement.getLineSegment();
					}
				}
			}
			result += getExcess()[1];
			if (attributes != null && attributes.length > 0) {
				result += "{";
				var j:uint = 0;
				for (var s:String in attributes) {
					var arrAttribute:Object = attributes[s];
					if (arrAttribute is LineAttribute) {
						var attribute:LineAttribute = arrAttribute as LineAttribute;
						if (j != 0) {
							result += ",";
						}
						result += attribute.getLineSegmentAsParsed();
					} else {
						//what...?
						ErrorManager.showDevelopmentError("Line::getLineStripped(delay:Boolean, triggers:Boolean, substitutions:Boolean):String - attribute[" + j + "] is not a LineAttribute");
					}
					j++;
				}
				result += "}" + getExcess()[2];
			}
			return result;
		}
		
		public function getLineStrippedLiteralSubs(delay:Boolean, triggers:Boolean):String {
			var result:String = "";
			result += getLineName() + ":" + getExcess()[0];
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var arrObject:Object = content[i];
				if (arrObject is String) {
					var stringObject:String = arrObject as String;
					if (!delay || stringObject.indexOf("Б") == -1) { //(stripDelay && no delay chars) || !stripDelay ~ (p && q) || !p ~ !p || q
						result += arrObject;
					}
				} else if (arrObject is LineElement) {
					var lineElement:LineElement = arrObject as LineElement;
					if (lineElement is Trigger) {
						var contentTrigger:Trigger = lineElement as Trigger;
						if (!triggers) {
							result += contentTrigger.getLineSegment();
						}
					} else if (lineElement is VariableInsertion) {
						var contentVariableInsertion:VariableInsertion = lineElement as VariableInsertion;
						result += contentVariableInsertion.getLineSegment();
					} else if (lineElement is VariableSubstitution) {
						var contentVariableSubstitution:VariableSubstitution = lineElement as VariableSubstitution;
						if (contentVariableSubstitution.getLineSegment() == "*FINISHES*") {
							result += contentVariableSubstitution.getLineSegment();
						} else {
							result += contentVariableSubstitution.getRawContent();
						}
					} else {
						result += lineElement.getLineSegment();
					}
				}
			}
			result += getExcess()[1];
			if (attributes != null && attributes.length > 0) {
				result += "{";
				var j:uint = 0;
				for (var s:String in attributes) {
					var arrAttribute:Object = attributes[s];
					if (arrAttribute is LineAttribute) {
						var attribute:LineAttribute = arrAttribute as LineAttribute;
						if (j != 0) {
							result += ",";
						}
						result += attribute.getLineSegmentAsParsed();
					} else {
						//what...?
						ErrorManager.showDevelopmentError("Line::getLineStripped(delay:Boolean, triggers:Boolean, substitutions:Boolean):String - attribute[" + j + "] is not a LineAttribute");
					}
					j++;
				}
				result += "}" + getExcess()[2];
			}
			return result;
		}
		
		public function getLineAsString():String {
			var result:String = "";
			result += getLineName() + ":" + getExcess()[0] + "\"";
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var arrObject:Object = content[i];
				if (arrObject is String) {
					result += arrObject;
				} else if (arrObject is LineElement) {
					var lineElement:LineElement = arrObject as LineElement;
					result += lineElement.getLineSegment();
				}
			}
			result += "\"" + getExcess()[1];
			if (attributes != null && attributes.length > 0) {
				result += "{";
				for (var j:uint = 0, jsize:uint = attributes.length; j < jsize; j++) {
					var arrAttribute:Object = attributes[i];
					if (arrAttribute is LineAttribute) {
						var attribute:LineAttribute = arrAttribute as LineAttribute;
						if (j != 0) {
							result += ",";
						}
						result += attribute.getLineSegment();
					} else {
						//what...?
						ErrorManager.showDevelopmentError("Line::getLineAsString() - attribute[" + j + "] is not a LineAttribute");
					}
				}
				result += "}" + getExcess()[2];
			}
			return result;
		}
		
		public function getLineAsStringWithExcessTrimmed():String {
			var result:String = "";
			result += getLineName() + ":" + (getExcess()[0].replace(/^\s+|\s+$/g, '')) + "\"";
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var arrObject:Object = content[i];
				if (arrObject is String) {
					result += arrObject;
				} else if (arrObject is LineElement) {
					var lineElement:LineElement = arrObject as LineElement;
					result += lineElement.getLineSegment();
				}
			}
			result += "\"" + getExcess()[1].replace(/^\s+|\s+$/g, '');
			if (attributes != null && attributes.length > 0) {
				result += "{";
				for (var j:uint = 0, jsize:uint = attributes.length; j < jsize; j++) {
					var arrAttribute:Object = attributes[i];
					if (arrAttribute is LineAttribute) {
						var attribute:LineAttribute = arrAttribute as LineAttribute;
						if (j != 0) {
							result += ",";
						}
						result += attribute.getLineSegment();
					} else {
						//what...?
						ErrorManager.showDevelopmentError("Line::getLineAsString() - attribute[" + j + "] is not a LineAttribute");
					}
				}
				result += "}" + getExcess()[2].replace(/^\s+|\s+$/g, '');
			}
			return result;
		}
	}

}