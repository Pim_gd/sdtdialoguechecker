package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class SyntaxChecker extends Checker {
		
		public function SyntaxChecker(dc:DialogueChecker) {
			super(dc);
		}
		
		public override function getName():String {
			return "SyntaxChecker";
		}
		
		public override function checkDialogue(dialogue:Dialogue, options:Options):void {
			var lines:Array = dialogue.getLines();
			for each (var line:Line in lines) {
				checkLineOldStyle(line, dialogue, options);
				checkLineNewStyle(line, dialogue, options);
			}
		}
		
		private function checkLineNewStyle(lineObject:Line, dialogue:Dialogue = null, options:Options = null):void {
			if (lineObject.isCommentLine()) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			
			var line:String = lineObject.getRaw();
			var linebreak:String = Options.DEFAULT_LINEBREAK;
			if (options != null) {
				linebreak = options.getLineBreak();
			}
			var issue:Issue = null;
			var issueMessage:String = "";
			var issueSnippet:String = "";
			
			var lineName:String = lineObject.getLineName();
			
			//Finish lines can't have line attributes
			if (lineObject.hasLineAttributes()) {
				if (lineName.indexOf("finish") == 0) {//is a finish line and has lineattributes
					issueSnippet = StringFunctions.getSnippet(line, "{");
					issueMessage += "Usage of lineattributes on a finish line on line " + lineObject.getLineNumber() + ", col " + (line.indexOf("{")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Defining lineattributes for finish lines causes them not to be loaded by SDT." + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				} else if (lineName.indexOf("finish") != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "{");
					issueMessage += "A line's name contains \"finish\" and has line-attributes - line " + lineObject.getLineNumber() + ", col " + (line.indexOf("{")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Lines of which the name contains \"finish\" are not loaded by SDT if they have line-attributes." + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				
				if (lineName == "interrupt") { //is an interrupt line and has lineattributes
					issueSnippet = StringFunctions.getSnippet(line, "{");
					issueMessage += "Usage of lineattributes on an interrupt line on line " + lineObject.getLineNumber() + ", col " + (line.indexOf("{")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Defining lineattributes for interrupt lines does not work. There is no known workaround as of yet." + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			if (line.indexOf("{") != -1 && line.indexOf("{") < line.indexOf(":")) { //{ before :
				var previousLine:Line = dialogue.getLineByLineNumber(lineObject.getLineNumber() - 1);
				if (previousLine != null && !previousLine.hasLineAttributes()) {
					issueMessage += "Misplaced lineattributes on line " + lineObject.getLineNumber() + "." + linebreak;
					issueMessage += "The previous line doesn't have lineattributes, and this line seems to only consist out of line attributes." + linebreak;
					issueMessage += "Did you accidentally add a newline between the previous line's content and lineattributes?" + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssueAsFirstForReportedLine(issue);
					issueMessage = "";
				}
			}
			
			if (lineName != "finishOther" && StringFunctions.stringStartsWith(lineName, "finish")) {
				var strippedLineName:String = lineName.substr("finish".length);
				if (isNaN(Number(strippedLineName))) {
					issueSnippet = "";
					issueMessage += "A finish line's name does not end with a number or \"Other\" on line " + lineObject.getLineNumber() + "." + linebreak;
					issueMessage += "Lines that contain finish as part of their name are reserved by SDT to use for *FINISHES*. You should name them finish0, finish1, finish2... finishOther." + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				} else if (Number(strippedLineName) < 0) {
					issueSnippet = "";
					issueMessage += "A finish line's name DOES end with a number on line " + lineObject.getLineNumber() + ", but it's a negative number." + linebreak;
					issueMessage += "Lines that contain finish as part of their name are reserved by SDT to use for *FINISHES*. You should name them finish0, finish1, finish2... finishOther." + linebreak;
					issueMessage += "Since the lowest finish line that *FINISHES* can reach is finish0, lines with negative finish numbers will not get played." + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			if (TriggerType.identifyType(lineObject.getLineName()) != TriggerType.LINEREFERENCE) {
				issueSnippet = "";
				issueMessage += "A line has the same linename as a trigger defined by SDT or a mod, on line " + lineObject.getLineNumber() + "." + linebreak;
				issueMessage += "SDT will not play lines for which a trigger action is taken." + linebreak;
				issueMessage += "The checker reports this error as MAJOR because it cannot identify the location where you'd want to trigger this line but instead trigger the trigger." + linebreak;
				issueMessage += "If you can't edit the name of your line and must access it for some reason, you can do so by using the \"next\" lineattribute." + linebreak;
				issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			var currentMood:Mood = null;
			var checkedFor:Mood = null;
			var checkingAgainstMoodAttribute:Boolean = false;
			var moodAttribute:LineAttribute = lineObject.getLineAttribute("\"mood\"");
			if (moodAttribute != null) {
				checkedFor = Mood.fromMoodAttributeValue(moodAttribute.getValue() as String);
				if (checkedFor != null) { //else failed to identify mood value? handled in checkLineOldStyle
					currentMood = checkedFor;
					checkingAgainstMoodAttribute = true;
				}
			}
			
			var triggers:Array = lineObject.getTriggers();
			
			for (var i:uint = 0, isize:uint = triggers.length; i < isize; i++) {
				var trigger:Trigger = triggers[i] as Trigger;
				var setMood:Mood = Mood.fromTrigger(trigger);
				if (setMood != null) {
					if (setMood == currentMood) {
						issueSnippet = "";
						if (checkingAgainstMoodAttribute) {
							issueMessage = "Line " + lineObject.getLineNumber() + " checks for " + checkedFor.getIDStandardCapitalization() + " mood, but then also sets the mood to " + setMood.getIDStandardCapitalization() + " in the line itself." + linebreak;
							issueMessage += "It's impossible (except if the user manually changed it in the brief period in between the check and the trigger) for the mood to change in between the check and the set, so this trigger could be removed." + linebreak;
						} else {
							issueMessage = "Line " + lineObject.getLineNumber() + " sets the mood to " + setMood.getIDStandardCapitalization() + " multiple times in a row." + linebreak;
							issueMessage += "Repeated setting of the mood to the same value has no result, and thus the trigger can be removed." + linebreak;
						}
						issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
						issue = new Issue(Severity.MINOR, lineObject, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
						checkingAgainstMoodAttribute = false;//flip flag to allow "repeated mood setting" issue to pop up
					}
					currentMood = setMood;
				}
			}
			
			var setAttribute:LineAttribute = lineObject.getLineAttribute("\"set\"");
			if (setAttribute != null) {
				var setAttributeValue:Object = setAttribute.getValue();
				if (setAttributeValue is Array) {
					for (var i:uint = 0, isize:uint = setAttributeValue.length; i < isize; i++) {
						var variableRef:VariableReference = setAttributeValue[i];
						var variableRefValue:Object = variableRef.getValue();
						
						if (variableRefValue == null) {
							issueMessage += "Failed to determine value for variable (" + variableRef.getName() + ") on line " + lineObject.getLineNumber() + "." + linebreak;
							issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
							continue;
						}
						var variableRefValueString:String = "" + variableRef;
						if (StringFunctions.isQuoted(variableRefValueString)) {
							var variableRefValueStripped:String = StringFunctions.stripQuotesIfNeeded(variableRefValueString);
							var variableRefValueAsNumber:Number = new Number(variableRefValueStripped);
							if (!isNaN(variableRefValueAsNumber) && variableRefValueStripped.indexOf("-") == -1 && variableRefValueStripped.indexOf("+") == -1 && variableRefValueStripped.length != 0) {
								//GOTCHA!
								issueSnippet = StringFunctions.getSnippet(line, "{");
								issueMessage += "Wrong format for numeric value (" + variableRefValueStripped + ") of attribute (" + variableRef.getName() + ") in set lineattribute on line " + lineObject.getLineNumber() + "." + linebreak;
								issueMessage += "SDT will ignore numeric values placed in strings." + linebreak;
								issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
								addIssue(issue);
								issueMessage = "";
							}
						}
					}
				} else if(setAttributeValue is String) {
					issueSnippet = StringFunctions.getSnippet(line, "\"set\":");
					issueMessage += "Wrong format for line attribute 'set' (" + setAttributeValue + ") on line " + lineObject.getLineNumber() + "." + linebreak;
					issueMessage += "Set and check lineattributes must be formatted like so: {\"set\":{\"key\":\"stringvalue\",\"otherKey\":numericValue}}." + linebreak;
					issueMessage += "Have you forgotten to add braces?" + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			var checkAttribute:LineAttribute = lineObject.getLineAttribute("\"check\"");
			if (checkAttribute != null) {
				var checkAttributeValue:Object = checkAttribute.getValue();
				if (checkAttributeValue is Array) {
					for (var i:uint = 0, isize:uint = checkAttributeValue.length; i < isize; i++) {
						var variableRef:VariableReference = checkAttributeValue[i];
						var variableRefvalue:* = variableRef.getValue();
						if (variableRefvalue == null) {
							issueSnippet = StringFunctions.getSnippet(line, variableRef.getName());
							issueMessage += "Failed to parse check lineattribute on line " + lineObject.getLineNumber() + "." + linebreak;
							issueMessage += "Error occured somewhere near (" + issueSnippet + "), specifically " + variableRef.getName() + "." + linebreak;
							issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
						} else if (StringFunctions.isQuoted(variableRefvalue)) {
							var variableRefValueStripped:String = StringFunctions.stripQuotesIfNeeded(variableRefvalue); //stripping ""
							var variableRefValueAsNumber:Number = new Number(variableRefValueStripped);
							if (!isNaN(variableRefValueAsNumber) && variableRefValueStripped.indexOf("-") == -1 && variableRefValueStripped.indexOf("+") == -1 && variableRefValueStripped.length != 0) {
								//GOTCHA!
								issueSnippet = StringFunctions.getSnippet(line, "{");
								issueMessage += "Wrong format for numeric value (" + variableRefValueStripped + ") of attribute (" + variableRef.getName() + ") in check lineattribute on line " + lineObject.getLineNumber() + "." + linebreak;
								issueMessage += "SDT will ignore numeric values placed in strings." + linebreak;
								issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
								addIssue(issue);
								issueMessage = "";
							}
						}
					}
				} else if(checkAttributeValue is String) {
					issueSnippet = StringFunctions.getSnippet(line, "\"check\":");
					issueMessage += "Wrong format for line attribute 'check' (" + checkAttributeValue + ") on line " + lineObject.getLineNumber() + "." + linebreak;
					issueMessage += "Set and check lineattributes must be formatted like so: {\"check\":{\"key\":\"stringvalue\",\"otherKey\":numericValue}}." + linebreak;
					issueMessage += "Have you forgotten to add braces?" + linebreak;
					issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			if (lineObject.getLineName() == "initial_settings") {
				var lineAttributes:Array = lineObject.getLineAttributes();
				for (var i:uint = 0, isize:uint = lineAttributes.length; i < isize; i++) {
					var lineAttribute:LineAttribute = lineAttributes[i];
					var lineAttributeValue:Object = lineAttribute.getValue();
					var lineAttributeValueString:String = "" + lineAttributeValue;
					if (StringFunctions.isQuoted(lineAttributeValueString)) {
						var lineAttributeValueStripped:String = StringFunctions.stripQuotesIfNeeded(lineAttributeValueString);
						var lineAttributeValueAsNumber:Number = new Number(lineAttributeValueStripped);
						if (!isNaN(lineAttributeValueAsNumber) && lineAttributeValueStripped.indexOf("-") == -1 && lineAttributeValueStripped.indexOf("+") == -1 && lineAttributeValueStripped.length != 0) {
							//GOTCHA!
							issueSnippet = StringFunctions.getSnippet(line, "{");
							issueMessage += "Wrong format for numeric value (" + lineAttributeValueStripped + ") of attribute (" + lineAttribute.getName() + ") for initial_settings on line " + lineObject.getLineNumber() + "." + linebreak;
							issueMessage += "SDT will ignore numeric values placed in strings." + linebreak;
							issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
						}
					}
				}
			}
			
			var lineContent:Array = lineObject.getContent();
			for (var i:uint = 0, isize:uint = lineContent.length; i < isize; i++) {
				if (lineContent[i] is String) {
					var hexString:String = "0123456789ABCDEFabcdef";
					
					if (lineContent[i].indexOf("finish") != -1 && lineObject.hasLineAttributes()) {
						issueSnippet = StringFunctions.getSnippet(line, "finish");
						issueMessage += "A line with lineattributes contains the string literal \"finish\" as part of the content, on line " + lineObject.getLineNumber() + "." + linebreak;
						issueMessage += "SDT will ignore lines that have finish in them, and also have line attributes." + linebreak;
						issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
					if (lineContent[i].indexOf("%") == 0 && !(lineContent[i].length >= 3 && hexString.indexOf(lineContent[i].charAt(1)) != -1 && hexString.indexOf(lineContent[i].charAt(2)) != -1)) {
						//issue is major if it could be a percent encoded value, but one of the two fields is not a percent encoded bit.
						var isMajor:Boolean = lineContent[i].length >= 3 && (hexString.indexOf(lineContent[i].charAt(1)) != -1 || hexString.indexOf(lineContent[i].charAt(2)) != -1);
						issueSnippet = StringFunctions.getSnippet(line, "%");
						issueMessage += "A line contains a percent character, but the value after the percent (" + lineContent[i].substring(1, Math.min(3, lineContent[i].length)) + ") can't be parsed as a percent encoded value, on line " + lineObject.getLineNumber() + ", near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += "If you want to use a simple percent character in your dialogue, use %25 to create a percent character." + linebreak;
						issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
						issue = new Issue(isMajor ? Severity.MAJOR : Severity.MINOR, lineObject, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				} else if (lineContent[i] is Trigger) {
					var contentTrigger:Trigger = lineContent[i] as Trigger;
					if (contentTrigger.getType() == TriggerType.LINEREFERENCE) {
						var insertionStripped:String = contentTrigger.getContentWithInsertionsReplaced("");
						if (insertionStripped.indexOf(".") != -1 || insertionStripped.indexOf(",") != -1 || insertionStripped.indexOf("!") != -1 || insertionStripped.indexOf("?") != -1) {
							issueSnippet = StringFunctions.getSnippet(line, contentTrigger.getLineSegment());
							issueMessage += "A line-reference trigger (" + contentTrigger.getLineSegment() + ") contains punctuation such as \"!\", \"?\", \".\" or \",\" in the plaintext part of the trigger, on line " + lineObject.getLineNumber() + ", col " + (line.indexOf(contentTrigger.getLineSegment())) + ", near \"" + issueSnippet + "\"" + linebreak;
							issueMessage += "Triggers that have punctuation in them do not work. Use the next lineattribute instead, if you must use punctuation in your linenames." + linebreak;
							issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
						}
					}
					
					var contentTriggerAction:String = contentTrigger.getAction().toLowerCase();
					if (lineObject.hasLineAttributes() && contentTriggerAction.indexOf("finish") != -1) {
						var finishesInsertIndex:int = contentTriggerAction.indexOf("*finishes*");
						if ((finishesInsertIndex == -1 || finishesInsertIndex != (contentTriggerAction.indexOf("finish") -1)) && lineObject.hasLineAttributes()) {
							issueSnippet = StringFunctions.getSnippet(line, contentTrigger.getLineSegment());
							issueMessage += "A line-reference trigger (" + contentTrigger.getLineSegment() + ") contains the word \"finish\" on a line with line-attributes on line " + lineObject.getLineNumber() + ", col " + (line.indexOf(contentTrigger.getLineSegment())) + ", near \"" + issueSnippet + "\"" + linebreak;
							issueMessage += "SDT will ignore lines that have finish in them, and also have line attributes." + linebreak;
							issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
						}
					}
				} else if (lineContent[i] is VariableInsertion) {
					var variableInsertion:VariableInsertion = lineContent[i] as VariableInsertion;
					if (variableInsertion.getContent().indexOf("finish") != -1 && lineObject.hasLineAttributes()) {
						issueSnippet = StringFunctions.getSnippet(line, variableInsertion.getLineSegment());
						issueMessage += "A variable insertion (" + variableInsertion.getLineSegment() + ") contains the word \"finish\" on a line with line-attributes on line " + lineObject.getLineNumber() + ", col " + (line.indexOf(variableInsertion.getLineSegment())) + ", near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += "SDT will ignore lines that have finish in them, and also have line attributes." + linebreak;
						issueMessage += "Line " + lineObject.getLineNumber() + ": " + line + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, lineObject, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				}
			}
		}
		
		private function checkLineOldStyle(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			//TODO: Convert all checks to new system (Issue class)
			var line:String = l.getRaw();
			var linebreak:String = "";
			if (options != null) {
				linebreak = options.getLineBreak();
			} else {
				linebreak = "\r\n";
			}
			var issue:Issue = null;
			var issueMessage:String = "";
			var issueSnippet:String = "";
			var attributeSection:String = "";
			var lineAttributeNames:Array = ["held", "style", "mood", "set", "check", "next"];
			var validValues:Array = new Array();
			validValues["held"] = ["true", "false"];
			validValues["style"] = ["thought", "speak", "him", "Thought", "Speak", "Him"];
			validValues["mood"] = ["Normal", "Angry", "Happy", "Ahegao"];
			validValues["set"] = "all";
			validValues["check"] = "all";
			validValues["next"] = "all";
			var altStyleValidValues = ["ThoughtAlt1", "ThoughtAlt2", "ThoughtAlt3", "ThoughtCustom", "SpeakAlt1", "SpeakAlt2", "SpeakAlt3", "SpeakCustom", "HimAlt1", "HimAlt2", "HimAlt3", "HimCustom"];
			//var specificErrorName:Array = new Array();
			//var specificErrorValue:Array = new Array();
			if (line.indexOf("initial_settings") != 0 && line.indexOf("{") != -1 && line.lastIndexOf("}") != -1 && line.indexOf("{") < line.lastIndexOf("}")) {
				attributeSection = line.substring(line.indexOf("{"), line.lastIndexOf("}") + 1);
				var arr:Array = parseLineAttributes(attributeSection);
				var attributeList:Array = new Array();
				for (var arrindex:uint = 0, arrsize:uint = arr.length; arrindex < arrsize; arrindex++) {
					var attributeName:String = arr[arrindex][0];
					var attributeValue:String = arr[arrindex][1];
					if (attributeValue == null) {
						issueSnippet = "";
						issueMessage += "Unable to identify line-attribute value for attribute (" + attributeName + ") on line " + l.getLineNumber() + "." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.SEVERE, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					} else if (validValues[attributeName] != null) {
						var isValidAltStyle = (attributeName == "style" && altStyleValidValues.indexOf(attributeValue) != -1);
						if (validValues[attributeName] == "all") {
							if (attributeList[attributeName] == null) {
								attributeList[attributeName] = attributeValue;
								if (attributeName == "next" && attributeValue == "") {
									issueSnippet = "";
									issueMessage += "The value of the next line-attribute is empty string on line " + l.getLineNumber() + "." + linebreak;
									issueMessage += "SDT can't read the empty string and will ignore this line-attribute." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
									addIssue(issue);
									issueMessage = "";
								}
							} else {
								issueSnippet = "";
								issueMessage += "A line-attribute's value is defined more than once on line " + l.getLineNumber() + ". Attribute name: " + attributeName + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
								addIssue(issue);
								issueMessage = "";
							}
						} else if (validValues[attributeName].indexOf(attributeValue) == -1 && !isValidAltStyle) {
							issueSnippet = "";
							issueMessage += "Illegal value (" + attributeValue + ") for attribute (" + attributeName + ") on line " + l.getLineNumber() + "." + linebreak;
							var commonInvalidMoodValues:Array = ["normal", "angry", "happy", "ahegao"];
							if (attributeName == "mood") {
								issueMessage += "Illegal values for the mood-attribute will crash SDT's Dialogue system. This issue has to be fixed before SDT will be able to interpret your dialogue correctly." + linebreak;
								if (line.indexOf("-") == 0) {
									issueMessage += "\"But this line is commented!\" SDT doesn't care about that, the lines still get parsed and SDT will still crash." + linebreak;
								}
								if (commonInvalidMoodValues.indexOf(attributeValue) != -1) {
									var fixedAttributeValue:String = attributeValue.charAt(0);
									fixedAttributeValue = fixedAttributeValue.toUpperCase();
									fixedAttributeValue += attributeValue.substring(1);
									issueMessage += "Be sure to capitalize the first character of the value. So don't put \"" + attributeValue + "\" but put \"" + fixedAttributeValue + "\" instead." + linebreak;
								}
							}
							issueMessage += "The allowed values for the attribute " + attributeName + " are: " + validValues[attributeName] + linebreak;
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							if (attributeName == "mood") {
								issue = new Issue(Severity.SEVERE, l, issueSnippet, issueMessage, "Syntax");
							} else {
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
							}
							addIssue(issue);
							issueMessage = "";
						} else {
							if (attributeList[attributeName] == null) {
								attributeList[attributeName] = attributeValue;
								if (isValidAltStyle) {
									dialogueChecker.addStyleAttributeValueDependency("dialogdisplayedit by sby", attributeValue);//bah, should be refactored to go via Data
								}
							} else {
								issueSnippet = "";
								issueMessage += "A line-attribute's value is defined more than once on line " + l.getLineNumber() + ". Attribute name: " + attributeName + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
								addIssue(issue);
								issueMessage = "";
							}
						}
					} else {
						issueSnippet = "";
						issueMessage += "Unknown line-attribute \"" + attributeName + "\" on line " + l.getLineNumber() + "." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				}
			}
			if (line.indexOf("\"") == -1 && line.indexOf(":") != -1 && (line.indexOf("”") != -1 || line.indexOf("“") != -1)) {
				issueSnippet = "";
				issueMessage += "Line " + l.getLineNumber() + " uses angled double quotes, rather than normal double quotes. Angled double quotes aren't parsed like straight double quotes by SDT and thus don't work." + linebreak;
				issueMessage += "Be sure to search your dialogue for “ and ”, replacing them with \". Angled double quotes get in your dialogue most likely via using a 'smart' text editor like Microsoft Word." + linebreak;
				issueMessage += "To avoid this issue in the future, check your dialogue with the DialogueChecker before AND after you've spellchecked it." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			if (l.isCommentLine()) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			if (line.indexOf("[[") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "[[");
				issueMessage += "Double trigger opener on line " + l.getLineNumber() + ", col " + (line.indexOf("[[")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (line.indexOf("]]") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "]]");
				issueMessage += "Double trigger closer on line " + l.getLineNumber() + ", col " + (line.indexOf("]]")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			if (line.indexOf("::") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "::");
				issueMessage += "Double colon on line " + l.getLineNumber() + ", col " + (line.indexOf("::")) + " near \"" + issueSnippet + "\". Suggested fix - remove one of the colons." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			var leftBraceIndex:int = line.indexOf("{");
			if (leftBraceIndex != -1) {
				if (line.indexOf("{style", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "{style");
					issueMessage += "Probable improper use of style attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("{style")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("set:", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "set:", leftBraceIndex);
					issueMessage += "Probable improper use of set attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("set:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap set in double quotes, like so: \"set\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("style:", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "style:", leftBraceIndex);
					issueMessage += "Probable improper use of style attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("style:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap style in double quotes, like so: \"style\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("check:", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "check:", leftBraceIndex);
					issueMessage += "Probable improper use of check attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("check:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap check in double quotes, like so: \"check\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("mood:", leftBraceIndex) != -1 && (line.indexOf("mood:", leftBraceIndex) != (line.indexOf(";mood:", leftBraceIndex) + 1)) && (line.indexOf("da.charcode.load", leftBraceIndex) == -1 && line.indexOf("loadCharCode", leftBraceIndex) == -1)) {
					issueSnippet = StringFunctions.getSnippet(line, "mood:", leftBraceIndex);
					issueMessage += "Probable improper use of mood attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("mood:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap mood in double quotes, like so: \"mood\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("next:", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "next:", leftBraceIndex);
					issueMessage += "Probable improper use of next attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("next:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap next in double quotes, like so: \"next\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("held:", leftBraceIndex) != -1) {
					issueSnippet = StringFunctions.getSnippet(line, "held:", leftBraceIndex);
					issueMessage += "Probable improper use of held attribute on line " + l.getLineNumber() + ", col " + (line.indexOf("held:", leftBraceIndex)) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "Wrap held in double quotes, like so: \"held\":" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
				if (line.indexOf("},{") != -1) {
					issueSnippet = "";
					issueMessage += "Coding error on line " + l.getLineNumber() + "." + linebreak;
					issueMessage += "Don't combine {} blocks with only a comma. Add identifiers for each block so SDT understands what the block is for, like so: {\"check\":{\"a\":\"avalue\"},\"set\":{\"b\":\"bvalue\"}}" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			if (line.indexOf("%01") != -1) {
				issueSnippet = "";
				issueMessage += "Usage of %01 on line " + l.getLineNumber() + "." + linebreak;
				issueMessage += "Don't use the old way of delaying dialogue (with %01). It clutters your dialogue. During testing, I found that 1x Б equals 1x %01. Suggested fix: Replace %01 with Б." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (line.indexOf("[]") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "[]");
				issueMessage += "Empty trigger on line " + l.getLineNumber() + ", col " + line.indexOf("[]") + "." + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += "Whilst there's not wrong with empty triggers, you should not use empty triggers to trigger lines without linenames." + linebreak + "Lines without linenames cannot be triggered by the 'next' line-attribute." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (line.indexOf("initial-settings") != -1) {
				issueSnippet = "";
				issueMessage += "You've used \"initial-settings\" in a line (line " + l.getLineNumber() + "). Perhaps you meant \"initial_settings\"?" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (line.indexOf("dialogue-name") != -1) {
				issueSnippet = "";
				issueMessage += "You've used \"dialogue-name\" in a line (line " + l.getLineNumber() + "). Perhaps you meant \"dialogue_name\"?" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (line.indexOf("\"") != -1 && line.indexOf(":") != -1) {
				if (line.indexOf(":") > line.indexOf("\"")) {
					issueSnippet = "";
					issueMessage += "The linename of line " + l.getLineNumber() + " (" + line.substr(0, line.indexOf(":")) + ") contains double quotes. Did you accidentally misplace or forget the colon?" + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			var d:Array = line.split("\"");
			if (d.length % 2 != 1) {
				issueSnippet = "";
				issueMessage += "An uneven amount of double quotes was found on line " + l.getLineNumber() + ". This could indicate a coding error." + linebreak;
				if (line.indexOf("”") != -1 || line.indexOf("“") != -1) { 
					issueMessage += "The line does contain a few angled double quotes - did you (or text editor) accidentally change the double quotes?" + linebreak;
				}
				if (line.indexOf("'") != -1) {
					issueMessage += "The line contains at least one single quote - did you perhaps type a single quote where you should have used a double quote?" + linebreak;
				}
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (d.length > 1 && line.indexOf(":") == -1 && line.indexOf("-") != 0) {
				issueSnippet = "";
				issueMessage += "Double quotes were found on line " + l.getLineNumber() + ", but the line didn't have a colon. Did you forget it, or is this a commentary line?" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			if (!options.getSetting("NoIllegalColon") && d.length > 1 && line.indexOf(":\"") != -1 && line.indexOf(":\"") == line.indexOf(":")) {
				var s0:String = d[0];
				var s1:String = d[1];
				if (s1.indexOf(":") != -1) {
					issueSnippet = "";
					issueMessage += "Found a probable illegal colon on line " + l.getLineNumber() + ", col " + (s0.length + s1.indexOf(":") + 1) + ". Replace colons in dialogue lines with %3A." + linebreak;
					issueMessage += "You can disable this check with #DialogueChecker-NoIllegalColon# if you want to suppress this issue." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			var lineProperlyEnded:Boolean = true;
			if (d.length > 2) {
				var d0:String = d[0];
				var d1:String = d[1];
				var d2:String = d[2];
				if (leftBraceIndex > (d0.length + d1.length + d2.length + 2)) {
					issueSnippet = "";
					issueMessage += "A third double quote was found on line " + l.getLineNumber() + ", col " + (d0.length + d1.length + d2.length + 2) + ", before finding a { in that line. This indicates a coding error." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
					lineProperlyEnded = false;
				} else if (leftBraceIndex == -1) {
					if (d.length > 3) {
						issueSnippet = "";
						issueMessage += "More than 2 double quotes were found on line " + l.getLineNumber() + ", but the line didn't have lineattributes. Did you accidentally put a double quote (\") in the line?" + linebreak;
						issueMessage += "If you want to have double quotes in your line, you should replace them with %22. SDT will replace %22 with \" when playing the dialogue, but it won't interfere with loading the dialogue." + linebreak;
						issueMessage += "If you were to keep the double quotes in the line as it is now, the line will be cut short after the second double quote. There's likely an \"excess characters\" issue for this line as well - that's what will get cut off." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				}
				
			}
			
			var c:Array = line.split("*");
			var perc2a:Array = line.split("%2A"); //1+1, +1 for each *, +1 for each %2A. Looking for even amount of asterisk and %2A combined.
			if ((c.length + perc2a.length) % 2 != 0 && line.indexOf("initial_settings") != 0) {
				var ignoreUnevenAsteriskCount:Boolean = false;
				if (lineProperlyEnded && leftBraceIndex != -1) {
					var attributeString:String = line.substring(leftBraceIndex);
					var asteriskInAttribute:Array = attributeString.split("*");
					if ((c.length - (asteriskInAttribute.length - 1)) % 2 == 1) {
						ignoreUnevenAsteriskCount = true;
					}
				}
				if (!ignoreUnevenAsteriskCount) {
					issueSnippet = "";
					issueMessage += "An uneven amount of asterisks was found on line " + l.getLineNumber() + ". This could indicate a coding error." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			var e:Array = line.split("{");
			var f:Array = line.split("}");
			if (e.length != f.length) {
				issueSnippet = "";
				issueMessage += "An uneven amount of opening and closing {} was found on line " + l.getLineNumber() + ". This could indicate a coding error." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			var g:Array = line.split("[");
			var h:Array = line.split("]");
			if (g.length != h.length) {
				issueSnippet = "";
				issueMessage += "An uneven amount of opening and closing [] was found on line " + l.getLineNumber() + ". This could indicate a coding error." + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			var bracketCount:int = 0;
			var inInsertion:Boolean = false;
			for (var j:uint = 0, jsize:uint = line.length; j < jsize; j++) {
				var char:String = line.charAt(j);
				//TODO - check for [*ME *] which will always fail
				if (char == "*" || (char == "%" && line.length >= j + 2 && line.charAt(j + 1) == "2" && line.charAt(j + 2) == "A")) { //* or %2A
					inInsertion = !inInsertion;
				}
				if (char == "[") {
					bracketCount++;
					if (bracketCount > 1) {
						issueSnippet = "";
						issueMessage += "Probable nested triggers on line " + l.getLineNumber() + ", col " + j + ". This indicates a coding error." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
						break;
					}
				} else if (char == "]") {
					bracketCount--;
					if (bracketCount < 0) {
						issueSnippet = "";
						issueMessage += "Missing trigger start of a trigger on line " + l.getLineNumber() + ", col " + j + ". This indicates a coding error." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
						break;
					}
				} else if (char == " " && bracketCount == 1 && !inInsertion) {
					issueSnippet = "";
					issueMessage += "Space in trigger on line " + l.getLineNumber() + ", col " + j + "." + linebreak;
					issueMessage += "SDT does not accept spaces in triggers - triggers with spaces in them will appear as plain text in dialogues." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			var excess:Array = l.getExcess();
			for (var i:uint = 0, isize:uint = excess.length; i < isize; i++) {
				var excessString:String = excess[i];
				if (excessString != "") {
					if (i == 0) {
						issueSnippet = "";
						issueMessage += "Excess characters (" + excessString + ") between linename and line content on line " + l.getLineNumber() + ". This indicates a syntax error." + linebreak;
						issueMessage += "Excess characters between the linename and line content means the line will be ignored by SDT." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				}
				if (StringFunctions.replaceAll(excessString, " ", "") != "") {
					if (i == 1) {
						issueSnippet = "";
						issueMessage += "Excess characters (" + excessString + ") between line content and line attributes on line " + l.getLineNumber() + ". This indicates a syntax error." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					} else if (i == 2) {
						issueSnippet = "";
						issueMessage += "Excess characters (" + excessString + ") after line attributes on line " + l.getLineNumber() + ". This indicates a syntax error." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
					}
				}
			}
			
			var attributeRemainder:String = getLineAttributesRemainder(line);
			if (attributeRemainder.indexOf(":") != -1) {
				issueSnippet = "";
				issueMessage += "Failed to properly parse line-attributes for line " + l.getLineNumber() + ", parsing stopped at col " + (line.indexOf(attributeRemainder)) + (line.indexOf(attributeRemainder) == -1 ? " (ERROR)" : "") + " at \"" + attributeRemainder + "\"" + linebreak;
				for (var j:uint = 0, jsize:uint = lineAttributeNames.length; j < jsize; j++) {
					if (attributeRemainder.indexOf(lineAttributeNames[j]) == 1) {
						issueMessage += "It seems you made improper use of a line-attribute. The " + lineAttributeNames[j] + " line-attribute has to be surrounded with double-quotes, like so: \"" + lineAttributeNames[j] + "\"" + linebreak;
						j = jsize;
					}
				}
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
				addIssue(issue);
				issueMessage = "";
			}
			
			checkLineAttributes(l, dialogue, options);
		}
		
		private function checkLineAttributes(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			var lineRaw:String = l.getLineAttributesRaw();
			var linebreak:String = "";
			if (options != null) {
				linebreak = options.getLineBreak();
			} else {
				linebreak = "\r\n";
			}
			
			var issue:Issue = null;
			var issueMessage:String = "";
			var issueSnippet:String = "";
			//TODO check for {} - empty attribute string, makes no sense and should a minor issue
			
			//code duplication ahoy
			//why did I parse line attributes in the SyntaxChecker class in the first place
			
			//{"test":"hi","bob":1,"alice":"hey, bob"}
			var lineAttributes:Array = new Array();
			var singleVar:String = "";
			var attributeLine:String = lineRaw;
			var state:uint = 0; //0 = pre parsing vars, 1 = start of parsing a var, 2 = end parsing a var, 3 = find 
			while (attributeLine != "") {
				if (StringFunctions.stringStartsWith(attributeLine, "}")) {
					return;
				}
				if (state == 0) {
					state = 1;
					if (attributeLine.indexOf("{") == -1) {
						return;
					}
					attributeLine = "," + attributeLine.substring(attributeLine.indexOf("{") + 1); //shortcut
				} else if (state == 1) {
					//,"test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf(",\"") != 0 && attributeLine.indexOf(", \"") != 0) {
						return; //TODO figure out when this triggers, then write appropriate warning
					}
					if (attributeLine.indexOf(",\"") == 0) {
						attributeLine = attributeLine.substring(2);
					} else if (attributeLine.indexOf(", \"") == 0) {
						attributeLine = attributeLine.substring(3);
					}
					//test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == -1) { //Failed to identify singlevar
						issueSnippet = StringFunctions.getSnippet(l.getRaw(), attributeLine);
						issueMessage += "Failed to parse variable name for variable " + lineAttributes.length + " in line-attributes (" + l.getLineAttributesRaw() + ") on line " + l.getLineNumber() + ". This indicates a syntax error, possibly missing double quotes?" + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
						return;
					}
					singleVar = attributeLine.substring(0, attributeLine.indexOf("\""));
					attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
					//:"hi","bob":1,"alice":"hey, bob"}
					if (!StringFunctions.stringStartsWith(attributeLine, ":")) {
						issueSnippet = StringFunctions.getSnippet(l.getRaw(), attributeLine);
						issueMessage += "Failed to parse variable value for " + singleVar + " in line-attributes (" + l.getLineAttributesRaw() + ") on line " + l.getLineNumber() + "." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
						addIssue(issue);
						issueMessage = "";
						
						lineAttributes.push([singleVar]);
						return;
					}
					attributeLine = attributeLine.substring(attributeLine.indexOf(":") + 1); //TODO check if SDT cares about whitespace between "variableName" and : and "value" in lineattributes
					//"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == 0) {
						attributeLine = attributeLine.substring(1);
						//hi","bob":1,"alice":"hey, bob"}
						lineAttributes.push([singleVar, attributeLine.substring(0, attributeLine.indexOf("\""))]);
						if (attributeLine.indexOf("\"") != -1) {
							attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
								//,"bob":1,"alice":"hey, bob"}
						}
					} else if (attributeLine.indexOf("{") == 0) {
						var rightBraceIndex:int = attributeLine.indexOf("}");
						if (rightBraceIndex == -1) {
							issueSnippet = "";
							issueMessage += "The variable " + singleVar + " on line " + l.getLineNumber() + " starts a key-value block with a {, but doesn't end it. This will cause SDT to fail parsing the line attributes." + linebreak;
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
							return;
						}
						lineAttributes.push([singleVar, attributeLine.substring(1, rightBraceIndex)]); //uhhh... am I just tossing everything in a nested lineattribute in a variable and calling it a day?
						var remainder:String = getLineAttributesRemainder(attributeLine.substring(0, rightBraceIndex + 1));
						if (remainder.indexOf(":") != -1) {
							issueSnippet = "";
							issueMessage += "Failed to properly parse line-attributes for line " + l.getLineNumber() + ", parsing stopped at col " + (l.getRaw().indexOf(remainder)) + (l.getRaw().indexOf(remainder) == -1 ? " (ERROR)" : "") + " at \"" + remainder + "\"" + linebreak;
							if (StringFunctions.stringStartsWith(remainder, "\"")) {
								issueMessage += "Looks like you forgot to put a comma between a value and the next variable." + linebreak;
							}
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Syntax");
							addIssue(issue);
							issueMessage = "";
						}
						
						attributeLine = attributeLine.substring(rightBraceIndex + 1);
					} else if (attributeLine.indexOf("}") != 0) {
						var numend:int = findEndOfNumber(attributeLine);
						if (numend != -1) {
							lineAttributes.push([singleVar, attributeLine.substring(0, numend)]);
							var nextcomma:int = attributeLine.indexOf(",");
							var nextrightaccolade:int = attributeLine.indexOf("}");
							var nextvar:int = nextcomma;
							if (nextcomma < nextrightaccolade) {
								nextvar = nextrightaccolade;
							}
							attributeLine = attributeLine.substring(0, nextvar);
						}
					} else {
						return;
					}
				}
			}
		}
		
		//TODO find a way to get rid of this function
		public function parseLineAttributes(line:String):Array {
			//{"test":"hi","bob":1,"alice":"hey, bob"}
			var lineAttributes:Array = new Array();
			var singleVar:String = "";
			var attributeLine:String = line;
			var state:uint = 0; //0 = pre parsing vars, 1 = start of parsing a var, 2 = end parsing a var, 3 = find 
			while (attributeLine != "") {
				if (attributeLine.indexOf("}") == 0) {
					return lineAttributes;
				}
				if (state == 0) {
					state = 1;
					if (attributeLine.indexOf("{") == -1) {
						return lineAttributes;
					}
					attributeLine = "," + attributeLine.substring(attributeLine.indexOf("{") + 1); //shortcut
				} else if (state == 1) {
					//,"test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf(",\"") != 0 && attributeLine.indexOf(", \"") != 0) {
						return lineAttributes;
					}
					if (attributeLine.indexOf(",\"") == 0) {
						attributeLine = attributeLine.substring(2);
					} else if (attributeLine.indexOf(", \"") == 0) {
						attributeLine = attributeLine.substring(3);
					}
					//test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == -1) { //Failed to identify singlevar
						return lineAttributes;
					}
					singleVar = attributeLine.substring(0, attributeLine.indexOf("\""));
					attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
					//:"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf(":") != 0) {
						lineAttributes.push([singleVar]);
						return lineAttributes;
					}
					attributeLine = attributeLine.substring(attributeLine.indexOf(":") + 1);
					//"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == 0) {
						attributeLine = attributeLine.substring(1);
						//hi","bob":1,"alice":"hey, bob"}
						lineAttributes.push([singleVar, attributeLine.substring(0, attributeLine.indexOf("\""))]);
						if (attributeLine.indexOf("\"") != -1) {
							attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
								//,"bob":1,"alice":"hey, bob"}
						}
					} else if (attributeLine.indexOf("{") == 0) {
						var rightBraceIndex:int = attributeLine.indexOf("}");
						if (rightBraceIndex == -1) {
							return lineAttributes;
						}
						lineAttributes.push([singleVar, attributeLine.substring(1, rightBraceIndex)]);
						attributeLine = attributeLine.substring(rightBraceIndex + 1);
					} else if (attributeLine.indexOf("}") != 0) {
						var numend:int = findEndOfNumber(attributeLine);
						if (numend != -1) {
							lineAttributes.push([singleVar, attributeLine.substring(0, numend)]);
							var nextcomma:int = attributeLine.indexOf(",");
							var nextrightaccolade:int = attributeLine.indexOf("}");
							var nextvar:int = nextcomma;
							if (nextcomma < nextrightaccolade) {
								nextvar = nextrightaccolade;
							}
							attributeLine = attributeLine.substring(0, nextvar);
						}
					} else {
						return lineAttributes;
					}
				}
			}
			return lineAttributes;
		}
		
		private function findEndOfNumber(line:String):int {
			var comma:int = line.indexOf(",");
			if (comma != -1) {
				return comma;
			}
			var rightBrace:int = line.indexOf("}");
			if (rightBrace != -1) {
				return rightBrace;
			}
			return line.indexOf(" ");
		}
		
		//TODO find a way to get rid of this function
		public function getLineAttributesRemainder(line:String):String {
			var lineAttributes:Array = new Array();
			var singleVar:String = "";
			var attributeLine:String = line;
			var state:uint = 0; //0 = pre parsing vars, 1 = start of parsing a var, 2 = end parsing a var, 3 = find 
			while (attributeLine != "") {
				if (attributeLine.indexOf("}") == 0) {
					return attributeLine;
				}
				if (state == 0) {
					state = 1;
					if (attributeLine.indexOf("{") == -1) {
						return ""; //Line has no attributes
					}
					attributeLine = "," + attributeLine.substring(attributeLine.indexOf("{") + 1); //shortcut
				} else if (state == 1) {
					//,"test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf(",\"") != 0 && attributeLine.indexOf(", \"") != 0) {
						return attributeLine;
					}
					if (attributeLine.indexOf(",\"") == 0) {
						attributeLine = attributeLine.substring(2);
					} else if (attributeLine.indexOf(", \"") == 0) {
						attributeLine = attributeLine.substring(3);
					}
					//test":"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == -1) { //Failed to identify singlevar
						return attributeLine;
					}
					singleVar = attributeLine.substring(0, attributeLine.indexOf("\""));
					attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
					//:"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf(":") != 0) {
						lineAttributes.push([singleVar]);
						return attributeLine;
					}
					attributeLine = attributeLine.substring(attributeLine.indexOf(":") + 1);
					//"hi","bob":1,"alice":"hey, bob"}
					if (attributeLine.indexOf("\"") == 0) {
						attributeLine = attributeLine.substring(1);
						//hi","bob":1,"alice":"hey, bob"}
						lineAttributes.push([singleVar, attributeLine.substring(0, attributeLine.indexOf("\""))]);
						if (attributeLine.indexOf("\"") != -1) {
							attributeLine = attributeLine.substring(attributeLine.indexOf("\"") + 1);
								//,"bob":1,"alice":"hey, bob"}
						}
					} else if (attributeLine.indexOf("{") == 0) {
						var rightBraceIndex:int = attributeLine.indexOf("}");
						if (rightBraceIndex == -1) {
							return attributeLine;
						}
						lineAttributes.push([singleVar, attributeLine.substring(1, rightBraceIndex)]);
						attributeLine = attributeLine.substring(rightBraceIndex + 1);
					} else if (attributeLine.indexOf("}") != 0) {
						var numend:int = findEndOfNumber(attributeLine);
						if (numend != -1) {
							lineAttributes.push([singleVar, attributeLine.substring(0, numend)]);
							var nextcomma:int = attributeLine.indexOf(",");
							var nextrightaccolade:int = attributeLine.indexOf("}");
							var nextvar:int = nextcomma;
							if (nextcomma > nextrightaccolade) {
								nextvar = nextrightaccolade;
							}
							attributeLine = attributeLine.substring(nextvar);
						}
					} else {
						return attributeLine;
					}
				}
			}
			return attributeLine;
		}
	
	}

}