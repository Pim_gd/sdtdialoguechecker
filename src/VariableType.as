package {
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableType extends Enum {
		private static const enums:Array = Enum.loadValuesForEnum(VariableType);
		public static const NUMERIC:VariableType = enums["NUMERIC"];
		public static const STRING:VariableType = enums["STRING"];
		public static const BOOLEAN:VariableType = enums["BOOLEAN"];
		public static const UNKNOWN:VariableType = enums["UNKNOWN"];
		
		public function VariableType(val:uint, ID:String) {
			super(val, ID);
		}
		
		public static function getAllEnums():Array {
			var tempArray:Array = new Array();
			for each (var variableType:VariableType in enums) {
				tempArray[variableType.getValue()] = variableType;
			}
			return tempArray;
		}
	}

}