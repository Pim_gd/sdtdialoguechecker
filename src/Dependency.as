package {
	
	/**
	 * DialogueChecker has dependencies, Checkers can call addDependency(modName, object) - object is of Line, Variable or Trigger type.
	 * Triggers & Lines will be tested by LineUsageChecker
	 * Variables are tested by VariableChecker
	 * @author Pimgd
	 */
	public class Dependency {
		private var modName:String;
		private var variables:Array; //of String
		private var triggers:Array; //of String
		private var lines:Array; //of String (via lineName)
		private var styleAttributeValues:Array; //of String (the actual values)
		
		public function Dependency(name:String) {
			modName = name;
			variables = new Array();
			triggers = new Array();
			lines = new Array();
			styleAttributeValues = new Array();
		}
		
		public function addTrigger(trigger:String):void {
			ArrayFunctions.addIfNotExists(triggers, trigger);
		}
		
		public function addVariable(variable:String):void {
			ArrayFunctions.addIfNotExists(variables, variable);
		}
		
		public function addLine(line:String):void {
			ArrayFunctions.addIfNotExists(lines, line);
		}
		
		public function addStyleAttributeValue(attributeValue:String):void {
			ArrayFunctions.addIfNotExists(styleAttributeValues, attributeValue);
		}
		
		public function printDependency(o:Options):String {
			if ((variables.length + triggers.length + lines.length + styleAttributeValues.length) == 0) {
				return "";
			}
			var linebreak:String = o.getLineBreak();
			var result:String = modName + ", through the use of ";
			for (var i:uint = 0, isize:uint = lines.length; i < isize; i++) {
				result += lines[i] + " - linename, ";
			}
			for (var j:uint = 0, jsize:uint = triggers.length; j < jsize; j++) {
				result += triggers[j] + " - trigger, ";
			}
			for (var k:uint = 0, ksize:uint = variables.length; k < ksize; k++) {
				result += variables[k] + " - variable, ";
			}
			for (var m:uint = 0, msize:uint = styleAttributeValues.length; m < msize; m++) {
				result += styleAttributeValues[m] + " - style attribute value, ";
			}
			
			result = result.substring(0, result.length - 2) + "."; //replace last comma-space with .
			result += linebreak;
			return result;
		}
	
	}

}