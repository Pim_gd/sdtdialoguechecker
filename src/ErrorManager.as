package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class ErrorManager {
		public static const debugging:Boolean = false;
		
		public function ErrorManager() {
		
		}
		
		public static function showDevelopmentError(errorMessage:String):void {
			trace(errorMessage);
			var error:Error = new Error(errorMessage);
			trace(error.getStackTrace());
			throw error;
		}
	
	}

}