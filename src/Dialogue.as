package {
	
	/**
	 * Represents a Dialogue for SDT.
	 * Should be seen as an entry point to a datastructure for a Dialogue.
	 * @author Pimgd
	 */
	public class Dialogue {
		/**
		 * The lines of the dialogue file, stored in order of appearance.
		 */
		private var fileLines:Array;
		/**
		 * The lines of the dialogue, stored in order of appearance.
		 */
		private var lines:Array;
		/**
		 * A dictionary of the lines. Structured like so:
		 * Indexed by linename. Contents are an array of indexes for the lines array.
		 */
		private var linesDictionary:Array;
		
		/**
		 * The entire dialogue in string format, if available.
		 */
		private var dialogue:String;
		
		/**
		 * String indexed.
		 */
		private var variables:Array;
		
		public function Dialogue(dialogueText:String = "", dialogueLines:Array = null, dialogueLinesDictionary:Array = null) {
			variables = new Array();
			dialogue = dialogueText;
			if (dialogueLines == null) {
				lines = new Array();
			} else {
				lines = dialogueLines;
			}
			if (dialogueLinesDictionary == null) {
				linesDictionary = new Array();
			} else {
				linesDictionary = dialogueLinesDictionary;
			}
			parseDialogueFromString(dialogueText);
		}
		
		public function getRawDialogue():String {
			return dialogue;
		}
		
		private function splitDialogueToLines(dialogueText:String):Array {
			var a:Array = dialogueText.split("\r"); //Maybe I should split by \r\n first, then by \n\r, then by \r, then by \n?
			if (a.length == 1) {
				a = dialogueText.split("\n");
			}
			return a;
		}
		
		/**
		 * Searches through the dialogue file for a line that starts with the specified string.
		 * Useful for checking if the dialogue contains lines that might parse badly.
		 * @param	searchFor The string to search for
		 * @return The line number of the line that matches the search string (1-indexed!). -1 if not found.
		 */
		public function getFileLineNumberWhereLineStartsWith(searchFor:String):int {
			for (var i:uint = 0, isize:uint = fileLines.length; i < isize; i++) {
				var line:String = fileLines[i];
				if (line.indexOf(searchFor) == 0) {
					return i+1;
				}
			}
			return -1;
		}
		
		public function getFileLineByLineNumber(lineNumber:int):String {
			return fileLines[lineNumber-1];
		}
		
		public function parseDialogueFromString(dialogueText:String):void {
			//Parse the dialogue here.
			fileLines = splitDialogueToLines(dialogueText);
			
			//Parse each line individually...
			for (var i:uint = 0, isize:uint = fileLines.length; i < isize; i++) {
				var line:Line = parseLine(fileLines[i], i);
				if (line != null) {
					readVariableSetsFromLine(line);
					
					//Then place them in the dictionary and main array.
					ArrayFunctions.getOrCreateSubArray(linesDictionary, line.getLineName()).push(lines.push(line) - 1);
				}
			}
		}
		
		private function readVariableSetsFromLine(line:Line):void {
			for each (var trigger:Trigger in line.getTriggers()) {
				if (StringFunctions.stringStartsWith(trigger.getAction(), "SETVAR_") && !trigger.containsVariableInsertion()) {
					var variableName:String = StringFunctions.addQuotes(trigger.getArgument(1, 1));
					var value:String = trigger.getArgument(1, 2);
					if (StringFunctions.stringStartsWith(value, "+=")) {
						value = StringFunctions.addQuotes("+" + value.substr(2));//working around SDT's logic here - if x is 50, x "+10" = 60. x "10" = 10. x "-10" = 40. x -10 = -10. x 10 = 10. So += is replicated via "+VALUE".
					} else if (StringFunctions.stringStartsWith(value, "-=")) {
						value = StringFunctions.addQuotes("-" + value.substr(2));//"-VALUE"
					}
					addSetValueToVariable(variableName, value);
				}
			}
		}
		
		private function determineNextParseMode(remainingContent:String):String {
			var hasPercentAsteriskAtIndexZero:Boolean = remainingContent.indexOf("%2A") == 0;
			if (hasPercentAsteriskAtIndexZero) {
				return "Insertion";
			}
			var firstChar:String = remainingContent.charAt(0);
			if (firstChar == "%") {
				return "SpecialChar";
			} else if (firstChar == "[") {
				return "Trigger";
			} else if (firstChar == "*") {
				return "Insertion";
			} else if (firstChar == "Б") {
				return "Delay";
			}
			return "Normal";
		}
		
		/**
		 * Parses a single Line of Dialogue from a String.
		 * Note that since SDT still parses commented lines, the DialogueChecker has to do so as well.
		 * Certain severe issues don't care that a line is unused, for instance.
		 * @param	lineText the string that should be converted to a Line object.
		 * @param	lineIndex the index of the line in the array - used for determining lineNumber.
		 * @return a line object if possible, nothing otherwise.
		 */
		private function parseLine(lineText:String, lineIndex:uint):Line {
			var lineNumber:uint = lineIndex + 1;
			var lineName:String = "";
			var rawLine:String = lineText;
			var lineContent:Array = new Array();
			var lineExcess:Array = ["", "", ""];//(After Period, before start of content), (After content, before start of line attributes), (After line attributes)
			var lineAttribute:LineAttribute = null;
			var lineAttributeValueArray:Array = new Array();
			var lineAttributes:Array = new Array();
			var lineAttributesString:String = "";
			var lineContentString:String = "";
			var line:Line = null;
			
			//1|intro:"Hi!" {"style":"Him"}
			//2|general : "Hello"
			//3|help"me"{"next":"pls"}
			//4|held:Let go!{"held":"true"}
			//5|pre_cum"Coming!"
			//6|font:Arial
			var source:String = lineText;
			var nameEnd:uint = 0;
			if (source.indexOf(":") != -1 || source.indexOf("\"") != -1) {
				//valid for all 6 examples.
				//Needs : or " to be a line. Otherwise it's not parseable.
				if (source.indexOf(":") != -1) {
					nameEnd = source.indexOf(":");
				} else {
					nameEnd = source.indexOf("\"");
				}
			} else {
				return null; //Comment Filter! Woo.
			}
			lineName = source.substr(0, nameEnd);
			source = source.substring(nameEnd);
			//1|:"Hi!" {"style":"Him"}
			//2|: "Hello"
			//3|:"pls"}
			//4|:Let go!{"held":"true"}
			//5|"Coming!"
			//6|:Arial
			var contentStart:int = 0;
			if (source.indexOf(":") == 0) {
				source = source.substring(1);
				//1|"Hi!" {"style":"Him"}
				//2| "Hello"
				//3|"pls"}
				//4|Let go!{"held":"true"}
				//6|Arial
				contentStart = source.indexOf("\"");
			}
			if (contentStart >= 0 && lineName != "initial_settings") {
				lineExcess[0] = source.substr(0, contentStart);
				source = source.substr(contentStart);
			}
			//1|"Hi!" {"style":"Him"}
			//2|"Hello"
			//3|"pls"}
			//4|"held":"true"}
			//5|"Coming!"
			//6|Arial
			var contentString:String = "";
			if (source.indexOf("\"") == 0 && source.indexOf("\"", 1) != -1 && lineName != "initial_settings") {
				contentString = source.substr(0, source.indexOf("\"", 1) + 1);
				source = source.substr(source.indexOf("\"", 1) + 1);
			}
			//source:
			//1| {"style":"Him"}
			//2|
			//3|}
			//4|:"true"}
			//5|
			//6|Arial
			
			//contentString:
			//1|"Hi!"
			//2|"Hello"
			//3|"pls"
			//4|"held"
			//5|"Coming!"
			//6|
			if (contentString == "" && lineName != "initial_settings") {
				contentString = source;
				source = "";
			}
			lineContentString = contentString;
			//Parse contentString
			//When parsing a contentString, you're treating everything as normal string until...
			//Б - change to delayString (new normal string, keep until different character)
			//% - change to special char (new normal string, parse next 2 chars, keep that as pattern, then parse until pattern is broken)
			//* - change to VariableInsertion/VariableSubstitution (Treat as Insertion until YOU/ME/YOUR/MY/FINISHES is detected)
			//[ - change to Trigger
			//Remember kids, Stringly-typed code is bad! What I'm doing here is bad! So don't do it! (Even although it makes things easier sometimes...)
			while (contentString != "") {
				var currentContentSegment:String = "";
				var contentParseMode:String = determineNextParseMode(contentString);
				if (contentParseMode == "Normal") {
					var checkArray:Array = new Array();
					checkArray.push(contentString.indexOf("Б"));
					checkArray.push(contentString.indexOf("%"));
					checkArray.push(contentString.indexOf("*"));
					checkArray.push(contentString.indexOf("["));
					var nextIssue:int = contentString.length;
					for (var i:uint = 0, isize:uint = checkArray.length; i < isize; i++) {
						if (checkArray[i] != -1 && nextIssue > checkArray[i]) {
							nextIssue = checkArray[i];
						}
					}
					lineContent.push(contentString.substr(0, nextIssue));
					contentString = contentString.substr(nextIssue);
				} else if (contentParseMode == "Delay") {
					//Add all continuous delay characters as contentpiece
					var delayCharacters:uint = 0;
					while (contentString.length > delayCharacters && contentString.charAt(delayCharacters) == "Б") {
						delayCharacters++;
					}
					lineContent.push(contentString.substring(0, delayCharacters));
					contentString = contentString.substr(delayCharacters);
				} else if (contentParseMode == "SpecialChar") {
					//Identify special char type, loop until pattern is not next section of string. Add all looped chars to content array, strip from content string. Evaluate next section.
					var specialCharPattern:String = contentString.substr(0, 3);
					//check if it's actually a percent encoded value here
					var hexString:String = "0123456789ABCDEFabcdef";
					if (!(specialCharPattern.length == 3 && hexString.indexOf(specialCharPattern.charAt(1)) != -1 && hexString.indexOf(specialCharPattern.charAt(2)) != -1)) {
						//it's not a proper percent encoded value.
						//treat the % as %25, and strip only 1 character.
						
						currentContentSegment = "%25";
						contentString = contentString.substring(1); //strip 1 char
					} else {
						var occurances:uint = 0;
						while (contentString.length >= ((occurances * 3) + 3) && contentString.substring(occurances * 3, (occurances * 3) + 3) == specialCharPattern) {
							occurances++;
						}
						currentContentSegment = contentString.substring(0, (occurances * 3));
						contentString = contentString.substr(occurances * 3);
					}
					lineContent.push(currentContentSegment);
				} else if (contentParseMode == "Trigger") {
					//TODO: Identify variable insertions within triggers
					var trigger:String = "";
					var rightBracketIndex:int = contentString.indexOf("]");
					if (rightBracketIndex != -1) {
						trigger = contentString.substr(1, rightBracketIndex - 1);
						contentString = contentString.substr(rightBracketIndex + 1);
					} else {
						//user error, so ignore it. Act like rest of line is trigger.
						trigger = contentString.substr(1);
						contentString = "";
					}
					var type:TriggerType = TriggerType.identifyType(trigger);
					var triggerObject:Trigger = new Trigger(trigger, type);
					lineContent.push(triggerObject);
				} else if (contentParseMode == "Insertion") {
					//Find insertion and determine type.
					var insertion:String = "";
					var indexOfAsterisk:int = contentString.indexOf("*");
					var indexOfPercent2A:int = contentString.indexOf("%2A");
					var indexOfSecondAsterisk:int = contentString.indexOf("*", 1);
					var indexOfSecondPercent2A:int = contentString.indexOf("%2A", 1);
					if (Math.max(indexOfSecondAsterisk, indexOfSecondPercent2A) != -1) {
						var insertionIndex:int = indexOfAsterisk;
						var insertionEndIndex:int = indexOfSecondAsterisk;
						var insertionStartLength:uint = 1;
						var insertionEndLength:uint = 1;
						if (indexOfAsterisk == -1 || (indexOfPercent2A < indexOfAsterisk && indexOfPercent2A != -1)) {
							insertionIndex = indexOfPercent2A;
							insertionStartLength = 3;
						}
						if (indexOfSecondAsterisk == -1 || (indexOfSecondPercent2A < indexOfSecondAsterisk && indexOfSecondPercent2A != -1)) {
							insertionEndIndex = indexOfSecondPercent2A;
							insertionEndLength = 3;
						}
						insertion = contentString.substr(insertionStartLength, insertionEndIndex - insertionStartLength);
						contentString = contentString.substr(insertionEndIndex + insertionEndLength);
					} else {
						//user error, so ignore it. Act like rest of line is insertion.
						insertion = contentString.substr(1);
						contentString = "";
					}
					var insertionObject:LineElement = null;
					if (insertion.indexOf("YOU") != -1 || insertion.indexOf("ME") != -1 || insertion.indexOf("MY") != -1 || insertion.indexOf("FINISHES") != -1) { //There's also YOUR, but that's already covered by checking for YOU.
						insertionObject = new VariableSubstitution(insertion);
					} else {
						var variable:Variable = getOrCreateVariable(StringFunctions.addQuotes(insertion), VariableType.UNKNOWN);
						variable.setInsertedInDialogue(true);
						insertionObject = new VariableInsertion(variable, insertion);
					}
					lineContent.push(insertionObject);
				} else {
					contentString = "";
					ErrorManager.showDevelopmentError("Line parsing infinite loop");
				}
			}
			//source:
			//1| {"style":"Him"}
			//2|
			//3|}
			//4|:"true"}
			//5|
			//6|
			
			//contentString:
			//1|"Hi!"
			//2|"Hello"
			//3|"pls"
			//4|"held"
			//5|"Coming!"
			//6|Arial
			var attributeStart:int = 0;
			var hasAttributes:Boolean = false;
			if (source.indexOf("{") != -1) {
				hasAttributes = true;
				attributeStart = source.indexOf("{");
				lineExcess[1] = source.substr(0, attributeStart);
			}
			//source:
			//1|{"style":"Him"}
			//2|
			//3|}
			//4|:"true"}
			//5|
			//6|
			var attributeString:String = "";
			if (hasAttributes) {
				var endIndex:Number = Math.max(source.lastIndexOf("{"), source.lastIndexOf("}"));
				attributeString = source.substr(attributeStart, endIndex + 1);
				source = source.substr(endIndex + 1);
			}
			//Parse attributeString
			lineAttributesString = attributeString;
			var attributeParseMode:String = "Start"; //Oh no Pim, more stringly-typed coding! What are you doing?! I'm trying to make things work, that's what I'm doing.
			var attributeName:String = "";
			var attributeValue:Object; //LineAttributes can be multiple variable references.
			var attributeName2:String = "";
			var attributeValue2:String = "";
			while (attributeString != "") {
				//I really should have included an initial_settings line in my examples.
				if (attributeParseMode == "Start") {
					if (attributeString.indexOf("{") != 0) {
						//Not sure who is to blame here. Me or the user?
						attributeString = "";
					} else {
						attributeString = attributeString.substr(1);
						attributeParseMode = "NextVar";
					}
				} else if (attributeParseMode == "NextVar") {
					//nextvar, reset.
					attributeName = "";
					attributeValue = null;
					attributeName2 = "";
					attributeValue2 = "";
					var start:int = attributeString.indexOf("\"");
					if (start != -1) {
						var end:int = attributeString.indexOf("\"", start + 1);
						attributeName = attributeString.substring(start, end + 1);
						attributeString = attributeString.substr(end + 1);
						attributeParseMode = "NextVal";
					} else {
						if (attributeString.indexOf("}") == 0) {
							attributeString = attributeString.substr(1);
						}
						//No more variables, so this is the end.
						//source = attributeString + source; //<- I had that line of code, but lineAttributesString makes it unnecessary. I think. Gotta test it first.
						attributeString = "";
					}
				} else if (attributeParseMode == "NextVal") {
					if (attributeString.indexOf(":") != 0) {
						//user error. Now what.
						//Decided to add string version of lineAttributes, so I can always detect shit like missing }.
						//Same goes for this. whatever.
						attributeString = "";
					} else {
						attributeString = attributeString.substr(1);
						if (attributeString.charAt(0) == "\"") {
							//String.
							if (attributeString.indexOf("\"", 1) != -1) {
								attributeValue = attributeString.substring(0, attributeString.indexOf("\"", 1) + 1);
								attributeString = attributeString.substring(attributeString.indexOf("\"", 1) + 1);
							} else {
								attributeValue = attributeString;
							}
							lineAttribute = new LineAttribute(attributeName, attributeValue);
							lineAttributes.push(lineAttribute);
							if (lineName == "initial_settings") {
								var variable:Variable = getOrCreateVariable(attributeName);
								if (variable.getType() == VariableType.UNKNOWN) {
									if (attributeValue == "\"false\"" || attributeValue == "\"true\"") {
										variable.setType(VariableType.BOOLEAN);
									} else {
										variable.setType(VariableType.STRING);
									}
								}
								variable.setInitialValue(attributeValue);
							}
							attributeParseMode = "NextVar";
						} else if (attributeString.indexOf("{") == 0) {
							//Array.
							attributeString = attributeString.substr(1);
							lineAttributeValueArray = new Array();
							attributeParseMode = "NextSubVar";
						} else if (attributeString.indexOf(",") != -1 || attributeString.indexOf("}") != -1) {
							//Numeric.
							var nextcomma:int = attributeString.indexOf(",");
							var nextbackaccolade:int = attributeString.indexOf("}");
							if (nextcomma != -1) {
								if (nextbackaccolade != -1 && nextbackaccolade < nextcomma) { //I don't know either.
									attributeValue = attributeString.substr(0, nextbackaccolade);
									attributeString = attributeString.substr(nextbackaccolade);
								} else {
									attributeValue = attributeString.substr(0, nextcomma);
									attributeString = attributeString.substr(nextcomma);
								}
							} else {
								attributeValue = attributeString.substr(0, nextbackaccolade);
								attributeString = attributeString.substr(nextbackaccolade);
							}
							lineAttribute = new LineAttribute(attributeName, attributeValue);
							lineAttributes.push(lineAttribute);
							if (lineName == "initial_settings") {
								var variable:Variable = getOrCreateVariable(attributeName, VariableType.NUMERIC);
								variable.setInitialValue(attributeValue);
							}
							attributeParseMode = "NextVar";
						} else {
							//Yeaahh... damn users again.
							//Unknown type, but lets register the attribute anyway.
							attributeValue = attributeString;
							attributeString = "";
							lineAttribute = new LineAttribute(attributeName, attributeValue);
							lineAttributes.push(lineAttribute);
							if (lineName == "initial_settings") {
								var variable:Variable = getOrCreateVariable(attributeName, VariableType.UNKNOWN);
								variable.setInitialValue(attributeValue);
							}
							attributeParseMode = "NextVar";
						}
					}
				} else if (attributeParseMode == "NextSubVar") {
					//"name":??? (value)
					attributeName2 = "";
					attributeValue2 = "";
					var nextSegmentArray:Array = new Array();
					nextSegmentArray.push([attributeString.indexOf("}"), "}"]);
					nextSegmentArray.push([attributeString.indexOf("\""), "\""]);
					nextSegmentArray.sortOn([0], [Array.NUMERIC]);
					var nextSegmentLocation:int = -1;
					var nextSegmentChar:String = "";
					for (var i:uint = 0, isize:uint = nextSegmentArray.length; i < isize; i++) {
						if (nextSegmentArray[i][0] != -1) {
							nextSegmentLocation = nextSegmentArray[i][0];
							nextSegmentChar = nextSegmentArray[i][1];
							i = isize;
						}
					}
					if (nextSegmentChar == "\"") {
						attributeString = attributeString.substr(nextSegmentLocation); //I hope that didn't contain anything important.
						if (attributeString.indexOf("\"") == 0) {
							if (attributeString.indexOf("\"", 1) != -1) {
								//This segment - NextSubVar name String, correct
								attributeName2 = attributeString.substr(0, attributeString.indexOf("\"", 1) + 1);
								attributeString = attributeString.substr(attributeString.indexOf("\"", 1) + 1);
							} else if (attributeString.indexOf(":", 1) != -1) {
								//This segment - NextSubVar name String, missing end double quote
								attributeName2 = attributeString.substr(0, attributeString.indexOf(":", 1));
								attributeString = attributeString.substr(attributeString.indexOf(":", 1));
							} else {
								//fuck you, learn to write dialogues for once.
								//I mean, how inept do you have to be to keep making all these mistakes - 
								//given a line like {"set":{"var":"value","var2":0}}
								//Here's where I have to watch out for...
								//{"set:{"var":"value","var2":0}}
								//{"set"{"var":"value","var2":0}}
								//{"set": {"var":"value","var2":0}}
								//{"set":{ "var":"value","var2":0}}
								//{"set":{"var""value","var2":0}}
								//{"set":{"var":value","var2":0}}
								//etc. how hard is it to just, yknow, write the line properly?
								attributeString = "";
							}
						}
						attributeParseMode = "NextSubVal";
					} else if (nextSegmentChar == "}") {
						attributeString = attributeString.substr(nextSegmentLocation + 1);
						attributeParseMode = "NextVar";
						lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
						lineAttributes.push(lineAttribute);
					}
				} else if (attributeParseMode == "NextSubVal") {
					//This segment - NextSubVal
					//Does it start with "? Find next ", that is value. Else, find next , or }
					//Does it start with something else? Find next , or }.
					if (attributeString.indexOf(":") == 0) {
						attributeString = attributeString.substr(1);
						var nextSegmentArray:Array = new Array();
						nextSegmentArray.push([attributeString.indexOf("}"), "}"]);
						nextSegmentArray.push([attributeString.indexOf("\""), "\""]);
						nextSegmentArray.push([attributeString.indexOf(","), ","]);
						nextSegmentArray.sortOn([0], [Array.NUMERIC]);
						var nextSegmentLocation:int = -1;
						var nextSegmentChar:String = "";
						for (var i:uint = 0, isize:uint = nextSegmentArray.length; i < isize; i++) {
							if (nextSegmentArray[i][0] != -1) {
								nextSegmentLocation = nextSegmentArray[i][0];
								nextSegmentChar = nextSegmentArray[i][1];
								i = isize;
							}
						}
						if (nextSegmentChar == "\"") {
							attributeValue2 = attributeString.substr(0, nextSegmentLocation);
							attributeString = attributeString.substr(nextSegmentLocation);
							if (attributeString.indexOf("\"") == 0) {
								if (attributeString.indexOf("\"", 1) != -1) {
									attributeValue2 += attributeString.substr(0, attributeString.indexOf("\"", 1) + 1);
									attributeString = attributeString.substr(attributeString.indexOf("\"", 1) + 1);
									handleSubVariableValue(attributeName, attributeName2, attributeValue2);
									lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
									
									attributeParseMode = "NextSubVar";
								} else {
									//find next comma or }
									var nextCommaIndex:int = attributeString.indexOf(",");
									var nextBackAccoladeIndex:int = attributeString.indexOf("}");
									if (nextCommaIndex != -1) {
										if (nextBackAccoladeIndex != -1 && nextBackAccoladeIndex < nextCommaIndex) {
											attributeValue2 += attributeString.substr(0, attributeString.indexOf("}"));
											attributeString = attributeString.substr(attributeString.indexOf("}") + 1);
											
											handleSubVariableValue(attributeName, attributeName2, attributeValue2);
											lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
											
											lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
											lineAttributes.push(lineAttribute);
											attributeParseMode = "NextVar";
										} else {
											attributeValue2 += attributeString.substr(0, attributeString.indexOf(","));
											attributeString = attributeString.substr(attributeString.indexOf(",") + 1);
											
											handleSubVariableValue(attributeName, attributeName2, attributeValue2);
											lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
											
											attributeParseMode = "NextSubVar";
										}
									} else if (nextBackAccoladeIndex != -1) {
										attributeValue2 += attributeString.substr(0, attributeString.indexOf("}"));
										attributeString = attributeString.substr(attributeString.indexOf("}") + 1);
										
										handleSubVariableValue(attributeName, attributeName2, attributeValue2);
										lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
										
										lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
										lineAttributes.push(lineAttribute);
										attributeParseMode = "NextVar";
									} else {
										//we fucked
										attributeValue2 += attributeString;
										attributeString = "";
										
										handleSubVariableValue(attributeName, attributeName2, attributeValue2);
										lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
										
										lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
										lineAttributes.push(lineAttribute);
										attributeParseMode = "NextVar"; //safety measure
									}
								}
							} else {
								//yeah so now what
								attributeString = "";
							}
						} else if (nextSegmentChar == ",") {
							attributeValue2 = attributeString.substr(0, attributeString.indexOf(","));
							attributeString = attributeString.substr(attributeString.indexOf(",") + 1);
							
							handleSubVariableValue(attributeName, attributeName2, attributeValue2);
							lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
							
							attributeParseMode = "NextSubVar";
						} else if (nextSegmentChar == "}") {
							attributeValue2 = attributeString.substr(0, attributeString.indexOf("}"));
							attributeString = attributeString.substr(attributeString.indexOf("}") + 1);
							
							handleSubVariableValue(attributeName, attributeName2, attributeValue2);
							lineAttributeValueArray.push(new VariableReference(attributeName2, attributeValue2));
							
							lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
							lineAttributes.push(lineAttribute);
							attributeParseMode = "NextVar";
						} else {
							//stop fucking things up, man
							attributeString = "";
						}
					} else {
						//I've had it.
						lineAttributeValueArray.push(new VariableReference(attributeName2, null));
						lineAttribute = new LineAttribute(attributeName, lineAttributeValueArray);
						lineAttributes.push(lineAttribute);
						attributeString = "";
					}
					
				}
			}
			//source:
			//1|
			//2|
			//3|}
			//4|:"true"}
			//5|
			//6|
			//atributeString:
			//1|{"style":"Him"}
			//2|
			//3|
			//4|
			//5|
			//6|
			lineExcess[2] = source;
			line = new Line(lineNumber, lineName, rawLine, lineContent, lineContentString, lineExcess, lineAttributes, lineAttributesString);
			return line;
		}
		
		private function handleSubVariableValue(lineAttributeName:String, subVariableName:String, subVariableValue:Object):void {
			if (lineAttributeName == "\"set\"") {
				addSetValueToVariable(subVariableName, subVariableValue);
			} else if (lineAttributeName == "\"check\"") {
				addCheckedValueToVariable(subVariableName, subVariableValue);
			}
		}
		
		private function addCheckedValueToVariable(variableName:String, value:Object):void {
			var variable:Variable = getVariable(variableName);
			if (variable != null) {
				variable.addCheckedValue(value);
			}
		}
		
		private function addSetValueToVariable(variableName:String, value:Object):void {
			var variable:Variable = getVariable(variableName);
			if (variable != null) {
				variable.addSetValue(value);
			}
		}
		
		public function getLinesByName(lineName:String):Array {
			var lineIndices:Array = linesDictionary[lineName];
			if (lineIndices == null) {
				return new Array();
			} else {
				var result:Array = new Array();
				for (var i:uint = 0, isize:uint = lineIndices.length; i < isize; i++) {
					var line:Line = lines[lineIndices[i]];
					if (line != null) {
						result.push(line);
					}
				}
				return result;
			}
		}
		
		public function getLineNames():Array {
			var lineNames:Array = new Array();
			var lineDict:Array = new Array();
			for (var i:uint = 0, isize:uint = lines.length; i < isize; i++) {
				var line:Line = lines[i];
				var lineName:String = line.getLineName();
				if (lineDict[lineName] == null) {
					lineNames[lineDict.length] = lineName;
					lineDict[lineName] = lineName;
				}
			}
			return lineNames;
		}
		
		public function getLineByLineNumber(lineNumber:uint):Line {
			for (var i:uint = 0, isize:uint = lines.length; i < isize; i++) {
				var line:Line = lines[i];
				if (line.getLineNumber() == lineNumber) {
					return line;
				}
			}
			return null;
		}
		
		/**
		 * DO NOT USE IN A LOOP - EATS YOUR PERFORMANCE
		 * For checkers, mostly.
		 * Make a loop, start with 0, then toss in the obtained line's lineNumber until null is recieved.
		 * @param	previousLineNumber the previous lineNumber.
		 * @return the next line with content.
		 */
		public function getNextLine(previousLineNumber:uint):Line {
			ErrorManager.showDevelopmentError("USE OF DEPRECATED FUNCTION - Dialogue::getNextLine");
			for (var i:uint = 0, isize:uint = lines.length; i < isize; i++) {
				var line:Line = lines[i];
				if (line.getLineNumber() > previousLineNumber) {
					return line;
				}
			}
			return null;
		}
		
		public function getLines():Array {
			return lines.concat(new Array());
		}
		
		public function getOrCreateVariable(variableName:String, variableType:VariableType = null):Variable {
			var variable:Variable = getVariable(variableName);
			if (variable == null) {
				if (variableType == null) {
					variableType = VariableType.UNKNOWN;
				}
				variable = new Variable(variableType, variableName);
				addVariable(variable);
			}
			return variable;
		}
		
		public function getVariable(variableName:String):Variable {
			return variables[variableName];
		}
		
		public function addVariable(variable:Variable):void {
			variables[variable.getName()] = variable;
		}
	}

}