package {
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	
	/**
	 * For keeping track of difference between HANDJOB mode and NORMAL mode
	 * For use in DialogueStates
	 * do not use for arms_back/arms_legs etc, just keep track of handjob mode yes/no
	 * @author Pimgd
	 */
	public class ArmPosition extends Enum {
		private static const thisClass:Class = ArmPosition;
		private static const enums:Array = loadValues();
		public static const NORMAL:ArmPosition = enums["NORMAL"];
		public static const HANDJOB:ArmPosition = enums["HANDJOB"];

		public function ArmPosition(val:uint, ID:String) {
			super(val, ID);
		}
		
		private static function loadValues():Array {
			var arr:Array = new Array();
			var description:XML = describeType(thisClass);
			var id:uint = 0;
			for each ( var constant:XML in description.constant) 
			{
				var constantName:String = constant.@name.toString();
				if (constantName == constantName.toUpperCase()) {
					arr[constantName] = new ArmPosition(id++, constantName);
				}
			}
			return arr;
		}
		
		public static function getAllEnums():Array {
			var tempArray:Array = new Array();
			for each (var armPosition:ArmPosition in enums) {
				tempArray[armPosition.getValue()] = armPosition;
			}
			return tempArray;
		}
	}

}