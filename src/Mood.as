package {
	/**
	 * Represents a mood that a character can have in SDT.
	 * Moods affect how a character behaves. This has been extended to dialogue functions as well.
	 * Dialogue writers can check for moods to allow a character to act differently on different situations.
	 * This enumeration represents all the possible moods that SDT has.
	 * @author Pimgd
	 */
	public class Mood extends Enum {
		private static const enums:Array = Enum.loadValuesForEnum(Mood);
		public static const NORMAL:Mood = enums["NORMAL"];
		public static const ANGRY:Mood = enums["ANGRY"];
		public static const HAPPY:Mood = enums["HAPPY"];
		public static const AHEGAO:Mood = enums["AHEGAO"];
		
		public function Mood(val:uint, ID:String) {
			super(val, ID);
		}
		
		public static function fromString(id:String):Mood {
			return Mood(enums[id]);
		}
		
		public static function hasEnum(id:String):Boolean {
			return fromString(id) != null;
		}
		
		public static function fromMoodAttributeValue(value:String):Mood {
			if (value == null) {
				return null;
			}
			
			if (StringFunctions.stringStartsWith(value, "\"") && StringFunctions.stringEndsWith(value, "\"")) {
				value = value.substring(1, value.length - 1);
			}
			var moods:Array = getAllEnums();
			for (var i:uint = 0; i < moods.length; i++) {
				if (moods[i].getIDStandardCapitalization() == value) {
					return moods[i];
				}
			}
			return null;
		}
		
		public static function fromTrigger(trigger:Trigger):Mood {
			if (trigger != null && trigger.getType() == TriggerType.MOOD) {
				return fromString(trigger.getAction().split("_")[0]);
			}
			return null;
		}
		
		public static function getAllEnums():Array {
			var tempArray:Array = new Array();
			for each (var mood:Mood in enums) {
				tempArray[mood.getValue()] = mood;
			}
			return tempArray;
		}
		
		public function getIDStandardCapitalization():String {
			var ownID:String = getID();
			ownID = ownID.toLowerCase();
			ownID = ownID.charAt(0).toUpperCase() + ownID.substr(1);
			return ownID;
		}
	}

}