package  
{
	/**
	 * For Dialogue traversal. Currently unused.
	 * @author Pimgd
	 */
	public class DialogueState 
	{
		/**
		 * If this is false, moods will be taken into consideration.
		 * Else I'll just assume that everything can be everything.
		 */
		private var requiredUserIntervention:Boolean = false;
		private var atLine:Line;
		private var mood:Mood;
		private var leftArm:ArmPosition;
		private var rightArm:ArmPosition;
		private var variableValues:Array;
		public function DialogueState() 
		{
			variableValues = new Array();
		}
		
		public function setUserInterventionRequirement(neededUser:Boolean):void {
			requiredUserIntervention = neededUser;
		}
		
		public function isUserInterventionRequired():Boolean {
			return requiredUserIntervention;
		}
		
		public function getMood():Mood {
			return mood;
		}
		
		public function setMood(m:Mood):void {
			mood = m;
		}
		
		public function setArms(position:ArmPosition):void {
			setLeftArmPosition(position);
			setRightArmPosition(position);
		}
		public function setLeftArmPosition(position:ArmPosition):void {
			leftArm = position;
		}
		public function setRightArmPosition(position:ArmPosition):void {
			rightArm = position;
		}
		
		public function getLeftArmPosition():ArmPosition {
			return leftArm;
		}
		
		public function getRightArmPosition():ArmPosition {
			return rightArm;
		}
		
		public function getLine():Line {
			return atLine;
		}
		
		public function setLine(l:Line):void {
			atLine = l;
		}
		
		public function getVariableValues():Array {
			return variableValues;
		}
		
		public function setVariableValues(values:Array):Array {
			variableValues = values.concat();//Can't have modifications in an Ant alter history
		}
		
		public function equalsState(state:DialogueState):Boolean {
			if (this === state) {
				return true;//JAVA style, o.equals(o) is always true. 
			}
			
			if (isUserInterventionRequired() != state.isUserInterventionRequired()) {
				return false;
			}
			
			//TODO: finish this
			
			return true;
		}
		
	}

}