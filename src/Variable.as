package  
{
	/**
	 * Represents a variable in a Dialogue for SDT.
	 * The variable class contains the name and type for a variable,
	 * as well as the values the variable can have.
	 * 
	 * For checkers:
	 * A variable can obtain a value that is never checked for, 
	 * but a variable may never be checked for a value it cannot achieve.
	 * @author Pimgd
	 */
	public class Variable 
	{
		private var type:VariableType;
		private var name:String;
		private var setValues:Array;//first value should be initial value.
		private var checkedValues:Array;
		private var insertedInDialogue:Boolean = false;
		public function Variable(variableType:VariableType, variableName:String) 
		{
			type = variableType;
			name = variableName;
			setValues = new Array();
			checkedValues = new Array();
		}
		public function getName():String {
			return name;
		}
		public function getType():VariableType {
			return type;
		}
		public function setType(varType:VariableType):void {
			type = varType;
		}
		public function addSetValue(value:Object):void {
			setValues.push(value);
		}
		public function addCheckedValue(value:Object):void {
			checkedValues.push(value);
		}
		public function isInsertedInDialogue():Boolean {
			return insertedInDialogue;
		}
		public function setInsertedInDialogue(b:Boolean):void {
			insertedInDialogue = b;
		}
		public function setInitialValue(value:Object):void{
			var temp:Object = setValues[0];
			setValues[0] = value;
			if (temp != null) {
				setValues.push(temp);
			}
		}
		
		public function getInitialValue():Object {
			return setValues[0];
		}
		
		public function getExplicitUniqueValues():Array {
			var strippedValues:Array = new Array();
			for (var i:uint = 0, isize:uint = setValues.length; i < isize; i++) {
				var variableValue:String = setValues[i];
				if (!isSubstractionValue(variableValue) && !isAdditionValue(variableValue)) {
					strippedValues.push(StringFunctions.escapeRegexChars(StringFunctions.stripQuotesIfNeeded(variableValue)));
				}
			}
			
			var strippedAndFiltered:Array = new Array();
			for (var i:uint = 0, isize:uint = strippedValues.length; i < isize; i++) {
				ArrayFunctions.addIfNotExists(strippedAndFiltered, strippedValues[i]);
			}
			
			return strippedAndFiltered;
		}
		
		public function getRegexOfPossibleValues():String {
			var includeNumeric:Boolean = false;
			if (containsAdditionValue() || containsSubstractionValue()) {
				includeNumeric = true;
			}
			
			var values:Array = getExplicitUniqueValues();
			
			var regexp:String = "(";
			for (var i:uint = 0, isize:uint = values.length; i < isize; i++) {
				if (i != 0) {
					regexp += "|";
				}
				regexp += values[i];
			}
			if (includeNumeric) {
				if (regexp != "(" ) {
					regexp += "|";
				}
				regexp += "(0|((-?[1-9])\\d*))";
			}
			regexp += ")";
			return regexp;
		}
		
		
		
		public function containsAdditionValue():Boolean {
			for (var i:uint = 0, isize:uint = setValues.length; i < isize; i++) {
				var variableValue:String = setValues[i];
				if (variableValue.indexOf("\"+") == 0) {
					return true;
				}
			}
			return false;
		}
		public function containsSubstractionValue():Boolean {
			for (var i:uint = 0, isize:uint = setValues.length; i < isize; i++) {
				var variableValue:String = setValues[i];
				if (variableValue.indexOf("\"-") == 0) {
					return true;
				}
			}
			return false;
		}
		
		private function isSubstractionValue(value:String):Boolean {
			if (value.indexOf("\"-") == 0) {
				return true;
			}
			return false;
		}
		
		private function isAdditionValue(value:String):Boolean {
			if (value.indexOf("\"+") == 0) {
				return true;
			}
			return false;
		}
	}

}