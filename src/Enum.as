package {
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	
	/**
	 * Base class for Enumerations. 
	 * @author Pimgd
	 */
	public class Enum {
		private var value:uint;
		private var id:String;
		
		public function Enum(val:uint, ID:String) {
			value = val;
			id = ID;
		}
		
		private static function validateClassIsSubclassOf(classToCheck:Class, baseClass:Class):Boolean {
			//http://stackoverflow.com/a/6681302/540837
			var qn:String = getQualifiedClassName(classToCheck);
			var en:String = getQualifiedClassName(baseClass);
			if (qn != en) {
				qn = getQualifiedSuperclassName(classToCheck);//fix case where class isn't constructed yet?
			}
			while(qn != "Object") {
				if (qn == en) return true;
				qn = getQualifiedSuperclassName(getDefinitionByName(qn));
			}
			return false;
		}
		
		public static function loadValuesForEnum(enumClass:Class):Array {
			if (!validateClassIsSubclassOf(enumClass, Enum)) {
				ErrorManager.showDevelopmentError("loadValuesForEnum(" + getQualifiedClassName(enumClass) + ") failed - provided class is not a subclass of Enum!");
				return new Array();
			}
			
			var description:XML = describeType(enumClass);
			var arr:Array = new Array();
			var id:uint = 0;
			for each ( var constant:XML in description.constant) 
			{
				var constantName:String = constant.@name.toString();
				if (constantName == constantName.toUpperCase()) {
					arr[constantName] = new enumClass(id++, constantName);
				}
			}
			
			return arr;
		}
		
		public function getValue():uint {
			return value;
		}
		
		public function getID():String {
			return id;
		}
	
	}

}