package {
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class DialogueChecker {
		private var options:Options;
		private var dialogue:Dialogue;
		private var checkers:Vector.<Checker>;
		private var dependencies:Array; //of Dependency
		private var timingData:Array; //string keys
		private var importedSettings:Array; //of ImportSettingsFile || ErrorString, string indexed?
		
		public function DialogueChecker() {
			checkers = new Vector.<Checker>();
			dependencies = new Array();
			importedSettings = new Array();
		}
		
		public function checkDialogueObject(d:Dialogue, o:Options):Array {
			options = o;
			dialogue = d;
			checkDefines(dialogue.getRawDialogue());
			//???
			return checkDialogue();
		}
		
		public function checkDialogueString(s:String, o:Options):Array {
			options = o;
			checkDefines(s);
			dialogue = new Dialogue(s);
			//???
			return checkDialogue();
		}
		
		public function checkDefines(dialogueText:String):void {
			options.setSetting("IgnoreObjects", dialogueText.indexOf("#DialogueChecker-IgnoreObjects#") != -1, true);
			options.setSetting("MeasureTime", dialogueText.indexOf("#DialogueChecker-DebugTime#") != -1, true);
			options.setSetting("NoDoubleSpace", dialogueText.indexOf("#DialogueChecker-NoDoubleSpace#") != -1, true);
			options.setSetting("NoLeadingSpace", dialogueText.indexOf("#DialogueChecker-NoLeadingSpace#") != -1, true);
			options.setSetting("NoIllegalColon", dialogueText.indexOf("#DialogueChecker-NoIllegalColon#") != -1, true);
			options.setSetting("NoLineTruncation", dialogueText.indexOf("#DialogueChecker-NoLineTruncation#") != -1, true);
		}
		
		public function importSettingsFromFile(path:String):void {
			var url:URLRequest = new URLRequest(path);
            var loader:URLLoader = new URLLoader();
			loader.load(url);
			loader.addEventListener(Event.COMPLETE, loaderComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loaderError);
			function loaderComplete(e:Event):void
			{
				var importFile:ImportSettingsFile = null;
				var jsonObject:Object = null;
				try 
				{
					jsonObject = JSON.parse(loader.data);
					importFile = new ImportSettingsFile(jsonObject);
					importedSettings[path] = importFile;
					importFile.applySettingsBlindly();
				} 
				catch (err:Error)
				{
					importedSettings[path] = err.message;
				}
			}
			
			function loaderError(e:Event):void 
			{
				importedSettings[path] = e.toString();
			}
		}
		
		private function checkDialogue():Array {
			var issues:Array = new Array(); //of issues
			timingData = new Array();
			//Hand dialogue to checkers
			//Then retrieve issues from checkers
			//Sort them, maybe?
			checkers = new Vector.<Checker>();
			dependencies = new Array();
			if (options.hasChecker("Syntax")) {
				checkers.push(new SyntaxChecker(this));
			}
			if (options.hasChecker("Grammar")) {
				checkers.push(new GrammarChecker(this));
			}
			if (options.hasChecker("Variables")) {
				checkers.push(new VariableChecker(this));
			}
			if (options.hasChecker("LineUsage")) {
				checkers.push(new LineUsageChecker(this));
			}
			
			var isize:uint = checkers.length;
			for (var i:uint = 0; i < isize; i++) {
				var checker:Checker = checkers[i];
				var startTime:Number = new Date().valueOf();
				checker.checkDialogue(dialogue, options);
				var endTime:Number = new Date().valueOf();
				timingData[checker.getName()] = endTime - startTime;
			}
			var startTime:Number = new Date().valueOf();
			var lines:Array = dialogue.getLines();
			for each (var line:Line in lines) {
				var lineNumber:uint = line.getLineNumber();
				for (var i:uint = 0; i < isize; i++) {
					issues = issues.concat(checkers[i].getIssuesForLineNumber(lineNumber));
				}
			}
			var endTime:Number = new Date().valueOf();
			timingData["DialogueChecker"] = endTime - startTime;
			if (!options.getSetting("MeasureTime")) {
				timingData = new Array();
			}
			options.clearSingleUseSettings();
			return issues;
		}
		
		public function getTimingDataOfLastRun():Array {
			return timingData;
		}
		
		public function addTriggerDependency(dependency:String, trigger:String):void {
			getOrCreateDependency(dependency).addTrigger(trigger);
		}
		
		public function addLineDependency(dependency:String, line:String):void {
			getOrCreateDependency(dependency).addLine(line);
		}
		
		public function addVariableDependency(dependency:String, variable:String):void {
			getOrCreateDependency(dependency).addVariable(variable);
		}
		
		public function addStyleAttributeValueDependency(dependency:String, attributeValue:String):void {
			getOrCreateDependency(dependency).addStyleAttributeValue(attributeValue);
		}
		
		private function getOrCreateDependency(dependency:String):Dependency {
			if (dependencies[dependency] == null) {
				dependencies[dependency] = new Dependency(dependency);
			}
			return dependencies[dependency];
		}
		
		public function getDialogueVariable(variable:String):Variable {
			return dialogue.getVariable(variable);
		}
		
		public function printDependencies():String {
			var output:String = "";
			
			for (var dependencyName:String in dependencies) {
				var depString:String = dependencies[dependencyName].printDependency(options);
				if (depString != "") {
					output += options.getLineBreak() + depString;
				}
			}
			
			if (output != "") {
				output = options.getLineBreak() + "By the way, your dialogue makes use of the following dialogue mods:" + output;
				if (output.indexOf("VariableArithmetic") != -1 && output.indexOf("DialogueActions") != -1) {
					output += options.getLineBreak() + "VariableArithmetic has been integrated in DialogueActions from DialogueActions v2.00+ onward.";
				}
				output += options.getLineBreak() + "Don't forget to mention these dependencies when you release your dialogue!";
			}
			
			return output;
		}
		
		public function hasImports():Boolean {
			//hasAny
			for (var unused:* in importedSettings) {
				return true;
			}
			return false;
		}
		
		public function printImports(o:Options):String {
			var output:String = "";
			
			var importedSettingsSize:uint = 0;
			
			for (var path:String in importedSettings) {
				var importSetting:* = importedSettings[path];
				var importString:String = "";
				if (importSetting is String) {
					importString = path + " failed to load (" + importSetting + ")";
				} else {
					importString = path + " - " + importSetting.printDetails(o);
				}
				if (importString != "") {
					output += o.getLineBreak() + importString;
				}
				importedSettingsSize++;
			}
			
			if (output != "") {
				output = o.getLineBreak() + "Additionally, via the following "+importedSettingsSize+" imported files, also supported are... " + output;
			}
			
			return output;
		}
	}

}