package {
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class CheckBox extends Sprite {
		private var xLocation:Number;
		private var yLocation:Number;
		private var onScreen:Boolean;
		private var checked:Boolean;
		
		public function CheckBox(xLoc:Number, yLoc:Number) {
			xLocation = xLoc;
			yLocation = yLoc;
			onScreen = false;
			checked = false;
		}
		
		public function handleClickEvent(e:MouseEvent = null):void {
			checked = !checked;
			draw();
		}
		
		public function setChecked(b:Boolean):void {
			checked = b;
			if (onScreen) {
				draw();
			}
		}
		
		public function getChecked():Boolean {
			return checked;
		}
		
		public function isChecked():Boolean {
			return getChecked();
		}
		
		public function getAnswer():String {
			if (checked) {
				return "1";
			} else {
				return "0";
			}
		}
		
		public function show(s:DisplayObjectContainer):void {
			if (!onScreen) {
				s.addChild(this);
				//draw shit
				draw();
				addEventListener(MouseEvent.CLICK, handleClickEvent);
				onScreen = true;
				updatePosition();
			}
		}
		
		public function hide(s:DisplayObjectContainer):void {
			if (onScreen) {
				//remove shit
				graphics.clear();
				removeEventListener(MouseEvent.CLICK, handleClickEvent);
				onScreen = false;
				s.removeChild(this);
			}
		}
		
		private function draw():void {
			graphics.clear();
			graphics.lineStyle(2, 0x000000);
			graphics.moveTo(10, 10);
			graphics.beginFill(0xFFFFFF);
			graphics.drawRect(10, 10, 20, 20);
			graphics.endFill();
			if (checked) {
				graphics.lineStyle(5, 0x009900);
				graphics.moveTo(35, 5);
				graphics.lineTo(18, 28);
				graphics.lineTo(5, 13);
			}
		}
		
		public function updatePosition():void {
			if (onScreen) {
				if (!stage) {
					ErrorManager.showDevelopmentError("CheckBox::updatePosition():void - function called, object is on screen, but does not have a stage reference");
				}
				this.x = xLocation;
				this.y = yLocation;
			}
		}
		
		public function setX(xLoc:Number):void {
			xLocation = xLoc;
			updatePosition();
		}
		
		public function setY(yLoc:Number):void {
			yLocation = yLoc;
			updatePosition();
		}
		
		public function getX():Number {
			return xLocation;
		}
		
		public function getY():Number {
			return yLocation;
		}
	}

}