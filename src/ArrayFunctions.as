package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class ArrayFunctions {
		
		public function ArrayFunctions() {
		
		}
		
		public static function getOrCreateSubArray(array:Array, key:*):Array {
			if (array[key] == undefined) {
				array[key] = new Array();
			}
			return array[key];
		}
		
		public static function addIfNotExists(array:Array, toAdd:Object):Boolean {
			if (array.indexOf(toAdd) == -1) {
				array.push(toAdd);
				return true;
			}
			return false;
		}
		
		public static function remove(array:Array, toRemove:Object):Boolean {
			var index:int = array.indexOf(toRemove);
			if (index != -1) {
				array.splice(index, 1);
			}
			return index != -1;
		}
		
		public static function removeAll(array:Array, toRemove:Array):Array {
			var removedElements:Array = new Array();
			for each (var element:* in toRemove) {
				if (remove(array, element)) {
					removedElements[removedElements.length] = element;
				}
			}
			return removedElements;
		}
	
	}

}