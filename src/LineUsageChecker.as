package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class LineUsageChecker extends Checker {
		private var lineNames:Array; //of String - for Line Usage checking (whether a line is used)
		private var referencedLineNames:Array; //of String
		private var unusedLineNames:Array; //of String
		private var unusedReferencedLineNames:Array; //of String
		private var moodsSet:Array; //of Mood
		private var currentRegex:RegExp; //For line usage checking.
		private var builtRegexes:Array; //performance boosteer
		
		private var finishes:Array; //of String - for checking duplicates in finishes
		
		//performance booster?
		private var lineNameSet:Array;
		private var refLineNameSet:Array;
		
		public function LineUsageChecker(dc:DialogueChecker) {
			super(dc);
		}
		
		public override function getName():String {
			return "LineUsageChecker";
		}
		
		public override function checkDialogue(dialogue:Dialogue, options:Options):void {
			lineNameSet = new Array();
			refLineNameSet = new Array();
			
			lineNames = new Array();
			referencedLineNames = new Array();
			moodsSet = new Array();
			finishes = new Array();
			var lines:Array = dialogue.getLines();
			for each (var line:Line in lines) {
				scanLine(line, dialogue, options);
			}
			doTheMath();
			for each (var line:Line in lines) {
				checkLine(line, dialogue, options);
			}
		}
		
		private function doTheMath():void {
			//Duplicate lineNames into unusedLineNames
			unusedLineNames = lineNames.concat(); //cloning FTW
			//And duplicate referencedLineNames into unusedReferencedLineNames
			unusedReferencedLineNames = referencedLineNames.concat();
			
			//Clear the list of cached regexes
			builtRegexes = new Array();
			
			//go through the referencedLineNames first. For each that has matches in lineNames, cross off all matches in unusedReferencedLineNames
			for (var i:uint = 0, isize:uint = referencedLineNames.length; i < isize; i++) {
				var refLineName:String = referencedLineNames[i];
				
				var matches:Array = determineMatchesForLine(refLineName, lineNames);
				ArrayFunctions.removeAll(unusedLineNames, matches);
				
				//If a referencedLineName had matches, cross it off in unusedReferencedLineNames.
				if (matches.length != 0) {
					ArrayFunctions.remove(unusedReferencedLineNames, refLineName);
				}
			}
			
			//Finally, cross off all vanilla linenames and modded ones from unusedLineNames.
			for (var k:int = unusedLineNames.length - 1; k >= 0; k--) {
				var unusedLineName:String = unusedLineNames[k];
				if (Data.linesSDTVanilla.indexOf(unusedLineName) != -1) {
					unusedLineNames.splice(k, 1);
				} else if (unusedLineName.indexOf("restart") == 0) {
					//Whatever. I need to check this properly one day but I'm sure it's okay.
					unusedLineNames.splice(k, 1);
				} else if (unusedLineName.indexOf("finish") == 0) {
					//Same here. I'm sure it's okay. Secretly, I worry, but hey...
					unusedLineNames.splice(k, 1);
				}
			}
			
			//filter the lines from each mod, and add dependencies
			filterModLinesViaLineDependencyMap(Data.lineDependencyMap);
			filterModLinesViaLineDependencyMap(Data.customLines);
		}
		
		private function filterModLinesViaLineDependencyMap(lineDependencyMap:Object):void {
			for (var modName:String in lineDependencyMap) {
				filterModLines(unusedLineNames, lineDependencyMap[modName], modName);
			}
		}
		
		private function filterModLines(unusedLineNames:Array, linesFromMod:Array, modName:String):void {
			for (var i:int = unusedLineNames.length - 1; i >= 0; i--) {
				var unusedLineName:String = unusedLineNames[i];
				if (linesFromMod.indexOf(unusedLineName) != -1) {
					unusedLineNames.splice(i, 1);
					dialogueChecker.addLineDependency(modName, unusedLineName);
				}
			}
		}
		
		private function doesLineSetBackgroundVariableInSetAttribute(l:Line):Boolean {
			var lineAttributeSet:LineAttribute = l.getLineAttribute("\"set\"");
			if (lineAttributeSet != null) {
				var variableReferences:Array = lineAttributeSet.getValue() as Array;
				if (variableReferences != null) {
					for (var j:uint = 0, jsize:uint = variableReferences.length; j < jsize; j++) {
						var variableReference:VariableReference = variableReferences[j];
						if (variableReference.getName() == "\"background\"" || variableReference.getName() == "\"da.background.load\"") {
							return true;
						}
					}
				}
				
			}
			return false;
		}
		
		private function checkLine(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			//Identify lineName, if in unusedLineNames, create issue - unused line
			if (l.isCommentLine()) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			var linebreak:String = Options.DEFAULT_LINEBREAK;
			if (options != null) {
				linebreak = options.getLineBreak();
			}
			var issue:Issue = null;
			var issueMessage:String = "";
			if (unusedLineNames.indexOf(l.getLineName()) != -1) {
				issueMessage = "Line " + l.getLineNumber() + " is unused!" + linebreak;
				issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
				issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
				addIssue(issue);
				issueMessage = "";
			}
			
			var lineName:String = l.getLineName();
			if (lineName.indexOf("finish") == 0) {
				if (finishes[lineName] == null) {
					finishes[lineName] = l.getLineNumber();
				} else {
					issueMessage = "Duplicate finish line (" + lineName + " was first declared on line " + finishes[lineName] + ") on line " + l.getLineNumber() + ":" + linebreak;
					issueMessage += "Duplicate finish lines will not be loaded properly. Use a trigger to a normal line instead if you're using finishes for an entire line." + linebreak;
					issueMessage += "If you're using finishes to insert part of a line, consider making a callback function that sets a variable to one of your linesections." + linebreak;
					issueMessage += "Then you could insert that variable in your finish line." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			if (lineName == "passed_out") {
				var styleAttribute:LineAttribute = l.getLineAttribute("\"style\"");
				if (styleAttribute != null) {
					var styleValue:String = styleAttribute.getStrippedValue();
					if (styleValue != null && (styleValue == "thought" || styleValue == "speak" || styleValue == "Thought" || styleValue == "Speak")) {
						issueMessage += "Line " + l.getLineNumber() + " uses the style " + styleValue+", but only plays when she's passed out. This can never happen and thus the line can't be played." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage")
						addIssue(issue);
						issueMessage = "";
					}
				} else {
					issueMessage += "Line " + l.getLineNumber() + " uses the default style, Speak, but only plays when she's passed out. This can never happen and thus the line can't be played." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage")
					addIssue(issue);
					issueMessage = "";
				}
			}
			
			//Access triggers, including those in VariableSubstitutions
			var content:Array = l.getContent();
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var contentPiece:Object = content[i];
				if (contentPiece is String) {
					//var contentString:String = contentPiece as String; //boring
				} else if (contentPiece is Trigger) {
					var contentTrigger:Trigger = contentPiece as Trigger;
					var triggerAction:String = contentTrigger.getAction();
					if (contentTrigger.getType() == TriggerType.LINEREFERENCE) {
						if (unusedReferencedLineNames.indexOf(triggerAction) != -1 && !triggerActionIsANonLineReferenceTriggerViaVariableInsertion(triggerAction)) {
							issueMessage = "A trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + " refers to a non-existing line." + linebreak;
							if (contentTrigger.getAction() == "") {
								issueMessage += "Empty triggers will trigger a line without a linename. The use of lines without a linename is possible, but very bad coding/writing style." + linebreak;
								issueMessage += "It's also possible that you accidentally cut a trigger short. Take a good look at the line." + linebreak;
							} else {
								var fileLineNumberOfLineReference:int = dialogue.getFileLineNumberWhereLineStartsWith(triggerAction + ":");
								if (fileLineNumberOfLineReference != -1) {
									issueMessage += "There is at least one line (Line " + fileLineNumberOfLineReference + ") that seems to be the line this trigger is looking for, but it didn't parse correctly." + linebreak;
									issueMessage += "Please refer to the issues found for that line to fix the problem." + linebreak;
									issueMessage += "Referenced Line (Line " + fileLineNumberOfLineReference + "): " + dialogue.getFileLineByLineNumber(fileLineNumberOfLineReference) + linebreak;
								}
								
								var capitalizedAction:String = triggerAction.toUpperCase();
								if (TriggerType.isSimpleTrigger(capitalizedAction)) {
									if (fileLineNumberOfLineReference != -1) {
										issueMessage += "Additionally... ";
									}
									issueMessage += "Perhaps you meant [" + capitalizedAction + "]?" + linebreak;
								}
							}
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
							addIssue(issue);
							issueMessage = "";
						}
					} else if (contentTrigger.getType() == TriggerType.CUSTOM) {
						var mod:String = contentTrigger.getType().getDependentMod(triggerAction);
						if (mod != "") {
							dialogueChecker.addTriggerDependency(mod, triggerAction);
						}
						if (triggerAction.indexOf("FADE_BACKGROUND_") == 0 || triggerAction.indexOf("CHANGE_BACKGROUND") == 0) {
							var hasBGvarSet:Boolean = false;
							hasBGvarSet = doesLineSetBackgroundVariableInSetAttribute(l);
							if (!hasBGvarSet) {
								for (var j:uint = 0, jsize:uint = i; j < jsize; j++) {
									var contentPiece2:Object = content[j];
									if (contentPiece2 is Trigger) {
										var contentTrigger2:Trigger = contentPiece2 as Trigger;
										if (StringFunctions.stringStartsWith(contentTrigger2.getAction(), "SETVAR")) {
											if (contentTrigger2.getArgument(1, 1) == "da.background.load") {
												hasBGvarSet = true;
												break;
											}
										}
									}
								}
							}
							if (!hasBGvarSet) {
								issueMessage = "A trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + " requires the variable 'background' (DA v1) or 'da.background.load' (DA v2.00+) to be set, but the variable hasn't been set." + linebreak;
								issueMessage += "As a result of this, the background will not change when this trigger is triggered." + linebreak;
								if (triggerAction.indexOf("FADE_BACKGROUND_") == 0) {
									issueMessage += "If you don't want a background change, but just want a fade effect, use the FLASH_<hex> trigger instead." + linebreak;
								}
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
								addIssue(issue);
								issueMessage = "";
							}
						} else if (triggerAction == "LOAD_CHARCODE" || triggerAction == "LOAD_CHARCODE2" || triggerAction == "LOAD_FULL_CHARCODE") {
							var hasCharCodevarSet:Boolean = false;
							var lineAttributeSet:LineAttribute = l.getLineAttribute("\"set\"");
							if (lineAttributeSet != null) {
								var variableReferences:Array = lineAttributeSet.getValue() as Array;
								if (variableReferences != null) {
									for (var j:uint = 0, jsize:uint = variableReferences.length; j < jsize; j++) {
										var variableReference:VariableReference = variableReferences[j];
										if (variableReference.getName() == "\"loadCharCode\"" || variableReference.getName() == "\"da.charcode.load\"") {
											hasCharCodevarSet = true;
											j = jsize;
										}
									}
								}
								
							}
							hasCharCodevarSet = hasCharCodevarSet || lineSetsCharCodeViaTrigger(l.getRaw());
							
							if (!hasCharCodevarSet) {
								issueMessage = "A trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + " requires the variable 'loadCharCode' (DA v1) or 'da.charcode.load' (DA v2.00+) to be set, but the variable hasn't been set." + linebreak;
								issueMessage += "As a result of this, no changes should occur when this trigger is triggered." + linebreak;
								issueMessage += "However, due to the buggy nature of loadCharCode, it is advised you do not make unneeded use of [LOAD_CHARCODE]." + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MINOR, l, "", issueMessage, "Line Usage");
								addIssue(issue);
								issueMessage = "";
							}
						}
					} else if (contentTrigger.getType() == TriggerType.DEPRECATED) {
						var mod:String = contentTrigger.getType().getDependentMod(triggerAction);
						if (mod != "") {
							issueMessage = "A trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + " is deprecated! It used to be part of " + mod + "." + linebreak;
							issueMessage += "Deprecated triggers might not function properly or at all. I suggest you look at the documentation of " + mod + " and replace the trigger with other triggers to achieve the same functionality." + linebreak;
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							issue = new Issue(Severity.MINOR, l, "", issueMessage, "Line Usage");
							addIssue(issue);
							issueMessage = "";
						}
					}
				} else if (contentPiece is VariableInsertion) {
					//var contentVariableInsertion:VariableInsertion = contentPiece as VariableInsertion; //also boring
				} else if (contentPiece is VariableSubstitution) {
					var contentVariableSubstitution:VariableSubstitution = contentPiece as VariableSubstitution;
					if (contentVariableSubstitution.containsTrigger()) {
						var VSlineNames:Array = resolveVSToLineNames(contentVariableSubstitution);
						for (var j:uint = 0, jsize:uint = VSlineNames.length; j < jsize; j++) {
							if (unusedReferencedLineNames.indexOf(VSlineNames[j]) != -1) {
								issueMessage = "A trigger ([" + VSlineNames[j] + "]) in a variable substitution (" + contentVariableSubstitution.getLineSegment() + ") on line " + l.getLineNumber() + " refers to a non-existing line." + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
								addIssue(issue);
								issueMessage = "";
							}
						}
					}
				}
			}
			
			//Access next line-attribute and add it as a lineusage.
			var nextLineAttribute:LineAttribute = l.getLineAttribute("\"next\""); //LineAttributes are stored in "name" format, due to parsing and checking opportunties.
			if (nextLineAttribute != null) {
				var nextLineValue:String = nextLineAttribute.getValue() as String;
				if (nextLineValue == null) {
					issueMessage = "The next line-attribute on line " + l.getLineNumber() + " uses incorrect syntax." + linebreak;
					issueMessage += "Correct syntax is {\"next\":\"LINENAME\"} where LINENAME is the name of the line that you wish to trigger next." + linebreak;
					issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
					issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Syntax");
					addIssue(issue);
					issueMessage = "";
				} else if (StringFunctions.isQuoted(nextLineValue)) {
					nextLineValue = StringFunctions.stripQuotesIfNeeded(nextLineValue);
					if (unusedReferencedLineNames.indexOf(nextLineValue) != -1) {
						issueMessage = "The next line-attribute (value: " + nextLineValue + ") on line " + l.getLineNumber() + " refers to a non-existing line." + linebreak;
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, "", issueMessage, "Line Usage");
						addIssue(issue);
						issueMessage = "";
					}
				}
			}
			
			//Check mood line attribute for references to a mood that I don't have set in the dialogue
			var moodLineAttribute:LineAttribute = l.getLineAttribute("\"mood\""); //LineAttributes are stored in "name" format, due to parsing and checking opportunties.
			if (moodLineAttribute != null) {
				var moodString:String = moodLineAttribute.getStrippedValue().toUpperCase();
				if (Mood.hasEnum(moodString)) { //preventing user values from crashing the checker
					var mood:Mood = Mood.fromString(moodString);
					if (mood != null && moodsSet.indexOf(mood) == -1) {
						issueMessage = "Line " + l.getLineNumber() + " could be inaccessible - the dialogue does not contain the [" + mood.getID() + "_MOOD] trigger, yet this line requires a " + mood.getID().toLowerCase() + " mood to trigger." + linebreak;
						if (mood == Mood.NORMAL) {
							issueMessage += "If you are checking for normal mood, but don't want to return to normal mood, it seems like you're trying to split your dialogue up in multiple phases." + linebreak;
							issueMessage += "Consider using a variable for this instead, so you can use the mood for expression of emotion rather than state-progression." + linebreak;
						}
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MINOR, l, "", issueMessage, "Line Usage");
						addIssue(issue);
						//issueMessage = "";
					}
				}
			}
		}
		
		private function lineSetsCharCodeViaTrigger(line:String):Boolean {
			if (line.indexOf("loadCharCode") != -1) {
				if (line.indexOf("[VA_SET_VARIABLE_loadCharCode_") != -1 
				|| line.indexOf("[VA_SET_VARIABLEBYNAME_loadCharCode_") != -1) {
					return true;
				}
			}
			if (line.indexOf("da.charcode.load") != -1) {
				if (line.indexOf("[VA_SET_VARIABLE_da.charcode.load_") != -1
				|| line.indexOf("[VA_SET_VARIABLEBYNAME_da.charcode.load_") != -1
				|| line.indexOf("[SETVAR_da.charcode.load_") != -1
				|| line.indexOf("[SETVARBYNAME_da.charcode.load_") != -1
				|| line.indexOf("[SETVARBYNAEM_da.charcode.load_") != -1) {
					return true;
				}
			}
			return false;
		}
		
		private function scanLine(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			//Identify lineName, if new, add to array
			if (l.isCommentLine()) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			addLineName(l.getLineName());
			
			//Access triggers, including those in VariableSubstitutions
			var content:Array = l.getContent();
			var lastMoodTrigger:Trigger = null;
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var contentPiece:Object = content[i];
				if (contentPiece is String) {
					//var contentString:String = contentPiece as String; //boring
				} else if (contentPiece is Trigger) {
					var contentTrigger:Trigger = contentPiece as Trigger;
					if (contentTrigger.getType() == TriggerType.LINEREFERENCE) {
						addReferencedLineName(contentTrigger.getAction());
					} else if (contentTrigger.getType() == TriggerType.MOOD) {
						lastMoodTrigger = contentTrigger;
					}
				} else if (contentPiece is VariableInsertion) {
					//var contentVariableInsertion:VariableInsertion = contentPiece as VariableInsertion; //also boring
				} else if (contentPiece is VariableSubstitution) {
					var contentVariableSubstitution:VariableSubstitution = contentPiece as VariableSubstitution;
					if (contentVariableSubstitution.containsTrigger()) {
						var VSlineNames:Array = resolveVSToLineNames(contentVariableSubstitution);
						for (var j:uint = 0, jsize:uint = VSlineNames.length; j < jsize; j++) {
							addReferencedLineName(VSlineNames[j]);
						}
					}
				}
			}
			
			//if we found a mood trigger in this line, add it to the moods set
			if (lastMoodTrigger != null) {
				var moodString:String = lastMoodTrigger.getAction().toUpperCase();
				if (StringFunctions.stringEndsWith(moodString, "_MOOD")) {
					moodString = moodString.substring(0, moodString.indexOf("_MOOD"));
				}
				var mood:Mood = Mood.fromString(moodString);
				if (mood != null && moodsSet.indexOf(mood) == -1) { //How would that (mood == null) happen?
					moodsSet.push(mood);
				}
			}
			
			//Access next line-attribute and add it as a lineusage.
			var nextLineAttribute:LineAttribute = l.getLineAttribute("\"next\""); //LineAttributes are stored in "name" format, due to parsing and checking opportunties.
			if (nextLineAttribute != null) {
				var nextLineValue:String = nextLineAttribute.getValue() as String;
				if (StringFunctions.isQuoted(nextLineValue)) {
					nextLineValue = nextLineValue.substring(1, nextLineValue.length - 1);
					addReferencedLineName(nextLineValue);
				}
			}
		}
		
		private function addLineName(lineName:String):void {
			if (lineNameSet[lineName] == null) {
				lineNames.push(lineName);
				lineNameSet[lineName] = 1;
			}
		}
		
		private function addReferencedLineName(lineName:String):void {
			if (refLineNameSet[lineName] == null) {
				referencedLineNames.push(lineName);
				refLineNameSet[lineName] = 1;
			}
		}
		
		//handles single variable insertions only, which is most cases
		private function triggerActionIsANonLineReferenceTriggerViaVariableInsertion(triggerAction:String):Boolean {
			var sections:Array = triggerAction.split("*");
			if (sections.length != 3) {//before, during, after
				return false;
			}
			if (sections[0] != "" || sections[2] != "") {
				//given a trigger which has a single variable insertion, check if you can identify if it might be a simple trigger
				var potentialTriggerTypes:Array = TriggerType.getPossibleTriggerTypesBasedOnStartAndEndOfTrigger(sections[0], sections[2]);
				ArrayFunctions.remove(potentialTriggerTypes, TriggerType.ERROR);
				return potentialTriggerTypes.length > 1 || (potentialTriggerTypes.length == 1 && potentialTriggerTypes[0] != TriggerType.LINEREFERENCE);
			}
			var variable:Variable = dialogueChecker.getDialogueVariable(StringFunctions.addQuotes(sections[1]));
			if (variable != null) {
				var values:Array = variable.getExplicitUniqueValues();
				for (var i:uint = 0, isize = values.length; i < isize; i++) {
					var type:TriggerType = TriggerType.identifyType(values[i]);
					if (type == TriggerType.LINEREFERENCE || type == TriggerType.ERROR) {
						return false;
					}
				}
				return values.length > 0; //empty array = line reference trigger to empty linename
			}
			// if we don't know the variable, then we don't know for sure
			return false;
		}
		
		//Working on this... the idea is a recursive function that evaluates all lines for possibility.
		//Making this work with numerical values is a pain though.
		private function determineMatchesForLine(refLineName:String, possibleLines:Array = null):Array {
			if (possibleLines == null) {
				possibleLines = lineNames.concat();
					//possibleLines.sort();
			}
			
			var asterisksCount:uint = StringFunctions.countOccurances(refLineName, "*");
			if (asterisksCount % 2 != 0) {
				return new Array(); //Nothing to remove! INVALID LINE
			}
			if (builtRegexes[refLineName] == null) {
				builtRegexes[refLineName] = new RegExp(buildRegexForLineName(refLineName));
			}
			currentRegex = builtRegexes[refLineName];
			var matchedLines:Array = possibleLines.filter(filterLinesWithRegex);
			return matchedLines;
		}
		
		private function filterLinesWithRegex(element:*, index:int, arr:Array):Boolean {
			return currentRegex.test(element as String);
		}
		
		private function buildRegexForLineName(refLineName:String):String {
			var sections:Array = refLineName.split("*");
			if (sections.length % 2 != 1) {
				return ""; //HOW HARD CAN IT BE, PEOPLE
			}
			
			var regex:String = "^" + StringFunctions.escapeRegexChars(sections[0]);
			for (var i:uint = 1, isize:uint = sections.length; i < isize; i += 2) {
				var variable:Variable = dialogueChecker.getDialogueVariable(StringFunctions.addQuotes(sections[i]));
				if (variable != null) {
					regex += variable.getRegexOfPossibleValues();
				} else {
					regex += ".*"; //Because it hates you.
				}
				regex += StringFunctions.escapeRegexChars(sections[i + 1]);
			}
			regex += "$";
			return regex;
		}
		
		private function resolveVSToLineNames(vs:VariableSubstitution):Array {
			var content:String = vs.getRawContent();
			var vsVariable:String = vs.identifyVariable();
			content.replace(vsVariable, "*" + vsVariable + "*");
			var inTrigger:Boolean = false;
			var VSlineNames:Array = new Array();
			while (content != "") {
				if (inTrigger) {
					var rightBracketIndex:int = content.indexOf("]");
					if (rightBracketIndex != -1) {
						VSlineNames.push(content.substring(0, rightBracketIndex));
						content = content.substring(rightBracketIndex + 1);
						inTrigger = false;
					} else {
						content = "";
					}
				} else {
					var leftBracketIndex:int = content.indexOf("[");
					if (leftBracketIndex != -1) {
						content = content.substring(leftBracketIndex + 1);
						inTrigger = true;
					} else {
						content = "";
					}
				}
			}
			return VSlineNames;
		}
	}

}