package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableReference extends LineElement
	{
		private var variableName:String;
		private var variableValue:Object;
		//private var variable:Variable; //Maybe have a real reference to the variable somehow...
		public function VariableReference(name:String, value:Object) 
		{
			variableName = name;
			variableValue = value;
		}
		public function getName():String {
			return variableName;
		}
		public function getValue():Object {
			if (variableValue == null) {
				//trace("getValue() returned NULL - name is " + getName());
				//return "NULL";
			}
			return variableValue;
		}
		public override function getLineSegment():String {
			return getReferenceAsString();
		}
		public function getReferenceAsString():String {
			var result:String = StringFunctions.addQuotes(variableName) + ":";
			if (variableValue is String) {
				result += StringFunctions.addQuotes(variableValue);
			} else {
				result += variableValue;
			}
			return result;
		}
		public function getReferenceAsParsed():String {
			return variableName + ":" + variableValue;
		}
	}

}