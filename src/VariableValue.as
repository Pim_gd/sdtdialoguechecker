package  
{
	/**
	 * This object is immutable!
	 * You can use the set functions but they will return a new copy of this object with the modifications.
	 * @author Pimgd
	 */
	public class VariableValue 
	{
		private var name:String;
		private var value:String;
		public function VariableValue(variableName:String, variableValue:String) 
		{
			this.name = variableName;
			this.value = variableValue;
		}
		
		public function setName(variableName:String):VariableValue {
			return copy(variableName, value);
		}
		
		public function getName():String {
			return name;
		}
		
		public function setValue(variableValue:String):void {
			return copy(name, variableValue);
		}
		
		public function getValue():String {
			return value;
		}
		
		public function copy(newName:String, newValue:String):VariableValue {
			return new VariableValue(newName, newValue);
		}
		
		public function equals(otherVariableValue:VariableValue):Boolean {
			if (this === otherVariableValue) {
				return true;
			}
			
			if (name != otherVariableValue.getName()) {
				return false;
			}
			if (value != otherVariableValue.getValue()) {
				return false;
			}
			
			return true;
		}
		
	}

}