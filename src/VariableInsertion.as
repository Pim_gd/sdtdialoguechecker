package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableInsertion extends LineElement
	{
		private var variableReference:Variable;
		private var content:String;
		public function VariableInsertion(variable:Variable, text:String) 
		{
			variableReference = variable;
			content = text;
		}
		public function getVariable():Variable {
			return variableReference;
		}
		public function getContent():String {
			return content;
		}
		public override function getLineSegment():String {
			return "*" + content + "*";
		}
	}

}