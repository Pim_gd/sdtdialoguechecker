package  
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class TextBlock extends Sprite
	{
		private var txt:String;
		private var onScreen:Boolean;
		private var xLocation:Number;
		private var yLocation:Number;
		private var selectable:Boolean = false;
		private var fontSize:uint;
		private var textAlign:String;
		private var maxWidth:Number;
		private var maxHeight:Number;
		private var fontColor:Number;
		private var textFieldHolder:Sprite;
		public function TextBlock(xLoc:Number, yLoc:Number, maxW:Number, maxH:Number, text:String) 
		{
			xLocation = xLoc;
			yLocation = yLoc;
			maxWidth = maxW;
			maxHeight = maxH;
			txt = text;
			selectable = false;
			fontSize = 12;
			textAlign = "left";
			onScreen = false;
			fontColor = 0x000000;
		}
		
		public function getText():String {
			return txt;
		}
		
		public function show(s:DisplayObjectContainer):void {
			if (!onScreen) {
				s.addChild(this);
				onScreen = true;
				updateTextField();
				addChild(textFieldHolder);
				if (ErrorManager.debugging) {
					this.graphics.beginFill(0x990000);
					this.graphics.drawRect(0, 0, maxWidth, maxHeight);
					this.graphics.endFill();
				}
				updatePosition();
			}
		}
		
		public function hide(s:DisplayObjectContainer):void {
			if (onScreen) {
				removeChild(textFieldHolder);
				textFieldHolder = null;
				onScreen = false;
				s.removeChild(this);
			}
		}
		
		public function setSelectable(s:Boolean):void {
			selectable = s;
			updateTextField();
		}
		public function setFontSize(s:uint):void {
			fontSize = s;
			updateTextField();
		}
		public function setTextAlign(align:String):void {
			textAlign = align;
			updateTextField();
		}
		public function setFontColor(color:Number):void {
			fontColor = color;
			updateTextField();
		}
		public function updatePosition():void {
			if (onScreen) {
				if (!stage) {
					ErrorManager.showDevelopmentError("TextBlock::updatePosition():void - function called, object is on screen, but does not have a stage reference");
				}
				this.x = xLocation;
				this.y = yLocation;
			}
		}
		
		public function updateTextField():void {
			if (onScreen) {
				if(textFieldHolder != null){
					removeChild(textFieldHolder);
				}
				textFieldHolder = new Sprite();
				textFieldHolder.addChild(createTextField());
				addChild(textFieldHolder);
			}
		}
		
		private function createTextField():TextField {
			var tf:TextField = new TextField();
			tf.text = txt;
			tf.wordWrap = true;
			var txtFormat:TextFormat = new TextFormat();
			txtFormat.align = textAlign;
			txtFormat.size = fontSize;
			txtFormat.color = fontColor;
			txtFormat.font = "serif";
			tf.width = maxWidth;
			tf.height = maxHeight;
			tf.selectable = selectable;
			tf.setTextFormat(txtFormat);
			return tf;
		}
		
		public function setX(xLoc:Number):void {
			xLocation = xLoc;
			updatePosition();
		}
		
		public function setY(yLoc:Number):void {
			yLocation = yLoc;
			updatePosition();
		}
		
		public function getX():Number {
			return xLocation;
		}
		
		public function getY():Number {
			return yLocation;
		}
	}

}