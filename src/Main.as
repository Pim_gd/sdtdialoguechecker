package {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class Main extends Sprite {
		private var outputTF:TextField;
		private var inputTF:TextField;
		private var inputLinesTF:TextField;
		private var linebreakTF:TextField;
		private var checker:DialogueChecker;
		private var chkboxGrammar:CheckBox;
		private var chkboxSyntax:CheckBox;
		private var chkboxSubstitutes:CheckBox;
		private var chkboxLinefilter:CheckBox;
		private var chkboxVariables:CheckBox;
		private var chkboxLineUsage:CheckBox;
		private var chkboxSevSevere:CheckBox;
		private var chkboxSevMajor:CheckBox;
		private var chkboxSevMinor:CheckBox;
		//private var options:Options;
		
		private var importingSettingsFailed:Boolean = false;
		
		private static const INITIAL_INPUT_TEXT:String = "Copy-paste your dialogue into this box.";
		public static var version:String = "v3.08dev";
		
		/**
		 * 	- TODO: Alter LineUsageChecker so that it only treats the last linereference trigger as a usable one.
		 * 	- TODO: Alter VariableChecker to display a proper initial_settings line upon finding unused variables
		 *  - Fix bug: intro1:"yeah[intro2]*[YOU][intro2Named]*" (Ignore it as it's overridden anyway)
		 * 	- Add feature to check an entire folder of dialogues and report per dialogue
		 *  - Make line usage checker detect variable sets
		 *
		 * - add check for rustys issue "test[SHOCK]!" -> "test(shock) !" = "test !"
		 * - Add minor issue for having text in a line that sets loadDialogue in line attributes
		 *
		 *  - Change variablechecker to look for variables with " in them which is not at the start or end and is not whitespace
		 * Reactive Interrupt trick, which only ever works for Her lines" ... I have to add a check for that in the checker... lines with Him/Thought style with multiple lineref triggers don't need them and don't count as valid
		 * - Provide suggestion for solving finish in line with line attributes
		 * - Check for duplicate variable definition in initial_settings, only last one applies
		 * - Put lineattributes in a associative array
		 * - Extend variable checks and sets with line numbers so that {"check":{"var":1},"set":{"var":"+1"}} doesn't break the regex for validating values
		 */
		
		public function Main():void {
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			//trace("introtrue1".search(/^intro(true|false)$/));
			//trace("intro1".search(/^intro(true|false)$/));
			//trace("introfalse".search(/^intro(true|false)$/));
			//trace("intro1".search(/^intro(0|(0|((-?[1-9])\d*)))$/));
			
			checker = new DialogueChecker();
			importSettingsForChecker();
			
			inputLinesTF = new TextField();
			inputLinesTF.type = TextFieldType.DYNAMIC;
			inputLinesTF.y = 22;
			inputLinesTF.x = -2;
			inputLinesTF.width = 27;
			inputLinesTF.height = 230;
			inputLinesTF.selectable = false;
			inputLinesTF.multiline = true;
			stage.addChild(inputLinesTF);
			
			inputTF = new TextField();
			inputTF.width = 750;
			inputTF.height = 230;
			
			var instructionTF:TextField = new TextField();
			instructionTF.width = 775;
			instructionTF.height = 20;
			stage.addChild(instructionTF);
			instructionTF.x = 25;
			instructionTF.selectable = false;
			instructionTF.text = "Put your dialogue in the top box. Hit \"Check my dialogue!\" Copy output in bottom box to a text file. Fix your dialogue if something was found.";
			
			outputTF = new TextField();
			outputTF.width = 750;
			outputTF.height = 225;
			stage.addChild(inputTF);
			inputTF.x = 25;
			inputTF.y = 22;
			inputTF.multiline = true;
			inputTF.text = INITIAL_INPUT_TEXT;
			inputTF.type = TextFieldType.INPUT;
			inputTF.border = true;
			stage.addChild(outputTF);
			outputTF.x = 25;
			outputTF.y = 350;
			outputTF.multiline = true;
			outputTF.text = "Once you've copy-pasted your dialogue into the top box, hit \"Check my Dialogue!\". I'll put the results here.";
			outputTF.type = TextFieldType.INPUT;
			outputTF.wordWrap = true;
			outputTF.border = true;
			
			var parseBTN:Sprite = new Sprite();
			stage.addChild(parseBTN);
			parseBTN.x = 300;
			parseBTN.y = 255;
			parseBTN.graphics.lineStyle(2);
			parseBTN.graphics.beginFill(0x009900);
			parseBTN.graphics.drawRect(0, 0, 130, 90);
			parseBTN.graphics.endFill();
			var parseBTNTF:TextField = new TextField();
			parseBTNTF.text = "Check my Dialogue!";
			parseBTNTF.selectable = false;
			parseBTNTF.width = 130;
			parseBTN.addChild(parseBTNTF);
			parseBTN.addEventListener(MouseEvent.MOUSE_DOWN, parseDialogue);
			
			var aboutBTN:Sprite = new Sprite();
			stage.addChild(aboutBTN);
			aboutBTN.x = 350;
			aboutBTN.y = 577;
			aboutBTN.graphics.lineStyle(2);
			aboutBTN.graphics.beginFill(0x009900);
			aboutBTN.graphics.drawRect(0, 0, 60, 20);
			aboutBTN.graphics.endFill();
			var aboutBTNTF:TextField = new TextField();
			aboutBTNTF.text = "Credits";
			aboutBTNTF.setTextFormat(new TextFormat(null, 10, null, null, null, null, null, null, "center"));
			aboutBTNTF.selectable = false;
			aboutBTNTF.width = 60;
			aboutBTN.addChild(aboutBTNTF);
			aboutBTN.addEventListener(MouseEvent.MOUSE_DOWN, printAbout);
			
			var authorTF:TextField = new TextField();
			authorTF.width = 200;
			authorTF.height = 20;
			authorTF.text = "Made by Pimgd";
			authorTF.selectable = false;
			stage.addChild(authorTF);
			authorTF.x = 25;
			authorTF.y = 575;
			
			var versionTF:TextField = new TextField();
			versionTF.width = 50;
			versionTF.height = 20;
			versionTF.text = version;
			stage.addChild(versionTF);
			versionTF.x = 750;
			versionTF.y = 575;
			
			chkboxGrammar = new CheckBox(20, 250);
			chkboxGrammar.setChecked(true);
			chkboxGrammar.show(stage);
			
			var tbGrammar:TextBlock = new TextBlock(50, 260, 110, 30, "Check Grammar");
			tbGrammar.show(stage);
			
			chkboxSyntax = new CheckBox(20, 280);
			chkboxSyntax.setChecked(true);
			chkboxSyntax.show(stage);
			
			var tbSyntax:TextBlock = new TextBlock(50, 290, 110, 30, "Check Syntax");
			tbSyntax.show(stage);
			
			chkboxSubstitutes = new CheckBox(20, 310);
			chkboxSubstitutes.setChecked(true);
			chkboxSubstitutes.show(stage);
			
			var tbSubstitutes:TextBlock = new TextBlock(50, 320, 120, 30, "Check Substitutes");
			tbSubstitutes.show(stage);
			
			chkboxSevSevere = new CheckBox(430, 250);
			chkboxSevSevere.setChecked(true);
			chkboxSevSevere.show(stage);
			
			var tbSevSevere:TextBlock = new TextBlock(465, 260, 125, 30, "Report Severe Issues");
			tbSevSevere.show(stage);
			
			chkboxSevMajor = new CheckBox(430, 280);
			chkboxSevMajor.setChecked(true);
			chkboxSevMajor.show(stage);
			
			var tbSevMajor:TextBlock = new TextBlock(465, 290, 125, 30, "Report Major Issues");
			tbSevMajor.show(stage);
			
			chkboxSevMinor = new CheckBox(430, 310);
			chkboxSevMinor.setChecked(true);
			chkboxSevMinor.show(stage);
			
			var tbSevMinor:TextBlock = new TextBlock(465, 320, 125, 30, "Report Minor Issues");
			tbSevMinor.show(stage);
			
			chkboxLinefilter = new CheckBox(590, 250);
			chkboxLinefilter.setChecked(true);
			chkboxLinefilter.show(stage);
			
			var tbLinefilter:TextBlock = new TextBlock(630, 260, 150, 30, "Enable comment filter");
			tbLinefilter.show(stage);
			
			chkboxVariables = new CheckBox(155, 250);
			chkboxVariables.setChecked(true);
			chkboxVariables.show(stage);
			
			var tbVariables:TextBlock = new TextBlock(185, 260, 115, 30, "Check Variables");
			tbVariables.show(stage);
			
			chkboxLineUsage = new CheckBox(155, 280);
			chkboxLineUsage.setChecked(true);
			chkboxLineUsage.show(stage);
			
			var tbLineUsage:TextBlock = new TextBlock(185, 290, 115, 30, "Check Line Usage");
			tbLineUsage.show(stage);
			
			var tbLinebreak:TextBlock = new TextBlock(590, 290, 65, 30, "Linebreak:");
			tbLinebreak.show(stage);
			
			linebreakTF = new TextField();
			linebreakTF.width = 100;
			linebreakTF.height = 18;
			stage.addChild(linebreakTF);
			linebreakTF.x = 650;
			linebreakTF.y = 290;
			linebreakTF.multiline = false;
			linebreakTF.text = "\\r\\n";
			linebreakTF.type = TextFieldType.INPUT;
			linebreakTF.border = true;
			
			stage.addEventListener(Event.ENTER_FRAME, updateLineNumberDisplays);
		}
		
		private function importSettingsForChecker():void {			
			var url:URLRequest = new URLRequest("includes.txt");
            var loader:URLLoader = new URLLoader();
			loader.load(url);
			loader.addEventListener(Event.COMPLETE, loaderComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loaderError);
			function loaderComplete(e:Event):void
			{
				var dataString:String = loader.data;
				for each (var path:String in dataString.split("\r\n")){
					checker.importSettingsFromFile(path);
				}
			}
			
			function loaderError(e:Event):void 
			{
				//so uh, if you fail to load the includes, right...
				//What do?
				//Maybe you're on web?
				//So for now, don't scream
				//outputTF.text = "Failed to load custom settings, problem loading includes.txt - error message:  " + e.toString();
				importingSettingsFailed = true;
			}
		}
		
		public function updateLineNumberDisplays(e:Event = null):void {
			var scrollV:uint = inputTF.scrollV;
			var lineNumbersForInputTF:String = "" + scrollV;
			for (var i:uint = 1, ilimit:uint = Math.min(15, (inputTF.numLines+1) - scrollV); i < ilimit; i++) {
				lineNumbersForInputTF += "\n" + (scrollV + i);
			}
			inputLinesTF.text = lineNumbersForInputTF;
			inputLinesTF.setTextFormat(new TextFormat(null, 12, null, null, null, null, null, null, "right"));
		}
		
		public function printAbout(e:MouseEvent = null):void {
			var output:String = "";
			var o:Options = new Options();
			o.parseLineBreak(linebreakTF.text);
			var linebreak:String = o.getLineBreak();
			output += "DialogueChecker " + version + "!" + linebreak;
			output += "See https://bitbucket.org/Pim_gd/sdtdialoguechecker/src for licensing and sources." + linebreak;
			output += "Supports the following mods:" + linebreak;
			output += "AnimtoolsV31, dialogdisplayeditv6 and BreastExpansionPlusV2_4 by sby" + linebreak;
			output += "DialogueActions v4.00 by Pimgd" + linebreak;
			output += "DialogueActions v3.02 to v3.05 by Pimgd (simple variables only)" + linebreak;
			output += "DialogueActions v3.01 by WeeWillie / Pimgd (simple variables only)" + linebreak;
			output += "DialogueActions v3.00 by WeeWillie" + linebreak;
			output += "DialogueActions v1.11 by gollum / Pimgd (and v2.04 mostly)" + linebreak;
			output += "VariableArithmetic v1.05+ by Pimgd (missing support for BYNAME functions)";
			if (checker.hasImports()) {
				output += linebreak + checker.printImports(o);
			} else if (importingSettingsFailed) {
				output += linebreak + "Importing any additional settings failed - if you're running this in a browser, that's normal." + linebreak + "If you're running this in standalone flash player, that means you have a problem.";
			}
			outputTF.text = output;
		}
		
		public function parseDialogue(e:MouseEvent = null):void {
			//outputTF.text = checker.checkDialogue(inputTF.text, chkboxGrammar.isChecked(), chkboxSyntax.isChecked(), chkboxSubstitutes.isChecked(), chkboxLinefilter.isChecked(), linebreakTF.text);
			if (inputTF.text == INITIAL_INPUT_TEXT) {
				outputTF.text = "I think you forgot to paste your dialogue into the top box.";
				return;
			}
			if (inputTF.text == "") {
				outputTF.text = "There's no dialogue for me to check, you know. Go on, put a dialogue in the top input box. This DialogueChecker doesn't bite.";
				return;
			}
			var startTime:Number = new Date().valueOf();
			var o:Options = new Options();
			o.addSeverity(Severity.SEVERE);
			o.addSeverity(Severity.MAJOR);
			o.addSeverity(Severity.MINOR);
			if (chkboxGrammar.isChecked()) {
				o.addChecker("Grammar"); //Stringly typed =(
			}
			if (chkboxSyntax.isChecked()) {
				o.addChecker("Syntax");
			}
			if (chkboxVariables.isChecked()) {
				o.addChecker("Variables");
			}
			if (chkboxLineUsage.isChecked()) {
				o.addChecker("LineUsage");
			}
			o.setSetting("substitutes", chkboxSubstitutes.isChecked(), false);
			o.setSetting("commentfilter", chkboxLinefilter.isChecked(), false);
			o.parseLineBreak(linebreakTF.text);
			
			var d:Dialogue = null;
			var issues:Array = new Array();
			var timingData:Array = null;
			//for (var dialogueLoops:int = 0; dialogueLoops < 5; dialogueLoops++) {
				try {
					d = new Dialogue(inputTF.text);
					issues = checker.checkDialogueObject(d, o);
					timingData = checker.getTimingDataOfLastRun();
				
				} catch (e:Error) {
					outputTF.appendText("Something went horribly wrong, and I just caught a flash error." + o.getLineBreak());
					outputTF.appendText("Below this message is going to be an error message. It is possible that this error message is \"null\". That doesn't matter." + o.getLineBreak());
					outputTF.appendText("What I want you to do is to post on the DialogueChecker thread. The post needs to contain the dialogue you just tried to check, and this error message." + o.getLineBreak());
					outputTF.appendText(e.getStackTrace());
					outputTF.appendText(o.getLineBreak() + e.errorID);
					return;
				}
			//}
			var severeIssueCount:uint = 0;
			var majorIssueCount:uint = 0;
			var minorIssueCount:uint = 0;
			for (var i:uint = 0, isize:uint = issues.length; i < isize; i++) {
				var issue:Issue = issues[i];
				if (issue.getSeverity() == Severity.SEVERE) {
					severeIssueCount++;
				} else if (issue.getSeverity() == Severity.MAJOR) {
					majorIssueCount++;
				} else if (issue.getSeverity() == Severity.MINOR) {
					minorIssueCount++;
				}
			}
			var output:String = "";
			var outputText:String = "";
			if (issues.length > 0) {
				output = "I found " + issues.length + " issue(s) with your dialogue, of which " + severeIssueCount + " were severe, " + majorIssueCount + " were major issues, and " + minorIssueCount + " were minor issues." + o.getLineBreak();
				if (severeIssueCount > 0) {
					output += o.getLineBreak() + "Severe issues MUST be fixed before releasing your dialogue, or your dialogue will crash and fail to load.";
					if (!chkboxSevSevere.isChecked()) {
						output += o.getLineBreak() + "There are suppressed severe issues in your dialogue. To see these severe issues, tick the checkbox \"Report Severe Issues\", then let the checker recheck your dialogue.";
					}
				}
				if (majorIssueCount > 0) {
					if (!chkboxSevMajor.isChecked()) {
						output += o.getLineBreak() + "There are suppressed major issues in your dialogue. To see these major issues, tick the checkbox \"Report Major Issues\", then let the checker recheck your dialogue.";
					}
				}
				if (minorIssueCount > 0) {
					if (!chkboxSevMinor.isChecked()) {
						output += o.getLineBreak() + "There are suppressed minor issues in your dialogue. To see these minor issues, tick the checkbox \"Report Minor Issues\", then let the checker recheck your dialogue.";
					}
				}
				output += o.getLineBreak();
				
				for (var i:uint = 0, isize:uint = issues.length; i < isize; i++) {
					var issueSeverity:Severity = issues[i].getSeverity();
					if (issueSeverity == Severity.SEVERE) {
						if (chkboxSevSevere.isChecked()) {
							outputText += issues[i].printMessage();
						}
					} else if (issueSeverity == Severity.MAJOR) {
						if (chkboxSevMajor.isChecked()) {
							outputText += issues[i].printMessage();
						}
					} else if (issueSeverity == Severity.MINOR) {
						if (chkboxSevMinor.isChecked()) {
							outputText += issues[i].printMessage();
						}
					} else {
						outputText += "An issue with unknown severity was found!" + o.getLineBreak();
						outputText += issues[i].printMessage();
					}
					if (outputText.length >= 10000000) {
						output = "Output was capped at about 10 million characters to prevent crashes." + o.getLineBreak() + output;
						i = isize;
					}
				}
				outputText = output + outputText;
				
				if (issues.length > 0) {
					outputText += "Once you've fixed these issues, try letting me look your dialogue over again. Maybe I'll find something new.";
						//addRedlinesToInputBasedOnIssues(issues);
				}
			} else {
				if (chkboxGrammar.isChecked() && chkboxSubstitutes.isChecked() && chkboxSyntax.isChecked()) {
					outputText = "Your dialogue looks clean to me. Have you used a spellchecker for your dialogue?" + o.getLineBreak() + "What about loading the dialogue ingame, then going to the dialogue log? Is that clean of any errors?";
					outputText += checker.printDependencies();
				} else {
					var temparr:Array = new Array();
					if (chkboxGrammar.isChecked()) {
						temparr.push("grammar");
					}
					if (chkboxSyntax.isChecked()) {
						temparr.push("syntax");
					}
					if (chkboxSubstitutes.isChecked()) {
						temparr.push("substitutes");
					}
					if (chkboxVariables.isChecked()) {
						temparr.push("variables");
					}
					if (chkboxLineUsage.isChecked()) {
						temparr.push("line usage");
					}
					var tempstr:String = "";
					for (var i:uint = 0, isize:uint = temparr.length; i < isize; i++) {
						if (i == 0) {
							tempstr += temparr[i];
						} else if ((i + 1) < isize) {
							tempstr += ", " + temparr[i];
						} else if (i < isize) {
							tempstr += " and " + temparr[i];
						} else {
							tempstr += temparr[i];
						}
					}
					if (tempstr == "") {
						outputText = "Uh... You have to let me check something. Tick one of those checkboxes that says \"Check ...\", then press \"Check my Dialogue\"!";
					} else if (tempstr == "substitutes") {
						outputText = "Since v2.00, the DialogueChecker does not contain any specific checks for variable substitutions." + o.getLineBreak();
						outputText += "To check for issues regarding substitutes, enable both the checking of Grammar and the checking of Substitutes." + o.getLineBreak();
					} else {
						outputText = "";
						if (temparr.indexOf("substitutes") != -1 && temparr.indexOf("grammar") == -1) {
							outputText += "Since v2.00, the DialogueChecker does not contain any specific checks for variable substitutions." + o.getLineBreak();
							outputText += "To check for issues regarding substitutes, enable both the checking of Grammar and the checking of Substitutes." + o.getLineBreak();
							tempstr = tempstr.replace(", substitutes", "");
							tempstr = tempstr.replace("substitutes, ", "");
							tempstr = tempstr.replace("substitutes and ", "");
							tempstr = tempstr.replace("and substitutes", "");
						}
						outputText += "Your dialogue looks clean to me, regarding " + tempstr + "." + o.getLineBreak() + "How about enabling the other options?";
					}
					
				}
				
			}
			var endTime:Number = new Date().valueOf();
			var timingString:String = "";
			for (var checkerName:String in timingData) {
				timingString += checkerName + ": " + timingData[checkerName] + "ms." + o.getLineBreak();
			}
			if (timingString != "") {
				timingString = "Runtime:" + o.getLineBreak() + timingString + o.getLineBreak();
			}
			outputText = "Dialogue was checked in " + (endTime - startTime) + " ms with " + version + " of the DialogueChecker." + o.getLineBreak() + timingString + outputText;
			outputTF.text = outputText;
		}
	
		//Added in v2.08, Commented in v2.08. Works, but the dialogue dies as a result. Line breaks get fucked up and the parser dies after fixing the errors.
	/*private function addRedlinesToInputBasedOnIssues(issues:Array):void {
	   var affectedLines:Array = new Array();
	   for (var i:uint = 0, isize:uint = issues.length; i < isize; i++) {
	   affectedLines.push(issues[i].getRelatedLineNumber()-1);
	   }
	   affectedLines.sort(Array.NUMERIC);//Shameless ripping of code from the internet ahead
	   var j:int = 0;
	   while(j < affectedLines.length) {
	   while(j < affectedLines.length+1 && affectedLines[j] == affectedLines[j+1]) {
	   affectedLines.splice(j, 1);
	   }
	   j++;
	   }
	   affectedLines.sort(Array.NUMERIC);//I just hope it works.
	   var splitOn:String = "\r";
	   var lines:Array = inputTF.text.split("\r");//Maybe I should split by \r\n first, then by \n\r, then by \r, then by \n?
	   if (lines.length == 1) {
	   lines = inputTF.text.split("\n");
	   splitOn = "\n";
	   }
	   var htmlText:String = "";
	   if(affectedLines.length > 0){
	   var affectedLineIndex:uint = 0;
	   for (var i:uint = 0, isize:uint = lines.length; i < isize; i++) {
	   if (i == affectedLines[affectedLineIndex]) {
	   htmlText += "<font color=\"#FF0000\">" + lines[i] + splitOn + "</font>";
	   affectedLineIndex++;
	   } else {
	   htmlText += lines[i] + splitOn;
	   }
	   }
	   inputTF.htmlText = htmlText;
	   }
	 }*/
	
	}

}