package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableSubstitution extends LineElement
	{
		private var content:String;
		public function VariableSubstitution(text:String) 
		{
			content = text;
		}
		public override function getLineSegment():String {
			return "*" + content + "*";
		}
		public function containsTrigger():Boolean {
			return content.indexOf("[") != -1;
		}
		public function getRawContent():String {
			return content;
		}
		public function identifyVariable():String {
			var checkArray:Array = new Array();//Based upon tests I did, this is the order in which SDT checks for variables.
			checkArray.push([content.indexOf("FINISHES"), "FINISHES"]);
			checkArray.push([content.indexOf("YOUR"), "YOUR"]);
			checkArray.push([content.indexOf("YOU"), "YOU"]);
			checkArray.push([content.indexOf("ME"), "ME"]);
			checkArray.push([content.indexOf("MY"), "MY"]);
			//checkArray.sortOn([0], [Array.NUMERIC]);//So don't sort!
			for (var i:uint = 0, isize:uint = checkArray.length; i < isize; i++) {
				if (checkArray[i][0] != -1) {
					return checkArray[i][1];
				}
			}
			return "";
		}
	}

}