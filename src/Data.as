package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class Data {
		public static const modVariablesAnimtools:Array = ["atv_active", "atv_position_name", "atv_position", "atv_position2", "atv_dominant", "atv_dominant2", "atv_dominant_held", "atv_dominant_held2", "atv_inbodycontactloose", "atv_inbody", "atv_curresist", "atv_curresist2"];
		public static const modVariablesBreastExpansion:Array = ["bellysize", "bellysize2", "bigpenisbellychange", "bigpenisbellychange2", "bigpenisbodychange", "bigpenisbodychange2", "bigpenisbreastchange", "bigpenisbreastchange2", "bodysize", "bodysize2", "breastsize", "breastsize2", "breathLevel", "breathLevel2", "effectivecuminside", "effectivecuminside2", "eyecumtimer", "eyecumtimer2", "mouthFull", "passOutFactor", "passOutFactor2", "penisInMouthDist", "penisInMouthDist2", "superbreastsize", "superbreastsize2", "timerpuke", "timerpuke2"]; /* END OF BreastExpansionPlus variables */
		public static const modVariablesDialogueActions:Array = ["ankleCuffs", "armwear", "background", "bellyPiercing", "bottoms", "collar", "cuffs", "earring", "eyewear", "footwear", "gag", "hair", "headwear", "himBottoms", "himFootwear", "himTop", "legwear", "legwearB", "loadCharCode", "loadDialogue", "maxPenisLength", "maxPenisWidth", "minPenisLength", "minPenisWidth", "nipplePiercing", "panties", "penisLength", "penisSize", "penisWidth", "tonguePiercing", "top", "tops"]; /*End of DialogueActions variables*/
		public static const modVariablesDialogueActionsV204:Array = ["da.background.load", "da.charcode.load", "da.clothes.ankleCuffs", "da.clothes.ankleCuffs.a", "da.clothes.ankleCuffs.a2", "da.clothes.ankleCuffs.b", "da.clothes.ankleCuffs.b2", "da.clothes.ankleCuffs.g", "da.clothes.ankleCuffs.g2", "da.clothes.ankleCuffs.r", "da.clothes.ankleCuffs.r2", "da.clothes.ankleCuffs.type", "da.clothes.armwear", "da.clothes.armwear.a", "da.clothes.armwear.a2", "da.clothes.armwear.b", "da.clothes.armwear.b2", "da.clothes.armwear.g", "da.clothes.armwear.g2", "da.clothes.armwear.r", "da.clothes.armwear.r2", "da.clothes.armwear.type", "da.clothes.bellyPiercing", "da.clothes.bellyPiercing.a", "da.clothes.bellyPiercing.a2", "da.clothes.bellyPiercing.b", "da.clothes.bellyPiercing.b2", "da.clothes.bellyPiercing.g", "da.clothes.bellyPiercing.g2", "da.clothes.bellyPiercing.r", "da.clothes.bellyPiercing.r2", "da.clothes.bellyPiercing.type", "da.clothes.bottoms", "da.clothes.bottoms.a", "da.clothes.bottoms.a2", "da.clothes.bottoms.b", "da.clothes.bottoms.b2", "da.clothes.bottoms.g", "da.clothes.bottoms.g2", "da.clothes.bottoms.r", "da.clothes.bottoms.r2", "da.clothes.bottoms.type", "da.clothes.collar", "da.clothes.collar.a", "da.clothes.collar.a2", "da.clothes.collar.b", "da.clothes.collar.b2", "da.clothes.collar.g", "da.clothes.collar.g2", "da.clothes.collar.r", "da.clothes.collar.r2", "da.clothes.collar.type", "da.clothes.cuffs", "da.clothes.cuffs.a", "da.clothes.cuffs.a2", "da.clothes.cuffs.b", "da.clothes.cuffs.b2", "da.clothes.cuffs.g", "da.clothes.cuffs.g2", "da.clothes.cuffs.r", "da.clothes.cuffs.r2", "da.clothes.cuffs.type", "da.clothes.earring", "da.clothes.earring.a", "da.clothes.earring.a2", "da.clothes.earring.b", "da.clothes.earring.b2", "da.clothes.earring.g", "da.clothes.earring.g2", "da.clothes.earring.r", "da.clothes.earring.r2", "da.clothes.earring.type", "da.clothes.eyewear", "da.clothes.eyewear.a", "da.clothes.eyewear.a2", "da.clothes.eyewear.b", "da.clothes.eyewear.b2", "da.clothes.eyewear.g", "da.clothes.eyewear.g2", "da.clothes.eyewear.r", "da.clothes.eyewear.r2", "da.clothes.eyewear.type", "da.clothes.footwear", "da.clothes.footwear.a", "da.clothes.footwear.a2", "da.clothes.footwear.b", "da.clothes.footwear.b2", "da.clothes.footwear.g", "da.clothes.footwear.g2", "da.clothes.footwear.r", "da.clothes.footwear.r2", "da.clothes.footwear.type", "da.clothes.gag", "da.clothes.gag.a", "da.clothes.gag.a2", "da.clothes.gag.b", "da.clothes.gag.b2", "da.clothes.gag.g", "da.clothes.gag.g2", "da.clothes.gag.r", "da.clothes.gag.r2", "da.clothes.gag.type", "da.clothes.headwear", "da.clothes.headwear.a", "da.clothes.headwear.a2", "da.clothes.headwear.b", "da.clothes.headwear.b2", "da.clothes.headwear.g", "da.clothes.headwear.g2", "da.clothes.headwear.r", "da.clothes.headwear.r2", "da.clothes.headwear.type", "da.clothes.himBottoms", "da.clothes.himBottoms.a", "da.clothes.himBottoms.a2", "da.clothes.himBottoms.b", "da.clothes.himBottoms.b2", "da.clothes.himBottoms.g", "da.clothes.himBottoms.g2", "da.clothes.himBottoms.r", "da.clothes.himBottoms.r2", "da.clothes.himBottoms.type", "da.clothes.himFootwear", "da.clothes.himFootwear.a", "da.clothes.himFootwear.a2", "da.clothes.himFootwear.b", "da.clothes.himFootwear.b2", "da.clothes.himFootwear.g", "da.clothes.himFootwear.g2", "da.clothes.himFootwear.r", "da.clothes.himFootwear.r2", "da.clothes.himFootwear.type", "da.clothes.himTop", "da.clothes.himTop.a", "da.clothes.himTop.a2", "da.clothes.himTop.b", "da.clothes.himTop.b2", "da.clothes.himTop.g", "da.clothes.himTop.g2", "da.clothes.himTop.r", "da.clothes.himTop.r2", "da.clothes.himTop.type", "da.clothes.legwear", "da.clothes.legwear.a", "da.clothes.legwear.a2", "da.clothes.legwear.b", "da.clothes.legwear.b2", "da.clothes.legwear.g", "da.clothes.legwear.g2", "da.clothes.legwear.r", "da.clothes.legwear.r2", "da.clothes.legwear.type", "da.clothes.legwearB", "da.clothes.legwearB.a", "da.clothes.legwearB.a2", "da.clothes.legwearB.b", "da.clothes.legwearB.b2", "da.clothes.legwearB.g", "da.clothes.legwearB.g2", "da.clothes.legwearB.r", "da.clothes.legwearB.r2", "da.clothes.legwearB.type", "da.clothes.nipplePiercing", "da.clothes.nipplePiercing.a", "da.clothes.nipplePiercing.a2", "da.clothes.nipplePiercing.b", "da.clothes.nipplePiercing.b2", "da.clothes.nipplePiercing.g", "da.clothes.nipplePiercing.g2", "da.clothes.nipplePiercing.r", "da.clothes.nipplePiercing.r2", "da.clothes.nipplePiercing.type", "da.clothes.panties", "da.clothes.panties.a", "da.clothes.panties.a2", "da.clothes.panties.b", "da.clothes.panties.b2", "da.clothes.panties.g", "da.clothes.panties.g2", "da.clothes.panties.r", "da.clothes.panties.r2", "da.clothes.panties.type", "da.clothes.tonguePiercing", "da.clothes.tonguePiercing.a", "da.clothes.tonguePiercing.a2", "da.clothes.tonguePiercing.b", "da.clothes.tonguePiercing.b2", "da.clothes.tonguePiercing.g", "da.clothes.tonguePiercing.g2", "da.clothes.tonguePiercing.r", "da.clothes.tonguePiercing.r2", "da.clothes.tonguePiercing.type", "da.clothes.top", "da.clothes.top.a", "da.clothes.top.a2", "da.clothes.top.b", "da.clothes.top.b2", "da.clothes.top.g", "da.clothes.top.g2", "da.clothes.top.r", "da.clothes.top.r2", "da.clothes.top.type", "da.clothes.tops", "da.clothes.tops.a", "da.clothes.tops.a2", "da.clothes.tops.b", "da.clothes.tops.b2", "da.clothes.tops.g", "da.clothes.tops.g2", "da.clothes.tops.r", "da.clothes.tops.r2", "da.clothes.tops.type", "da.dialogue.load", "da.hair.load", "da.masturbation.herpleasure", "da.random"];
		public static const modVariablesDialogueActionsV300:Array = ["da.button1.name", "da.button2.name", "da.button3.name", "da.button4.name", "da.button5.name", "da.button6.name", "da.button7.name", "da.button8.name", "da.button9.name", "da.button10.name", "da.mod.load"];
		public static const modVariablesDialogueActionsV301:Array = ["da.button1.varname", "da.button2.varname", "da.button3.varname", "da.button4.varname", "da.button5.varname", "da.button6.varname", "da.button7.varname", "da.button8.varname", "da.button9.varname", "da.button10.varname"];
		public static const modVariablesDialogueActionsV302toV305:Array = ["da.pleasurePercentage", "da.breathPercentage", "da.oxygenPercentage", "da.canSpeakLineTrigger", "da.bgm.volume"];
		public static const modVariablesDAv401:Array = ["da.finishes"];
		public static const modVariablesDAv405:Array = ["da.blockingOrgasm"];
		public static const modVariablesDAv407:Array = ["da.dialogue.import", "da.sdtoptions.hoverOptions", "da.sdtoptions.showMouse", "da.sdtoptions.strandShaders", "da.sdtoptions.mirrored", "da.sdtoptions.spit", "da.sdtoptions.tears", "da.sdtoptions.mascara", "da.sdtoptions.smudging", "da.sdtoptions.nostrilSpray", "da.sdtoptions.sweat", "da.sdtoptions.breathing", "da.sdtoptions.gagging", "da.sdtoptions.coughing", "da.sdtoptions.tongue", "da.sdtoptions.dialogue", "da.sdtoptions.invertControls", "da.sdtoptions.bukkakeMode"];
		
		public static const linesSDTVanilla:Array = ["cough", "cum_in_eye", "cum_in_mouth", "cum_in_nose", "cum_in_throat", "cum_on_face", "dialogue_name", "drool", "finish", "finishOther", "first_dt", "first_throat", "font", "general", "hand_job_stroke", "head_grabbed", "held", "initial_settings", "interrupt", "intro", "lick_balls", "lick_penis", "pre_cum", "pulled_down", "pulled_up", "pull_off", "resistance", "restart", "swallow", "vigorous", "wake", "all", "custom", "CUSTOM", "DEFAULT"];
		public static const linesDialogueActions:Array = ["orgasm", "start", "passed_out", "choking"];
		public static const linesAnimtools:Array = ["bodycontactspaceheldinhilt", //added in v14, not present in user guide?
			"bodycontactspaceheldpush","bodycontactspaceheldpush_tit", "bodycontactspaceheldpush_vaginal", "bodycontactspaceheldpush_anal", "bodycontactspaceheldpush_other",
			"bodycontactspaceheldpushinhilt", "bodycontactspaceheldpushinhilt_tit", "bodycontactspaceheldpushinhilt_vaginal", "bodycontactspaceheldpushinhilt_anal", "bodycontactspaceheldpushinhilt_other",
			"bodycontacthilt", "bodycontacthilt_tit", "bodycontacthilt_vaginal", "bodycontacthilt_anal", "bodycontacthilt_other",
			"bodyexited", "bodyexited_tit", "bodyexited_vaginal", "bodyexited_anal", "bodyexited_other",
			"bodyentered", "bodyentered_tit", "bodyentered_vaginal", "bodyentered_anal", "bodyentered_other",
			"vigorousbodyexited", "vigorousbodyexited_tit", "vigorousbodyexited_vaginal", "vigorousbodyexited_anal", "vigorousbodyexited_other",
			"inbody", "inbody_tit", "inbody_vaginal", "inbody_anal", "inbody_other",
			"bodycontactloose", "bodycontactloose_tit", "bodycontactloose_vaginal", "bodycontactloose_anal", "bodycontactloose_other",
			"cuminher", "cuminher_tit", "cuminher_vaginal", "cuminher_anal", "cuminher_other",
			"vigorousbodyexitedrelax", "vigorousbodyexitedrelax_tit", "vigorousbodyexitedrelax_vaginal", "vigorousbodyexitedrelax_anal", "vigorousbodyexitedrelax_other",
			"bodycontactresistive", "bodycontactresistive_tit", "bodycontactresistive_vaginal", "bodycontactresistive_anal", "bodycontactresistive_other",
			"bodycontactmovementvigour", "bodycontactmovementvigour_tit", "bodycontactmovementvigour_vaginal", "bodycontactmovementvigour_anal", "bodycontactmovementvigour_other"];
		public static const linesBreastExpansion:Array = ["bellyexpansion", "bellyreduction", "bodyexpansion", "bodyreduction", "breastexpansion", "breastreduction", "cumfrombreast", "puke"];
		public static const linesDialogueActionsV300:Array = ["button1", "button2", "button3", "button4", "button5", "button6", "button7", "button8", "button9", "button10"];
		
		public static const lineDependencyMap:Object = { 
			"DialogueActions by gollum/Pimgd":linesDialogueActions, 
			"DialogueActions v3.00 by WeeWillie":linesDialogueActionsV300,
			"Animtools by sby":linesAnimtools,
			"BreastExpansionPlus by sby":linesBreastExpansion
			};
		
		public static const customLines:Array = [];
		
		public static const customVariables:Array = [];
		
		public function Data() {
		
		}
		
		/**
		 * Adds a mod-declared variable to the list of custom variables.
		 * @param	variableName The variable to add.
		 * @param	modName The mod the variable to add is from.
		 * @return Whether adding the mod-declared variable was successful
		 */
		public static function addModVariable(variableName:String, modName:String):Boolean {
			return ArrayFunctions.addIfNotExists(
				ArrayFunctions.getOrCreateSubArray(customVariables, modName), 
				variableName);
		}
		
		/**
		 * Adds a mod-declared line to the list of custom lines.
		 * @param	lineName The linename to add.
		 * @param	modName The mod the line to add is from.
		 * @return Whether adding the mod-declared line was successful
		 */
		public static function addCustomLine(lineName:String, modName:String):Boolean {
			return ArrayFunctions.addIfNotExists(
				ArrayFunctions.getOrCreateSubArray(customLines, modName), 
				lineName);
		}
		
		/**
		 * Internal usage, for finding declarations of variables and custom lines
		 * @param	inArray Array to search through
		 * @param	name to search fo
		 * @return the name of the mod that makes the declaration
		 */
		private static function findDeclaration(inArray:Array, name:String):String {
			for (var key:String in inArray) {
				var declarations:Array = inArray[key];
				if (declarations.indexOf(name) != -1) {
					return key;
				}
			}
			return "";
		}
		
		/**
		 * internal usage
		 * @param	lineName to search for
		 * @return the mod name it belongs to or empty string
		 */
		private static function findCustomLine(lineName:String):String {
			return findDeclaration(customLines, lineName);
		}
		
		public static function isCustomLine(lineName:String):Boolean {
			return findCustomLine(lineName) != "";
		}
		
		/**
		 * internal usage
		 * @param	variableName to search for
		 * @return the mod name it belongs to or empty string
		 */
		private static function findCustomModVariable(variableName:String):String {
			return findDeclaration(customVariables, variableName);
		}
		
		public static function isCustomModVariable(variableName:String):Boolean {
			return findCustomModVariable(variableName) != "";
		}
		
		public static function getDependencyOfModVariable(variableName:String):String {
			if (modVariablesAnimtools.indexOf(variableName) != -1) {
				return "Animtools by sby";
			}
			if (modVariablesBreastExpansion.indexOf(variableName) != -1) {
				return "BreastExpansionPlus by sby";
			}
			if (modVariablesDialogueActions.indexOf(variableName) != -1) {
				return "DialogueActions by gollum/Pimgd";
			} else if (modVariablesDialogueActionsV204.indexOf(variableName) != -1) {
				return "DialogueActions v2.04 by gollum/Pimgd";
			} else if (modVariablesDialogueActionsV300.indexOf(variableName) != -1) {
				return "DialogueActions v3.00 by WeeWillie";
			} else if (modVariablesDialogueActionsV301.indexOf(variableName) != -1) {
				return "DialogueActions v3.01 by WeeWillie/Pimgd";
			} else if (modVariablesDialogueActionsV302toV305.indexOf(variableName) != -1) {
				return "DialogueActions v3.02 - v3.05 by Pimgd";
			} else if (modVariablesDAv401.indexOf(variableName) != -1) {
				return "DialogueActions v4.01 by Pimgd";
			} else if (modVariablesDAv405.indexOf(variableName) != -1) {
				return "DialogueActions v4.05 by Pimgd";
			} else if (modVariablesDAv407.indexOf(variableName) != -1) {
				return "DialogueActions v4.07 by Pimgd";
			}
			
			return findCustomModVariable(variableName);
		}
		
		public static function isModVariable(v:String):Boolean {
			return isCustomModVariable(v) || 
			    !(modVariablesAnimtools.indexOf(v) == -1 
				&& modVariablesBreastExpansion.indexOf(v) == -1 
				&& modVariablesDialogueActions.indexOf(v) == -1 
				&& modVariablesDialogueActionsV204.indexOf(v) == -1 
				&& modVariablesDialogueActionsV300.indexOf(v) == -1 
				&& modVariablesDialogueActionsV301.indexOf(v) == -1 
				&& modVariablesDialogueActionsV302toV305.indexOf(v) == -1 
				&& modVariablesDAv401.indexOf(v) == -1 
				&& modVariablesDAv405.indexOf(v) == -1
				&& modVariablesDAv407.indexOf(v) == -1);
		}
	}

}