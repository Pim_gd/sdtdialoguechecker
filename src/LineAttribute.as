package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class LineAttribute extends LineElement {
		private var name:String;
		/**
		 * Value is either an Array, for set and check with multiple VariableReferences...
		 * Or a single String, containing the value of the named line-attribute.
		 */
		private var value:Object;
		
		public function LineAttribute(attributeName:String, attributeValue:Object) {
			name = attributeName;
			value = attributeValue;
		}
		
		public function getName():String {
			return name;
		}
		
		public function getValue():Object {
			return value;
		}
		
		public function getStrippedValue():String {
			return StringFunctions.stripQuotesIfNeeded((value as String));
		}
		
		public function getLineSegmentAsParsed():String {
			if (value is String) {
				return name + ":" + value;
			} else if (value is Array) {
				var result:String = name + ":{";
				//for loop, add to result.
				var valueArray:Array = value as Array;
				for (var i:uint = 0, isize:uint = valueArray.length; i < isize; i++) {
					var arrObject:Object = valueArray[i];
					if (arrObject is VariableReference) {
						var vr:VariableReference = arrObject as VariableReference;
						if (i != 0) {
							result += ",";
						}
						result += vr.getReferenceAsParsed();
					} else {
						ErrorManager.showDevelopmentError("LineAttribute::getContents - value is Array, but value at array index " + i + " is (" + arrObject + "), and is not a VariableReference");
					}
				}
				result += "}";
				return result;
			} else {
				ErrorManager.showDevelopmentError("LineAttribute::getContents - value is not String nor Array, value is (" + value + ")");
				return "";
			}
		}
		
		public override function getLineSegment():String {
			if (value is String) {
				return StringFunctions.addQuotes(name) + ":" + StringFunctions.addQuotes(value);
			} else if (value is Array) {
				var result:String = StringFunctions.addQuotes(name) + ":{";
				//for loop, add to result.
				var valueArray:Array = value as Array;
				for (var i:uint = 0, isize:uint = valueArray.length; i < isize; i++) {
					var arrObject:Object = valueArray[i];
					if (arrObject is VariableReference) {
						var vr:VariableReference = arrObject as VariableReference;
						if (i != 0) {
							result += ",";
						}
						result += vr.getReferenceAsString();
					} else {
						ErrorManager.showDevelopmentError("LineAttribute::getContents - value is Array, but value at array index " + i + " is (" + arrObject + "), and is not a VariableReference");
					}
				}
				result += "}";
				return result;
			} else {
				ErrorManager.showDevelopmentError("LineAttribute::getContents - value is not String nor Array, value is (" + value + ")");
				return "";
			}
		}
	}

}