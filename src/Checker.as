package {
	
	/**
	 * An abstract Checker class.
	 * @author Pimgd
	 */
	public /*abstract*/class Checker {
		protected var issues:Array; //of Issue
		protected var dialogueChecker:DialogueChecker; //In case more advanced functions are needed.
		
		public function Checker(dc:DialogueChecker) {
			dialogueChecker = dc;
			issues = new Array();
		}
		
		public function getIssues():Array {
			return issues;
		}
		
		public function getIssuesForLineNumber(lineNumber:uint):Array {
			var result:Array = new Array();
			if (issues[lineNumber] != null) {
				result = result.concat(issues[lineNumber]);
			}
			return result;
		}
		
		public /*abstract*/function checkDialogue(dialogue:Dialogue, options:Options):void {
			ErrorManager.showDevelopmentError("Checker::checkDialogue - an abstract function was called!");
		}
		
		public /*abstract*/function getName():String {
			ErrorManager.showDevelopmentError("Checker::getName - an abstract function was called!");
			return "";
		}
		
		private function getOrCreateIssueArrayForIssue(i:Issue):Array {
			var lineNumber:uint = i.getRelatedLineNumber();
			return ArrayFunctions.getOrCreateSubArray(issues, lineNumber);
		}
		
		protected function addIssue(i:Issue):void {
			getOrCreateIssueArrayForIssue(i).push(i);
		}
		
		protected function addIssueAsFirstForReportedLine(i:Issue):void {
			getOrCreateIssueArrayForIssue(i).unshift(i);
		}
	}

}