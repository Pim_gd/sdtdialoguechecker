package  
{
	/**
	 * Represents a trigger within a line in a Dialogue for SDT.
	 * @author Pimgd
	 */
	public class Trigger extends LineElement
	{
		private var type:TriggerType;
		private var content:String;
		public function Trigger(text:String, triggerType:TriggerType) 
		{
			content = text;
			type = triggerType;
		}
		public function getType():TriggerType {
			return type;
		}
		/**
		 * Given [TRIGGER],
		 * @return [TRIGGER]
		 */
		public override function getLineSegment():String {
			return "[" + content + "]";
		}
		/**
		 * Given [TRIGGER],
		 * @return TRIGGER
		 */
		public function getAction():String {
			return content;
		}
		public function containsVariableInsertion():Boolean {
			return content.indexOf("*") != -1;
		}
		public function getContentWithInsertionsReplaced(replacement:String):String {
			var splitted:Array = content.split("*");
			var result:String = splitted[0];
			for (var i:uint = 1; i < splitted.length; i++) {
				if (i % 2 == 1) {
					result += replacement;
				} else {
					result += splitted[i];
				}
			}
			return result;
		}
		/**
		 * Given [VA_SETVARIABLE_var1_5]
		 * @param	startIndex the index to start counting at. 0-indexed.
		 * @param	argumentIndex the index you want. 1-indexed (1 = first argument).
		 * @return  [VA_SETVARIABLE_var1_5] getArgument(2, 1) = var1. If out of bounds, returns null instead.
		 */
		public function getArgument(startIndex:uint, argumentIndex:uint):String {
			var splittedContent:Array = content.split("_");
			if (splittedContent.length >= startIndex + argumentIndex) {
				return splittedContent[startIndex + (argumentIndex - 1)];
			} else {
				return null;
			}
		}
		
		/**
		 * Negative result means there are no arguments at that start index.
		 */
		public function getArgumentCountBasedOnRawContent(startIndex:uint = 1):int {
			return (StringFunctions.countOccurances(content, "_") - (startIndex))+1;//A_B_C - start at B, that's B_C = 2 with only 1 occurance of _. So add 1.
		}
		
		/**
		 * Negative result means there are no arguments at that start index.
		 */
		public function getArgumentCountBasedOnEscapedContent(startIndex:uint = 1):int {
			return (StringFunctions.countOccurances(getContentWithInsertionsReplaced(""), "_") - (startIndex))+1;//A_B_C - start at B, that's B_C = 2 with only 1 occurance of _. So add 1.
		}
	}

}