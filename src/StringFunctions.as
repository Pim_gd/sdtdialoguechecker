package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class StringFunctions {
		
		public function StringFunctions() {
		
		}
		
		public static function addQuotes(str:*):String {
			return "\"" + str + "\"";
		}
		
		public static function stripQuotesIfNeeded(str:String):String {
			if (isQuoted(str)) {
				return str.substring(1, str.length - 1);
			}
			return str;
		}
		
		public static function isQuoted(str:String):Boolean {
			return stringStartsWith(str, "\"") && str.length >= 2 && stringEndsWith(str, "\"");
		}
		
		public static function stringStartsWith(str:String, startsWith:String):Boolean {
			if (str != null) {
				return startsWith != null && str.indexOf(startsWith) == 0;
			}
			return false;
		}
		
		public static function stringEndsWith(str:String, endsWith:String):Boolean {
			if (str != null && endsWith != null) {
				var lastIndex:int = str.lastIndexOf(endsWith);
				return lastIndex != -1 && lastIndex == (str.length - endsWith.length);
			}
			return false;
		}
		
		public static function replaceAll(str:String, match:String, replaceWith:String):String {
			var result:String = str;
			while (result.indexOf(match) != -1) {
				result = result.replace(match, replaceWith);
			}
			return result;
		}
		
		public static function countOccurances(str:String, match:String):uint {
			var arr:Array = str.split(match);
			return arr.length - 1;
		}
		
		public static function escapeRegexChars(s:String):String {
			var newString:String = s.replace(
				new RegExp("([{}\(\)\^$&.\*\?\/\+\|\[\\\\]|\]|\-)","g"),
				"\\$1");
			return newString;
		}
		
		public static function nthIndexOf(n:uint, str:String, match:String):int {
			var arr:Array = str.split(match);
			var endReached:Boolean = false;
			str = arr[0];
			if (n > 1) {
				for (var i:uint = 1, isize:uint = arr.length; i < isize && i <= n; i++) {
					str += match + arr[i];
					if (i == n) {
						return str.length;
					}
				}
				return -1;
			} else {
				return str.length;
			}
		}
		
		public static function getSnippet(line:String, match:String, startFrom:int = 0, snippetMaxLength:uint = 40):String {
			if (line.indexOf(match, startFrom) == -1) {
				return "Can't create snippet - parsing error";
			}
			var pieces:Array = line.split(match);
			var firstpiece:Boolean = true;
			for (var i:uint = 1, isize:uint = pieces.length; i < isize; i++) {
				if (pieces[0].length >= startFrom) {
					if (firstpiece) {
						firstpiece = false;
						pieces[1] = pieces[i];
					} else {
						pieces[1] += match + pieces[i];
					}
				} else {
					pieces[0] += match + pieces[i];
					if (pieces[0].length >= startFrom) {
						if ((i + 1) != isize) {
							pieces[1] = pieces[i + 1];
						} else {
							pieces[1] = "ERROR-CannotFindPiece"; //look at that hack
						}
					}
				}
			}
			var result:String = "";
			var spaceIndex:int = pieces[0].lastIndexOf(" ");
			if (spaceIndex == -1) {
				spaceIndex = 0;
			}
			if (spaceIndex + snippetMaxLength < pieces[0].length) {
				spaceIndex = pieces[0].length - snippetMaxLength;
			}
			if (spaceIndex != 0) {
				result += "(...)";
			}
			result += pieces[0].substring(spaceIndex) + match;
			spaceIndex = pieces[1].indexOf(" ");
			if (spaceIndex == -1) {
				spaceIndex = pieces[1].length - 1;
			}
			if ((spaceIndex - snippetMaxLength) > 0) {
				spaceIndex = snippetMaxLength;
			}
			result += pieces[1].substring(0, spaceIndex + 1);
			if (spaceIndex != pieces[1].length - 1) {
				result += "(...)";
			}
			return result;
		}
	}

}