package {
	
	/**
	 * needs to be cleaned up so it fits better in an OOP environment, but it'll do for now (now = v2.00)
	 * @author Pimgd
	 */
	public class Options {
		public static const DEFAULT_LINEBREAK:String = "\r\n";
		private var checkers:Array;
		private var severities:Array;
		private var linebreak:String = DEFAULT_LINEBREAK;
		private var settings:Array;
		private var singleUseSettings:Array;
		
		public function Options() {
			settings = new Array();
			checkers = new Array();
			severities = new Array();
			singleUseSettings = new Array();
		}
		
		public function setSetting(setting:String, value:Object, singleUse:Boolean):void {
			settings[setting] = value;
			if (singleUse) {
				singleUseSettings.push(setting);
			}
		}
		
		public function ignoreVariable(variableName:String):Boolean {
			return getSetting("IgnoreObjects") && variableName.indexOf(".") != -1;
		}
		
		public function getSetting(setting:String):Object {
			return settings[setting];
		}
		
		public function clearSingleUseSettings():void {
			for each (var setting:String in singleUseSettings) {
				delete settings[setting];
			}
			singleUseSettings = new Array();
		}
		
		public function getLineBreak():String {
			return linebreak;
		}
		
		public function setLineBreak(lb:String):void {
			linebreak = lb;
		}
		
		public function parseLineBreak(lb:String):void {
			var result:String = lb;
			result = StringFunctions.replaceAll(result, "\\r", "\r");
			result = StringFunctions.replaceAll(result, "\\n", "\n");
			setLineBreak(result);
		}
		
		public function addSeverity(s:Severity):void {
			severities.push(s);
		}
		
		public function addChecker(c:String):void {
			checkers.push(c);
		}
		
		public function getSeverities():Array {
			return severities;
		}
		
		public function getCheckers():Array {
			return checkers;
		}
		
		public function hasChecker(c:String):Boolean {
			return checkers.indexOf(c) != -1;
		}
		
		public function hasSeverity(s:Severity):Boolean {
			return severities.indexOf(s) != -1;
		}
	}

}