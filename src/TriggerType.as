package {
	/**
	 * ...
	 * @author Pimgd
	 */
	public class TriggerType extends Enum {
		private static const enums:Array = Enum.loadValuesForEnum(TriggerType);
		public static const MOOD:TriggerType = enums["MOOD"];
		public static const ARMS:TriggerType = enums["ARMS"];
		public static const HOLDING:TriggerType = enums["HOLDING"];
		public static const DEFAULT:TriggerType = enums["DEFAULT"];
		public static const CUSTOM:TriggerType = enums["CUSTOM"];
		public static const DEPRECATED:TriggerType = enums["DEPRECATED"];
		public static const LINEREFERENCE:TriggerType = enums["LINEREFERENCE"];
		public static const ERROR:TriggerType = enums["ERROR"];
		{buildTriggerArray() };
		
		/**
		 * String indexed array. Its use is to speed up performance.
		 * It contains TriggerString -> Enum mappings.
		 * Like this, it can check for a trigger via simpleTriggers[trigger] != null.
		 */
		private static var simpleTriggers:Array = new Array();
		private static var simpleTriggersIterable:Array = new Array();
		
		/**
		 * Contains Objects with .trigger and .dependency
		 * When a trigger STARTS WITH one of the values in this array, then it is of CUSTOM type.
		 */
		private static var importedComplexTriggers:Array = new Array();
		
		private static var importedSimpleTriggers:Array = new Array();
		private static var importedSimpleTriggersIterable:Array = new Array();
		
		public function TriggerType(val:uint, ID:String) {
			super(val, ID);
		}
		
		private static function addSimpleTriggers(triggers:Array, type:TriggerType):void {
			for each (var triggerName:String in triggers) {
				if (simpleTriggers[triggerName] == undefined) {
					simpleTriggersIterable.push(triggerName);
				}
				simpleTriggers[triggerName] = type;
			}
		}
		
		public static function registerComplexTriggerMapping(trigger:String, dependency:String):void {
			var obj:Object = new Object();
			obj.trigger = trigger;
			obj.dependency = dependency;
			importedComplexTriggers.push(obj);
		}
		
		public static function registerSimpleTriggerMapping(trigger:String, dependency:String):void {
			if (importedSimpleTriggers[trigger] == undefined) {
				importedSimpleTriggersIterable.push(trigger);
			}
			importedSimpleTriggers[trigger] = dependency;
		}
		
		private static function buildTriggerArray():void {
			var SDTMoodTriggers:Array = ["AHEGAO_MOOD", "ANGRY_MOOD", "HAPPY_MOOD", "NORMAL_MOOD"];
			addSimpleTriggers(SDTMoodTriggers, MOOD);
			
			var SDTArmsTriggers:Array = ["ARMS_BACK", "ARMS_HAND_JOB", "ARMS_HIS_LEGS", "ARMS_LEGS", "ARMS_LOOSE", "LEFT_ARM_BACK", "LEFT_ARM_HAND_JOB", "LEFT_ARM_HIS_LEGS", "LEFT_ARM_LEGS", "LEFT_ARM_LOOSE", "RIGHT_ARM_BACK", "RIGHT_ARM_HAND_JOB", "RIGHT_ARM_HIS_LEGS", "RIGHT_ARM_LEGS", "RIGHT_ARM_LOOSE"];
			addSimpleTriggers(SDTArmsTriggers, ARMS);
			
			var SDTHoldingTriggers:Array = ["HOLD", "RELEASE"];
			addSimpleTriggers(SDTHoldingTriggers, HOLDING);
			
			var SDTVanillaTriggers:Array = ["ADD_TEARS", "BLINK", "CLENCH_TEETH", "CLOSE_EYES", "COUGH", "DROOL", "EJACULATE", "FACE_FUCK_STYLE", "LOOK_UP", "LOOK_DOWN", "NORMAL_STYLE", "OPEN_EYES", "SHOCK", "SWALLOW", "TAP_HANDS", "WINCE"];
			addSimpleTriggers(SDTVanillaTriggers, DEFAULT);
			
			//custom
			var DialogueActionsTriggers:Array = ["ARMS_BREAST", "ARMS_CROTCH", "ARMS_HIS_CROTCH", "AUTO_HARD", "AUTO_KEYS", "AUTO_KEYS_OFF", "AUTO_KEYS_ON", "AUTO_NORMAL", "AUTO_OFF", "AUTO_SELF", "AUTO_SOFT", "BOUNCE_TITS", "CHANGE_BACKGROUND", "CLEAN_ALL", "CLEAN_CUM", "CLEAN_LIPSTICK", "CLEAN_MASCARA", "CLEAN_SPIT", "CLEAR", "CONTINUE", "DEEPTHROAT", "END_FLASH", "GAG", "GROW_PENIS", "HIDE_BALLS", "HIDE_HIM", "HIDE_PENIS", "LEFT_ARM_BREAST", "LOAD_CHARCODE", "MASTURBATE_OFF", "MASTURBATE_ON", "MOAN", "PAUSE", "PULL_OFF", "RANDOMIZE_HER", "RIGHT_ARM_BREAST", "RUB_HIS_CROTCH_OFF", "RUB_HIS_CROTCH_ON", "SHOW_HIM", "SHRINK_PENIS", "SHOW_BALLS", "SHOW_PENIS", "WAKE_UP"];
			var BETriggers:Array = ["ADD_EFFECTIVE_CUM", "PUKE", "SUB_EFFECTIVE_CUM"];
			addSimpleTriggers(DialogueActionsTriggers, CUSTOM);
			addSimpleTriggers(BETriggers, CUSTOM);
			
			var animtoolsTriggers:Array = ["ANIMTOOLSRANDOM","ANIMTOOLSSETVOICECHAR0","ANIMTOOLSSETVOICECHAR1","ANIMTOOLSSETVOICECHAR2","ANIMTOOLSSETVOICECHAR3","ANIMTOOLSSETVOICECHAR4","ANIMTOOLSSETVOICECHAR5","ANIMTOOLSSETVOICECHAR6","ANIMTOOLSSETVOICECHAR7","ANIMTOOLSDISABLEHUNNIESOUNDS","ANIMTOOLSENABLEHUNNIESOUNDS"];
			addSimpleTriggers(animtoolsTriggers, CUSTOM);
			
			var DAv204SimpleTriggers:Array = ["BOUNCE_TITS", "END_FLASH", "INSTANT_END_FLASH", "AUTO_OFF", "AUTO_SOFT", "AUTO_NORMAL", "AUTO_HARD", "AUTO_SELF", "HIDE_PENIS", "SHOW_PENIS", "HIDE_BALLS", "SHOW_BALLS", "CLEAN_CUM", "CLEAN_MASCARA", "CLEAN_SPIT", "CLEAN_LIPSTICK", "CLEAN_ALL", "GAG", "MOAN", "AUTO_KEYS", "AUTO_KEYS_ON", "AUTO_KEYS_OFF", "PAUSE", "CONTINUE", "PULL_OFF", "HIDE_HIM", "SHOW_HIM", "HIDE_HIS_ARM", "SHOW_HIS_ARM", "CUM", "ARMS_HIS_CROTCH", "ARMS_CROTCH", "RIGHT_ARM_BREAST", "LEFT_ARM_BREAST", "ARMS_BREAST", "RUB_HIS_CROTCH_ON", "RUB_HIS_CROTCH_OFF", "FIX_ARMS", "FIX_LEFT_ARM", "FIX_RIGHT_ARM", "TONGUE_IN", "TONGUE_OUT", "LOAD_HAIR", "CHANGE_BACKGROUND", "LOAD_CHARCODE", "WAKE_UP", "KNOCK_OUT", "DEEPTHROAT", "MASTURBATE_ON", "MASTURBATE_OFF", "MASTURBATE_STOP", "RANDOMIZE_HER", "RANDOMIZE_HER_BODY", "RESET_RESIST"];
			addSimpleTriggers(DAv204SimpleTriggers, CUSTOM);
			
			var WWDAv300Triggers:Array = ["SLAP", "OW", "AH_HARD", "OH_HARD", "OH_SOFT", "LOAD_MOD", "CLEAR_MOD"];
			addSimpleTriggers(WWDAv300Triggers, CUSTOM);
			
			for (var i:uint = 0; i <= 10; i++) {
				var WWDAv300ButtonTriggers:Array = ["BUTTON" + i + "_ON", "BUTTON" + i + "_OFF"];
				if (i == 0) { //dammit Pim stop being so lazy
					WWDAv300ButtonTriggers = ["BUTTONALL_ON", "BUTTONALL_OFF", "BUTTONALL_CLEAR"];
				}
				addSimpleTriggers(WWDAv300ButtonTriggers, CUSTOM);
			}
			
			addSimpleTriggers(["STOP_BGM"], CUSTOM); //DA v3.05
			addSimpleTriggers(["STOP_SFX"], CUSTOM); //DA v4.01
			addSimpleTriggers(["LOAD_CHARCODE2"], CUSTOM); //DA v4.02
			addSimpleTriggers(["LOAD_FULL_CHARCODE"], CUSTOM); //DA v4.04
			
			addSimpleTriggers(["CUM_BLOCK_ON"], CUSTOM); //DA v4.05
			addSimpleTriggers(["CUM_BLOCK_OFF"], CUSTOM); //DA v4.05
			
			var DialogueActionsTriggersDeprecated:Array = ["HANDJOB_OFF", "HANDJOB_ON", "HAND_OFF", "HAND_ON"];
			addSimpleTriggers(DialogueActionsTriggersDeprecated, DEPRECATED);
		
		}
		
		public static function getAllEnums():Array {
			var tempArray:Array = new Array();
			for each (var triggerType:TriggerType in enums) {
				tempArray[triggerType.getValue()] = triggerType;
			}
			return tempArray;
		}
		
		public static function isSimpleTrigger(trigger:String):Boolean {
			return simpleTriggers[trigger] != null;
		}
		
		public static function identifyType(trigger:String):TriggerType {
			var foundTriggerType:TriggerType = simpleTriggers[trigger];
			if (foundTriggerType != null) {
				return foundTriggerType;
			}
			
			//It turns out empty trigger IS valid, albeit VERY VERY VERY VERY wrong looking.
			/*if (trigger == "") {
			   return getEnum("ERROR");
			 }*/
			 
			if (importedSimpleTriggers[trigger] != undefined) {
				return CUSTOM;
			}
			 
			for (var i:uint = 0; i < importedComplexTriggers.length; i++) {
				if (trigger.indexOf(importedComplexTriggers[i].trigger) == 0) {
					return CUSTOM;
				}
			}
			
			if (trigger.indexOf("_") == -1) { //all complex triggers contain underscores.
				return LINEREFERENCE;
			}
			if (trigger.indexOf("ANIMTOOLS_") == 0) {
				return CUSTOM;
			}
			
			var DAv204ComplexTriggers:Array = ["BOUNCE_TITS_", "INSTANT_FLASH_", "FLASH_", "FLASH_CHANGE_COLOR_", "FADE_BACKGROUND_", "VA_SET_GLOBALVARIABLE_", "VA_SET_GLOBALVARIABLEBYNAME_", "VA_LOAD_GLOBALVARIABLE_", "VA_SET_VARIABLE_", "VA_SET_VARIABLEBYNAME_"];
			for (var i:uint = 0; i < DAv204ComplexTriggers.length; i++) {
				if (trigger.indexOf(DAv204ComplexTriggers[i]) == 0) {
					return CUSTOM;
				}
			}
			var WWDAv300ComplexTriggers:Array = ["VA_SAVE_SAVEGAME_", "VA_LOAD_SAVEGAME_", "VA_CLEAR_SAVEGAME_"];
			for (var i:uint = 0; i < WWDAv300ComplexTriggers.length; i++) {
				if (trigger.indexOf(WWDAv300ComplexTriggers[i]) == 0) {
					return CUSTOM;
				}
			}
			
			var DAv301ComplexTriggers:Array = ["SETVOICE_", "SETVAR_", "SETVARBYNAME_", "SETGLOBAL_", "SETGLOBALBYNAME_", "COPYOBJECT_", "DEFINEOBJECT_", "REGISTERGLOBALS_", "UNREGISTERGLOBALS_", "DELETEGLOBALS_", "DELETELOCALS_", "LOADGLOBALS_", "GETGLOBALS_", "SETGLOBALS_", "SAVESETVARS_", "SAVEGETVARS_", "SAVEREMOVEVARS_", "SAVEDELETE_"];
			for (var i:uint = 0; i < DAv301ComplexTriggers.length; i++) {
				if (trigger.indexOf(DAv301ComplexTriggers[i]) == 0) {
					return CUSTOM;
				}
			}
			
			var DAv302tov305Complex:Array = ["SETVARBYNAEM_", "SETGLOBALBYNAEM_", "PLAY_BGM_"];
			for (var i:uint = 0; i < DAv302tov305Complex.length; i++) {
				if (trigger.indexOf(DAv302tov305Complex[i]) == 0) {
					return CUSTOM;
				}
			}
			
			var DAv400Complex:Array = ["APPENDVAR_", "APPENDVARBYNAME_", "APPENDVARBYNAEM_"];
			for (var i:uint = 0; i < DAv400Complex.length; i++) {
				if (trigger.indexOf(DAv400Complex[i]) == 0) {
					return CUSTOM;
				}
			}
			
			if (trigger.indexOf("CLEARLINES_") == 0) {
				return CUSTOM;
			}
			
			if (trigger.indexOf("PUSSYDRIP_") == 0) {
				return CUSTOM;
			}
			
			if (trigger.indexOf("PLAY_SFX_") == 0) {
				return CUSTOM;
			}
			
			if (trigger.indexOf("dialogdisplayedit_") == 0) {
				return CUSTOM;
			}
			
			//assume linereference
			return LINEREFERENCE;
		}
		
		/**
		 * Given a start and an end of a potential trigger, gives you a list of simple triggers that it could be
		 * Useful for checking if [intro*variable + expression*_2] and [BUTTON*variable + expression*_ON] are line-reference triggers or action-triggers
		 * @return Array of String containing simple triggers only
		 */
		public static function getPossibleTriggersBasedOnStartAndEndOfTrigger(start:String, end:String):Array {
			var candidates:Array = [];
			for (var i:uint = 0; i < simpleTriggersIterable.length; i++) {
				if (StringFunctions.stringStartsWith(simpleTriggersIterable[i], start) && StringFunctions.stringEndsWith(simpleTriggersIterable[i], end)) {
					candidates.push(simpleTriggersIterable[i]);
				}
			}
			
			for (var i:uint = 0; i < importedSimpleTriggersIterable.length; i++) {
				if (StringFunctions.stringStartsWith(importedSimpleTriggersIterable[i], start) && StringFunctions.stringEndsWith(importedSimpleTriggersIterable[i], end)) {
					candidates.push(importedSimpleTriggersIterable[i]);
				}
			}
			
			return candidates;
		}
		
		public static function getPossibleTriggerTypesBasedOnStartAndEndOfTrigger(start:String, end:String):Array {
			var triggers:Array = getPossibleTriggersBasedOnStartAndEndOfTrigger(start, end);
			var distinctTriggerTypes:Array = [];
			for (var i:uint = 0; i < triggers.length; i++) {
				ArrayFunctions.addIfNotExists(distinctTriggerTypes, identifyType(triggers[i]));
			}
			return distinctTriggerTypes;
		}
		
		public function getDependentMod(trigger:String):String {
			if (!(this == CUSTOM || this == DEPRECATED)) {
				return "";
			}
			
			if (importedSimpleTriggers[trigger] != undefined) {
				return importedSimpleTriggers[trigger];
			}
			
			for (var i:uint = 0; i < importedComplexTriggers.length; i++) {
				if (trigger.indexOf(importedComplexTriggers[i].trigger +"_") == 0) {
					return importedComplexTriggers[i].dependency;
				}
			}
			
			var DialogueActionsTriggers:Array = ["ARMS_BREAST", "ARMS_CROTCH", "ARMS_HIS_CROTCH", "AUTO_HARD", "AUTO_KEYS", "AUTO_KEYS_OFF", "AUTO_KEYS_ON", "AUTO_NORMAL", "AUTO_OFF", "AUTO_SELF", "AUTO_SOFT", "BOUNCE_TITS", "CHANGE_BACKGROUND", "CLEAN_ALL", "CLEAN_CUM", "CLEAN_LIPSTICK", "CLEAN_MASCARA", "CLEAN_SPIT", "CLEAR", "CONTINUE", "DEEPTHROAT", "END_FLASH", "GAG", "GROW_PENIS", "HANDJOB_OFF", "HANDJOB_ON", "HAND_OFF", "HAND_ON", "HIDE_BALLS", "HIDE_HIM", "HIDE_PENIS", "LEFT_ARM_BREAST", "LOAD_CHARCODE", "MASTURBATE_OFF", "MASTURBATE_ON", "MOAN", "PAUSE", "PULL_OFF", "RANDOMIZE_HER", "RIGHT_ARM_BREAST", "RUB_HIS_CROTCH_OFF", "RUB_HIS_CROTCH_ON", "SHOW_HIM", "SHRINK_PENIS", "SHOW_BALLS", "SHOW_PENIS", "WAKE_UP"];
			var BETriggers:Array = ["ADD_EFFECTIVE_CUM", "PUKE", "SUB_EFFECTIVE_CUM"];
			if (DialogueActionsTriggers.indexOf(trigger) != -1) {
				return "DialogueActions by gollum/Pimgd";
			}
			if (BETriggers.indexOf(trigger) != -1) {
				return "BreastExpansionPlus by sby";
			}
			
			var animtoolsTriggers:Array = ["ANIMTOOLSRANDOM","ANIMTOOLSSETVOICECHAR0","ANIMTOOLSSETVOICECHAR1","ANIMTOOLSSETVOICECHAR2","ANIMTOOLSSETVOICECHAR3","ANIMTOOLSSETVOICECHAR4","ANIMTOOLSSETVOICECHAR5","ANIMTOOLSSETVOICECHAR6","ANIMTOOLSSETVOICECHAR7","ANIMTOOLSDISABLEHUNNIESOUNDS","ANIMTOOLSENABLEHUNNIESOUNDS"];
			if (trigger.indexOf("ANIMTOOLS_") == 0 || animtoolsTriggers.indexOf(trigger) != -1) {
				return "Animtools by sby";
			}
			
			if (trigger.indexOf("dialogdisplayedit_") == 0) {
				return "dialoguedisplayedit by sby";
			}
			
			if (trigger.indexOf("FADE_BACKGROUND_") == 0 || trigger.indexOf("FLASH_") == 0) {
				return "DialogueActions by gollum/Pimgd";
			}
			var DAv204SimpleTriggers:Array = ["BOUNCE_TITS", "END_FLASH", "INSTANT_END_FLASH", "AUTO_OFF", "AUTO_SOFT", "AUTO_NORMAL", "AUTO_HARD", "AUTO_SELF", "HIDE_PENIS", "SHOW_PENIS", "HIDE_BALLS", "SHOW_BALLS", "CLEAN_CUM", "CLEAN_MASCARA", "CLEAN_SPIT", "CLEAN_LIPSTICK", "CLEAN_ALL", "GAG", "MOAN", "AUTO_KEYS", "AUTO_KEYS_ON", "AUTO_KEYS_OFF", "PAUSE", "CONTINUE", "PULL_OFF", "HIDE_HIM", "SHOW_HIM", "HIDE_HIS_ARM", "SHOW_HIS_ARM", "CUM", "ARMS_HIS_CROTCH", "ARMS_CROTCH", "RIGHT_ARM_BREAST", "LEFT_ARM_BREAST", "ARMS_BREAST", "RUB_HIS_CROTCH_ON", "RUB_HIS_CROTCH_OFF", "FIX_ARMS", "FIX_LEFT_ARM", "FIX_RIGHT_ARM", "TONGUE_IN", "TONGUE_OUT", "LOAD_HAIR", "CHANGE_BACKGROUND", "LOAD_CHARCODE", "WAKE_UP", "KNOCK_OUT", "DEEPTHROAT", "MASTURBATE_ON", "MASTURBATE_OFF", "MASTURBATE_STOP", "RANDOMIZE_HER", "RANDOMIZE_HER_BODY", "RESET_RESIST"];
			if (DAv204SimpleTriggers.indexOf(trigger) != -1) {
				return "DialogueActions v2.04 by gollum/Pimgd";
			}
			var DAv204ComplexTriggers:Array = ["BOUNCE_TITS_", "INSTANT_FLASH_", "FLASH_", "FLASH_CHANGE_COLOR_", "FADE_BACKGROUND_", "VA_SET_GLOBALVARIABLE_", "VA_SET_GLOBALVARIABLEBYNAME_", "VA_LOAD_GLOBALVARIABLE_", "VA_SET_VARIABLE_", "VA_SET_VARIABLEBYNAME_"];
			for (var i:uint = 0; i < DAv204ComplexTriggers.length; i++) {
				if (DAv204ComplexTriggers[i].indexOf(trigger) == 0) {
					return "DialogueActions v2.04 by gollum/Pimgd";
				}
			}
			var WWDAv300Triggers:Array = ["SLAP", "OW", "AH_HARD", "OH_HARD", "OH_SOFT", "LOAD_MOD", "CLEAR_MOD"];
			if (WWDAv300Triggers.indexOf(trigger) != -1) {
				return "DialogueActions v3.00 by WeeWillie";
			}
			var WWDAv300ComplexTriggers:Array = ["VA_SAVE_SAVEGAME_", "VA_LOAD_SAVEGAME_", "VA_CLEAR_SAVEGAME_"];
			for (var i:uint = 0; i < WWDAv300ComplexTriggers.length; i++) {
				if (trigger.indexOf(WWDAv300ComplexTriggers[i]) == 0) {
					return "DialogueActions v3.00 by WeeWillie";
				}
			}
			for (var i:uint = 0; i <= 10; i++) {
				var WWDAv300ButtonTriggers:Array = ["BUTTON" + i + "_ON", "BUTTON" + i + "_OFF"];
				if (i == 0) { //dammit Pim
					WWDAv300ButtonTriggers = ["BUTTONALL_ON", "BUTTONALL_OFF", "BUTTONALL_CLEAR"];
				}
				if (WWDAv300ButtonTriggers.indexOf(trigger) != -1) {
					return "DialogueActions v3.00 by WeeWillie";
				}
			}
			
			var DAv301ComplexTriggers:Array = ["SETVOICE_", "SETVAR_", "SETVARBYNAME_", "SETGLOBAL_", "SETGLOBALBYNAME_", "COPYOBJECT_", "DEFINEOBJECT_", "REGISTERGLOBALS_", "UNREGISTERGLOBALS_", "DELETEGLOBALS_", "DELETELOCALS_", "LOADGLOBALS_", "GETGLOBALS_", "SETGLOBALS_", "SAVESETVARS_", "SAVEGETVARS_", "SAVEREMOVEVARS_", "SAVEDELETE_"];
			for (var i:uint = 0; i < DAv301ComplexTriggers.length; i++) {
				if (trigger.indexOf(DAv301ComplexTriggers[i]) == 0) {
					return "DialogueActions v3.01 by WeeWillie/Pimgd";
				}
			}
			
			var DAv302tov305Complex:Array = ["SETVARBYNAEM_", "SETGLOBALBYNAEM_", "PLAY_BGM_"];
			for (var i:uint = 0; i < DAv302tov305Complex.length; i++) {
				if (trigger.indexOf(DAv302tov305Complex[i]) == 0) {
					return "DialogueActions v3.02 - v3.05 by Pimgd";
				}
			}
			
			var DAv400Complex:Array = ["APPENDVAR_", "APPENDVARBYNAME_", "APPENDVARBYNAEM_"];
			for (var i:uint = 0; i < DAv400Complex.length; i++) {
				if (trigger.indexOf(DAv400Complex[i]) == 0) {
					return "DialogueActions v4.00 by Pimgd";
				}
			}
			
			if (trigger.indexOf("PLAY_SFX_") == 0) {
				return "DialogueActions v4.01 by Pimgd";
			}
			
			if (trigger == "STOP_SFX") {
				return "DialogueActions v4.01 by Pimgd";
			}
			
			if (trigger == "LOAD_CHARCODE2") {
				return "DialogueActions v4.02 by Pimgd";
			}
			
			if (trigger == "LOAD_FULL_CHARCODE") {
				return "DialogueActions v4.04 by Pimgd";
			}
			
			if (trigger == "CUM_BLOCK_ON" || trigger == "CUM_BLOCK_OFF") {
				return "DialogueActions v4.05 by Pimgd";
			}
			
			if (trigger.indexOf("CLEARLINES_") == 0 || trigger.indexOf("PUSSYDRIP_") == 0) {
				return "DialogueActions v4.07 by Pimgd";
			}
			
			if (trigger == "STOP_BGM") { //DA v3.05
				return "DialogueActions v3.02 - v3.05 by Pimgd";
			}
			if (trigger.indexOf("VA_") == 0) {
				return "VariableArithmetic by Pimgd";
			}
			
			return "";
		}
	}

}