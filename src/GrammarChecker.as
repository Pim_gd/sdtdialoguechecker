package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class GrammarChecker extends Checker {
		
		public function GrammarChecker(dc:DialogueChecker) {
			super(dc);
		}
		
		public override function getName():String {
			return "GrammarChecker";
		}
		
		public override function checkDialogue(dialogue:Dialogue, options:Options):void {
			var lines:Array = dialogue.getLines();
			for each (var line:Line in lines) {
				checkLine(line, dialogue, options);
			}
		}
		
		private function checkLine(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			var content:Array = l.getContent();
			checkLineOldStyle(l, "", false, dialogue, options);
		}
		
		private function checkLineOldStyle(l:Line, lineString:String, varsubsReplaced:Boolean = false, dialogue:Dialogue = null, options:Options = null):void {
			if (l.isCommentLine()) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			
			var linebreak:String = Options.DEFAULT_LINEBREAK;
			if (options != null) {
				linebreak = options.getLineBreak();
			}
			
			var line:String = l.getRaw();
			var showStrippedVersion:Boolean = false;
			var issue:Issue = null;
			var issueMessageDefault:String = "";
			var lineContainsYOU:Boolean = (l.getRaw().indexOf("YOU") != -1);
			if (lineString != "") {
				issueMessageDefault = "The following issue was found after ";
				if (options != null) {
					if (options.getSetting("substitutes") && varsubsReplaced) {
						issueMessageDefault += "replacing variable substitutions, and ";
					}
					issueMessageDefault += "stripping the line of "
				} else {
					issueMessageDefault += "stripping the line of variable substitutions, ";
				}
				issueMessageDefault += "delay characters and triggers";
				if (lineContainsYOU) {
					issueMessageDefault += "." + linebreak;
					issueMessageDefault += "Keep in mind that you can place characters in YOU/YOUR/ME/MY variable insertions, like \"Hello*, YOU*!\"." + linebreak;
				} else {
					issueMessageDefault += ":" + linebreak;
				}
				line = lineString;
				showStrippedVersion = true;
			}
			var issueMessage:String = issueMessageDefault;
			var issueSnippet:String = "";
			if (line.indexOf("\"") != -1 && line.indexOf("\"") == line.indexOf("\",")) {
				issueSnippet = StringFunctions.getSnippet(line, "\",");
				issueMessage += "Sentence starting with a comma (grammatical error) on dialogue line on line " + l.getLineNumber() + ", col " + (line.indexOf("\",")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf("\"") != -1 && line.indexOf("\"") == line.indexOf("\"! ")) {
				issueSnippet = StringFunctions.getSnippet(line, "\"! ");
				issueMessage += "Sentence starting with a disconnected exclamation mark (grammatical error) on dialogue line on line " + l.getLineNumber() + ", col " + (line.indexOf("\"! ")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf("\"") != -1 && line.indexOf("\"") == line.indexOf("\". ")) {
				issueSnippet = StringFunctions.getSnippet(line, "\". ");
				issueMessage += "Sentence starting with a disconnected period (grammatical error) on dialogue line on line " + l.getLineNumber() + ", col " + (line.indexOf("\". ")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(" ,") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, " ,");
				issueMessage += "Space before comma (grammatical error) on dialogue line on line " + l.getLineNumber() + ", col " + (line.indexOf(" ,")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(",,") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ",,");
				issueMessage += "Double comma (grammatical error) on dialogue line on line " + l.getLineNumber() + ", col " + (line.indexOf(",,")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf("  ") != -1 && (options == null || !options.getSetting("NoDoubleSpace"))) {
				var splitLine:Array = line.split("\"");
				var doubleSpaceInBadLocation:Boolean = false;
				for (var i:uint = 1, isize:uint = splitLine.length; i < isize; i += 2) { //after first quotation mark, after third, after 5th... blah"HERE blah blah" fff {"HEREset":{"HEREvar":"HEREvalue"}}
					if (splitLine[i].indexOf("  ") != -1) {
						doubleSpaceInBadLocation = true;
					}
				}
				if (doubleSpaceInBadLocation) { //to prevent lines with whitespace in excess from tripping errors
					issueSnippet = StringFunctions.getSnippet(line, "  ");
					issueMessage += "Double space on line " + l.getLineNumber() + ", col " + (line.indexOf("  ")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += "You can disable this check manually by putting #DialogueChecker-NoDoubleSpace# at the top of your dialogue if you prefer to keep double spaces without the checker complaining about them." + linebreak;
					issueMessage += l.printLineMessage(linebreak);
					if (showStrippedVersion) {
						issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
					}
					issueMessage += linebreak;
					issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
					addIssue(issue);
					issueMessage = issueMessageDefault;
				}
			}
			if (line.indexOf(" !") != -1 && !l.isIndexInSubstitution(line.indexOf(" !"))) {
				issueSnippet = StringFunctions.getSnippet(line, " !");
				issueMessage += "Grammatical error (space before exclamation mark) on line " + l.getLineNumber() + ", col " + (line.indexOf(" !")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(" ?") != -1 && !l.isIndexInSubstitution(line.indexOf(" ?"))) {
				issueSnippet = StringFunctions.getSnippet(line, " ?");
				issueMessage += "Grammatical error (space before question mark) on line " + l.getLineNumber() + ", col " + (line.indexOf(" ?")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(",!") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ",!");
				issueMessage += "Grammatical error (comma directly before exclamation mark) on line " + l.getLineNumber() + ", col " + (line.indexOf(",!")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(",?") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ",?");
				issueMessage += "Grammatical error (comma directly before question mark) on line " + l.getLineNumber() + ", col " + (line.indexOf(",?")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf("!,") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "!,");
				issueMessage += "Grammatical error (comma directly after exclamation mark) on line " + l.getLineNumber() + ", col " + (line.indexOf("!,")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf("?,") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "?,");
				issueMessage += "Grammatical error (comma directly after question mark) on line " + l.getLineNumber() + ", col " + (line.indexOf("?,")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(".,.") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ".,.");
				issueMessage += "Grammatical error (comma amidst periods: .,.) on line " + l.getLineNumber() + ", col " + (line.indexOf(".,.")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			} else {
				if (line.indexOf(".,") != -1) {
					if ((line.indexOf("...,") + 2) != line.indexOf(".,")) {
						issueSnippet = StringFunctions.getSnippet(line, ".,");
						issueMessage += "Grammatical error (period before comma ending: .,) on line " + l.getLineNumber() + ", col " + (line.indexOf(".,")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				} else if (line.indexOf(",.") != -1) {
					if (line.indexOf(",...") != line.indexOf(",.")) {
						issueSnippet = StringFunctions.getSnippet(line, ",.");
						issueMessage += "Grammatical error (comma before period ending: ,.) on line " + l.getLineNumber() + ", col " + (line.indexOf(".,")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				}
			}
			if (line.indexOf(", .") != -1) {
				if (line.indexOf(", ...") != line.indexOf(", .")) {
					issueSnippet = StringFunctions.getSnippet(line, ", .");
					issueMessage += "Grammatical error (\", .\" - proper comma, but then sentence ends) on line " + l.getLineNumber() + ", col " + (line.indexOf(", .")) + " near \"" + issueSnippet + "\"" + linebreak;
					issueMessage += l.printLineMessage(linebreak);
					if (showStrippedVersion) {
						issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
					}
					issueMessage += linebreak;
					issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
					addIssue(issue);
					issueMessage = issueMessageDefault;
				}
			}
			if (line.indexOf(".!.") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ".!.");
				issueMessage += "Grammatical error (Exclamation mark amidst periods: .!.) on line " + l.getLineNumber() + ", col " + (line.indexOf(".!.")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			} else {
				if (line.indexOf("!.") != -1) {
					if (line.indexOf("!...") != line.indexOf("!.")) {
						issueSnippet = StringFunctions.getSnippet(line, "!.");
						issueMessage += "Grammatical error (double sentence ending: !.) on line " + l.getLineNumber() + ", col " + (line.indexOf("!.")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				}
				if (line.indexOf(".!") != -1) {
					if ((line.indexOf("...!") + 2) != line.indexOf(".!")) {
						issueSnippet = StringFunctions.getSnippet(line, ".!");
						issueMessage += "Grammatical error (double sentence ending: .!) on line " + l.getLineNumber() + ", col " + (line.indexOf(".!")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				}
			}
			if (line.indexOf(".?.") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, ".?.");
				issueMessage += "Grammatical error (Question mark amidst periods: .?.) on line " + l.getLineNumber() + ", col " + (line.indexOf(".?.")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			} else {
				if (line.indexOf("?.") != -1) {
					if (line.indexOf("?...") != line.indexOf("?.")) {
						issueSnippet = StringFunctions.getSnippet(line, "?.");
						issueMessage += "Grammatical error (double sentence ending: ?.) on line " + l.getLineNumber() + ", col " + (line.indexOf("?.")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				}
				if (line.indexOf(".?") != -1) {
					if ((line.indexOf("...?") + 2) != line.indexOf(".?")) {
						issueSnippet = StringFunctions.getSnippet(line, ".?");
						issueMessage += "Grammatical error (double sentence ending: .?) on line " + l.getLineNumber() + ", col " + (line.indexOf(".?")) + " near \"" + issueSnippet + "\"" + linebreak;
						issueMessage += l.printLineMessage(linebreak);
						if (showStrippedVersion) {
							issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
						}
						issueMessage += linebreak;
						issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
						addIssue(issue);
						issueMessage = issueMessageDefault;
					}
				}
			}
			
			if (line.indexOf("%0A ") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, "%0A ");
				issueMessage += "Space at start of new line on line " + l.getLineNumber() + ", col " + (line.indexOf("%0A ")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(":\" ") != -1 && (options == null || !options.getSetting("NoLeadingSpace"))) {
				issueSnippet = StringFunctions.getSnippet(line, ":\" ");
				issueMessage += "Space at start of new line on line " + l.getLineNumber() + ", col " + (line.indexOf(":\" ")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (line.indexOf(" . ") != -1) {
				issueSnippet = StringFunctions.getSnippet(line, " . ");
				issueMessage += "Space before period for new line on line " + l.getLineNumber() + ", col " + (line.indexOf(" . ")) + " near \"" + issueSnippet + "\"" + linebreak;
				issueMessage += l.printLineMessage(linebreak);
				if (showStrippedVersion) {
					issueMessage += "Stripped Line " + l.getLineNumber() + ": " + lineString + linebreak;
				}
				issueMessage += linebreak;
				issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Grammar");
				addIssue(issue);
				issueMessage = issueMessageDefault;
			}
			if (issue == null) {
				if (lineString == "") {
					if (options == null) {
						checkLineOldStyle(l, l.getLineStripped(true, true, true), false, dialogue, options);
					} else {
						checkLineOldStyle(l, l.getLineStripped(true, true, options.getSetting("substitutes")), false, dialogue, options);
					}
				} else if (!varsubsReplaced) {
					if (options == null || options.getSetting("substitutes")) {
						checkLineOldStyle(l, l.getLineStrippedLiteralSubs(true, true), true, dialogue, options);
					}
				}
			}
		}
	}

}