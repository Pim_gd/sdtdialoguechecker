package {
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class VariableChecker extends Checker {
		private var declaredVariables:Array;
		private var usedVariables:Array; //if a declared variable is not in here, it's bad.
		private var badVariables:Array; //if the line's attribute string contains a bad variable in content *var* or attribute "var": context, it's bad
		
		public function VariableChecker(dc:DialogueChecker) {
			super(dc);
		}
		
		public override function getName():String {
			return "VariableChecker";
		}
		
		public override function checkDialogue(dialogue:Dialogue, options:Options):void {
			var variables:Array = new Array();
			declaredVariables = new Array();
			usedVariables = new Array();
			badVariables = new Array();
			var initialSettingsLines:Array = dialogue.getLinesByName("initial_settings");
			var initialSettingsLine:Line = initialSettingsLines[0];
			if (initialSettingsLine != null) {
				var initialSettingsAttributes:Array = initialSettingsLine.getLineAttributes();
				for (var i:uint = 0, isize:uint = initialSettingsAttributes.length; i < isize; i++) {
					var lineAttribute:LineAttribute = initialSettingsAttributes[i];
					var variableName:String = lineAttribute.getName();
					var variable:Variable = dialogue.getVariable(variableName);
					if (variable != null) {
						if (StringFunctions.isQuoted(variableName)) {
							variables.push(variableName); //Only variables declared in initial_settings are valid... and the custom ones.
							variableName = variableName.substring(1, variableName.length - 1); //stripping ""
							if (variableName.indexOf("*") != -1) {
								var issueMessage:String = "";
								var issue:Issue = null;
								issueMessage = "A variable (" + variableName + ") declared in initial_settings line (line " + initialSettingsLine.getLineNumber() + ") has one or more asterisks in the variable name." + options.getLineBreak();
								issueMessage += "Variables with asterisks in the variable name cannot be inserted in the dialogue." + options.getLineBreak();
								issueMessage += "Line " + initialSettingsLine.getLineNumber() + ": " + initialSettingsLine.getRaw() + options.getLineBreak() + options.getLineBreak();
								issue = new Issue(Severity.MINOR, initialSettingsLine, "", issueMessage, "Variables");
								addIssue(issue);
							}
								//disabled this in v2.15, no idea what I was going for... type-mismatch checking? That's a style issue anyway.
							/*if (variable.getType() == VariableType.getEnum("STRING")) {
							   var initialValue = variable.getInitialValue();
							   var convertedValue = new Number(initialValue);
							   if (!isNaN(convertedValue)) {
							   //Strings and numeric values don't work very well in SDT. Sorry.
							   }
							 }*/
						}
					}
				}
			}
			declaredVariables = variables;
			
			/*
			//Check lines for used variables and what not
			var line:Line = dialogue.getNextLine(0);
			while (line != null) {
				
				scanLine(line, dialogue, options);
				line = dialogue.getNextLine(line.getLineNumber());
			}
			
			//Now use gained info to go over the lines again and throw issues.
			line = dialogue.getNextLine(0);
			while (line != null) {
				checkLine(line, dialogue, options);
				line = dialogue.getNextLine(line.getLineNumber());
			}*/
			
			var lines:Array = dialogue.getLines();
			for each (var line:Line in lines) {
				scanLine(line, dialogue, options);
			}
			for each (var line:Line in lines) {
				checkLine(line, dialogue, options);
			}
		}
		
		private function scanLine(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			//Check for variable insertions
			if (l.getRaw().indexOf("-") == 0 || l.getRaw().indexOf("\"") == -1 || (StringFunctions.nthIndexOf(3, l.getRaw(), " ") != -1 && (StringFunctions.nthIndexOf(3, l.getRaw(), " ") < l.getRaw().indexOf("\"")))) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			var content:Array = l.getContent();
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var contentPiece:Object = content[i];
				if (contentPiece is String) {
					//var contentString:String = contentPiece as String;
				} else if (contentPiece is Trigger) {
					var contentTrigger:Trigger = contentPiece as Trigger;
					if (contentTrigger.containsVariableInsertion()) {
						var vars:Array = getVariablesFromTrigger(contentTrigger);
						var substitutes:Array = ["YOU", "ME", "MY", "FINISHES"]; //Implicit YOUR via YOU
						for (var j:uint = 0, jsize:uint = vars.length; j < jsize; j++) {
							var validVI:Boolean = true;
							for (var k:uint = 0, ksize:uint = substitutes.length; k < ksize; k++) {
								if (vars[j].indexOf(substitutes[k]) != -1) {
									validVI = false;
								}
							}
							if (validVI) {
								checkVariableAsString(vars[j]); //add to used list
							}
						}
						
					}
					if (contentTrigger.getAction().indexOf("VA_SET_") == 0) { //oh joy, a VariableArithmetic set function.
						//Global variables don't need to be declared, so let's not add them if they're only set to.
						if (contentTrigger.getAction().indexOf("VA_SET_VARIABLE_") == 0) { //|| contentTrigger.getAction().indexOf("VA_SET_GLOBALVARIABLE_") == 0) {
							//retrieve variable from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(3, 1); //first tile you'll touch once you've moved 3 steps
							checkVariableAsString(targetVariableName);
						} else if (contentTrigger.getAction().indexOf("VA_SET_VARIABLEBYNAME_") == 0) {
							//Retrieve variables from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(3, 1);
							var sourceVariableName:String = contentTrigger.getArgument(3, 2);
							checkVariableAsString(targetVariableName);
							checkVariableAsString(sourceVariableName);
						} else if (contentTrigger.getAction().indexOf("VA_SET_GLOBALVARIABLEBYNAME_") == 0) {
							//retrieve variable from trigger, add to used list.
							var sourceVariableName:String = contentTrigger.getArgument(3, 2);
							checkVariableAsString(sourceVariableName);
						}
					} else if (contentTrigger.getAction().indexOf("SET") == 0) { //oh joy, a VariableArithmetic set function.
						//Global variables don't need to be declared, so let's not add them if they're only set to.
						if (contentTrigger.getAction().indexOf("SETVAR_") == 0) { //|| contentTrigger.getAction().indexOf("VA_SET_GLOBALVARIABLE_") == 0) {
							//retrieve variable from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(1, 1); //first tile you'll touch once you've moved 3 steps
							checkVariableAsString(targetVariableName);
						} else if (contentTrigger.getAction().indexOf("SETVARBYNAME_") == 0 || contentTrigger.getAction().indexOf("SETVARBYNAEM_") == 0) {
							//Retrieve variables from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(1, 1);
							var sourceVariableName:String = contentTrigger.getArgument(1, 2);
							checkVariableAsString(targetVariableName);
							checkVariableAsString(sourceVariableName);
						} else if (contentTrigger.getAction().indexOf("SETGLOBALBYNAME_") == 0 || contentTrigger.getAction().indexOf("SETGLOBALBYNAEM_") == 0) {
							//retrieve variable from trigger, add to used list.
							var sourceVariableName:String = contentTrigger.getArgument(1, 2);
							checkVariableAsString(sourceVariableName);
						}
					} else if (contentTrigger.getAction().indexOf("APPENDVAR") == 0) {//DAv400 APPENDVAR_var_val / APPENDVARBYNAME_var1_var2 / APPENDVARBYNAEM_var1_var2
						if (contentTrigger.getAction().indexOf("APPENDVAR_") == 0) {//not byname variant
							//retrieve variable from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(1, 1);
							checkVariableAsString(targetVariableName);
						} else if (contentTrigger.getAction().indexOf("APPENDVARBYNAME_") == 0 || contentTrigger.getAction().indexOf("APPENDVARBYNAEM_") == 0) {
							//Retrieve variables from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(1, 1);
							var sourceVariableName:String = contentTrigger.getArgument(1, 2);
							checkVariableAsString(targetVariableName);
							checkVariableAsString(sourceVariableName);
						}
					}
				} else if (contentPiece is VariableInsertion) {
					var contentVariableInsertion:VariableInsertion = contentPiece as VariableInsertion;
					checkVariableInsertion(contentVariableInsertion);
				}
				/*else if (contentPiece is VariableSubstitution) {//not needed yet
				   //var contentVariableSubstitution:VariableSubstitution = contentPiece as VariableSubstitution;
				 }*/
			}
			
			//Check for usage of variables in check or set line-attributes
			checkVariablesInLineAttribute(l.getLineAttribute("\"set\"")); //V2 of the checker keeps the double quotes, so you can see if it was properly parsed.
			checkVariablesInLineAttribute(l.getLineAttribute("\"check\""));
		}
		
		private function checkVariablesInLineAttribute(lineAttribute:LineAttribute):void {
			if (lineAttribute != null) {
				var lineAttributeValues:Array = lineAttribute.getValue() as Array;
				if (lineAttributeValues != null) {
					for (var i:uint = 0, isize:uint = lineAttributeValues.length; i < isize; i++) {
						var VR:VariableReference = lineAttributeValues[i];
						checkVariableAsString(StringFunctions.stripQuotesIfNeeded(VR.getName()));
					}
				}
			}
		}
		
		private function checkLine(l:Line, dialogue:Dialogue = null, options:Options = null):void {
			if (l.getRaw().indexOf("-") == 0 || l.getRaw().indexOf("\"") == -1 || (StringFunctions.nthIndexOf(3, l.getRaw(), " ") != -1 && (StringFunctions.nthIndexOf(3, l.getRaw(), " ") < l.getRaw().indexOf("\"")))) {
				if (options == null || options.getSetting("commentfilter")) {
					return;
				}
			}
			var content:Array = l.getContent();
			//TODO: Check lines for bad variables
			var linebreak:String = Options.DEFAULT_LINEBREAK;
			if (options != null) {
				linebreak = options.getLineBreak();
			}
			var issue:Issue = null;
			var issueMessage:String = "";
			var issueSnippet:String = "";
			for (var i:uint = 0, isize:uint = content.length; i < isize; i++) {
				var contentPiece:Object = content[i];
				if (contentPiece is String) {
					//var contentString:String = contentPiece as String;
					//I don't care about string
				} else if (contentPiece is Trigger) {
					var contentTrigger:Trigger = contentPiece as Trigger;
					if (contentTrigger.containsVariableInsertion()) {
						var vars:Array = getVariablesFromTrigger(contentTrigger);
						var substitutes:Array = ["YOU", "ME", "MY", "FINISHES"]; //Implicit YOUR via YOU
						for (var j:uint = 0, jsize:uint = vars.length; j < jsize; j++) {
							var currentVariable:String = vars[j];
							var validVariableInsertion:Boolean = true;
							for (var k:uint = 0, ksize:uint = substitutes.length; k < ksize && validVariableInsertion; k++) {
								validVariableInsertion = currentVariable.indexOf(substitutes[k]) == -1;
							}
							if (validVariableInsertion && !validateVariableAsString(currentVariable) && (options == null || !options.ignoreVariable(currentVariable))) {
								//Create Issue - Undeclared variable in a variable insertion in a trigger
								issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
								issueMessage = "Undeclared variable (" + currentVariable + ") in a variable insertion in a trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
								addIssue(issue);
								issueMessage = "";
							}
						}
						
					}
					if (contentTrigger.getAction().indexOf("VA_SET_") == 0) { //oh joy, a VariableArithmetic set function
						//Global variables don't need to be declared, so let's not add them if they're only set to.
						var argCount:int = contentTrigger.getArgumentCountBasedOnRawContent(3); //VA(0)_SET(1)_VARIABLE(2)_(3, 1)_(3, 2)
						var argCountEscaped:int = contentTrigger.getArgumentCountBasedOnEscapedContent(3); //VA(0)_SET(1)_VARIABLE(2)_(3, 1)_(3, 2)
						if (argCount != 2) { //TRIGGER ABUSE!
							issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
							issueMessage = "Unexpected amount of arguments for a VariableArithmetic SET Trigger (" + contentTrigger.getLineSegment() + ") - expected 2, got " + argCount + " - on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
							if (argCountEscaped == 2) {
								issueMessage += "This might a false positive caused by having underscores in a variable insertion." + linebreak;
							}
							issueMessage += "Refer to the documentation of DialogueActions/VariableArithmetic for proper usage of this trigger." + linebreak;
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
							issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
							addIssue(issue);
							issueMessage = "";
						}
						if (contentTrigger.getAction().indexOf("VA_SET_VARIABLE_") == 0) { //|| contentTrigger.getAction().indexOf("VA_SET_GLOBALVARIABLE_") == 0) {
							//retrieve variable from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(3, 1); //first tile you'll touch once you've moved 3 steps
							if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
								if (!validateVariableAsString(targetVariableName)) { //is not declared!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Undeclared target variable (" + targetVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} //else: Deal with variables that are insertions. Annoying.
						} else if (contentTrigger.getAction().indexOf("VA_SET_VARIABLEBYNAME_") == 0) {
							//Retrieve variables from trigger, add to used list.
							var targetVariableName:String = contentTrigger.getArgument(3, 1);
							var sourceVariableName:String = contentTrigger.getArgument(3, 2);
							if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
								if (!validateVariableAsString(targetVariableName)) { //is not declared!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Undeclared target variable (" + targetVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} //else: Deal with variables that are insertions. Annoying.
							if (sourceVariableName != null && sourceVariableName.indexOf("*") == -1) {
								if (!validateVariableAsString(sourceVariableName)) { //is not declared!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Undeclared source variable (" + sourceVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Either your source variable is undeclared (bug) or you're creating it with VariableArithmetic later (bad form). Declaring your variable in initial_settings prevents debugging headaches when someone bypasses your variable creation." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} //else: Deal with variables that are insertions. Annoying.
						} else if (contentTrigger.getAction().indexOf("VA_SET_GLOBALVARIABLEBYNAME_") == 0) {
							//retrieve variable from trigger, add to used list.
							var sourceVariableName:String = contentTrigger.getArgument(3, 2);
							if (sourceVariableName != null && sourceVariableName.indexOf("*") == -1) {
								if (!validateVariableAsString(sourceVariableName)) { //is not declared!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Undeclared source variable (" + sourceVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Either your source variable is undeclared (bug) or you're creating it with VariableArithmetic later (bad form). Declaring your variable in initial_settings prevents debugging headaches when someone bypasses your variable creation." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} //else: Deal with variables that are insertions. Annoying.
						}
					} else if (contentTrigger.getAction().indexOf("SET") == 0) { //oh joy, a VariableArithmetic set function
						//Global variables don't need to be declared, so let's not add them if they're only set to.
						var expectedArgs:Array = new Array();
						expectedArgs["SETVAR"] = 2;
						expectedArgs["SETVARBYNAME"] = 2;
						expectedArgs["SETVARBYNAEM"] = 2;
						expectedArgs["SETGLOBAL"] = 2;
						expectedArgs["SETGLOBALS"] = -1;
						var expectedArgCount:* = expectedArgs[contentTrigger.getAction().substring(0, contentTrigger.getAction().indexOf("_"))];//using int here would break the check
						if (expectedArgCount != null) {
							
							var argCount:int = contentTrigger.getArgumentCountBasedOnRawContent(1);
							var argCountEscaped:int = contentTrigger.getArgumentCountBasedOnEscapedContent(1);
							if (expectedArgCount > 0) {
								if (argCount != expectedArgCount) { //TRIGGER ABUSE!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Unexpected amount of arguments for a VariableArithmetic SET Trigger (" + contentTrigger.getLineSegment() + ") - expected " + expectedArgCount + ", got " + argCount + " - on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									if (argCountEscaped == expectedArgCount) {
										issueMessage += "This might a false positive caused by having underscores in a variable insertion." + linebreak;
									}
									issueMessage += "Refer to the documentation of DialogueActions for proper usage of this trigger." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} else {
								if (argCount < (expectedArgCount * -1)) { //TRIGGER ABUSE!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Unexpected amount of arguments for a VariableArithmetic SET Trigger (" + contentTrigger.getLineSegment() + ") - expected at least " + (expectedArgCount * -1) + ", got " + argCount + " - on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Refer to the documentation of DialogueActions for proper usage of this trigger." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							}
							
							if (contentTrigger.getAction().indexOf("SETVAR_") == 0) {
								//retrieve variable from trigger, add to used list.
								var targetVariableName:String = contentTrigger.getArgument(1, 1); //first tile you'll touch once you've moved 3 steps
								if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(targetVariableName) && (options == null || !options.ignoreVariable(targetVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared target variable (" + targetVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
							} else if (contentTrigger.getAction().indexOf("SETVARBYNAME_") == 0 || contentTrigger.getAction().indexOf("SETVARBYNAEM_") == 0) {
								//Retrieve variables from trigger, add to used list.
								var targetVariableName:String = contentTrigger.getArgument(1, 1);
								var sourceVariableName:String = contentTrigger.getArgument(1, 2);
								if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(targetVariableName) && (options == null || !options.ignoreVariable(targetVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared target variable (" + targetVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
								if (sourceVariableName != null && sourceVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(sourceVariableName) && (options == null || !options.ignoreVariable(sourceVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared source variable (" + sourceVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Either your source variable is undeclared (bug) or you're creating it with VariableArithmetic later (bad form). Declaring your variable in initial_settings prevents debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
							} else if (contentTrigger.getAction().indexOf("SETGLOBALBYNAME_") == 0 || contentTrigger.getAction().indexOf("SETGLOBALBYNAEM_") == 0) {
								//retrieve variable from trigger, add to used list.
								var sourceVariableName:String = contentTrigger.getArgument(1, 2);
								if (sourceVariableName != null && sourceVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(sourceVariableName) && (options == null || !options.ignoreVariable(sourceVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared source variable (" + sourceVariableName + ") in a VariableArithmetic SET trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Either your source variable is undeclared (bug) or you're creating it with VariableArithmetic later (bad form). Declaring your variable in initial_settings prevents debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
							}
						}
					} else if (contentTrigger.getAction().indexOf("APPENDVAR") == 0) {//DAv400 APPENDVAR_var_val / APPENDVARBYNAME_var1_var2 / APPENDVARBYNAEM_var1_var2
						var expectedArgs:Array = new Array();
						expectedArgs["APPENDVAR"] = 2;
						expectedArgs["APPENDVARBYNAME"] = 2;
						expectedArgs["APPENDVARBYNAEM"] = 2;
						var expectedArgCount:* = expectedArgs[contentTrigger.getAction().substring(0, contentTrigger.getAction().indexOf("_"))];
						if (expectedArgCount != null) {
							
							var argCount:int = contentTrigger.getArgumentCountBasedOnRawContent(1);
							var argCountEscaped:int = contentTrigger.getArgumentCountBasedOnEscapedContent(1);
							if (expectedArgCount > 0) {
								if (argCount != expectedArgCount) { //TRIGGER ABUSE!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Unexpected amount of arguments for a DialogueActions Trigger (" + contentTrigger.getLineSegment() + ") - expected " + expectedArgCount + ", got " + argCount + " - on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									if (argCountEscaped == expectedArgCount) {
										issueMessage += "This might a false positive caused by having underscores in a variable insertion." + linebreak;
									}
									issueMessage += "Refer to the documentation of DialogueActions for proper usage of this trigger." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							} else {
								if (argCount >= (expectedArgCount * -1)) { //TRIGGER ABUSE!
									issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
									issueMessage = "Unexpected amount of arguments for a DialogueActions Trigger (" + contentTrigger.getLineSegment() + ") - expected " + (expectedArgCount * -1) + ", got " + argCount + " - on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
									issueMessage += "Refer to the documentation of DialogueActions for proper usage of this trigger." + linebreak;
									issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
									issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
									addIssue(issue);
									issueMessage = "";
								}
							}
							
							if (contentTrigger.getAction().indexOf("APPENDVAR_") == 0) {
								
								var targetVariableName:String = contentTrigger.getArgument(1, 1);
								if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(targetVariableName) && (options == null || !options.ignoreVariable(targetVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared target variable (" + targetVariableName + ") in a DialogueActions trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
							} else if (contentTrigger.getAction().indexOf("APPENDVARBYNAME_") == 0 || contentTrigger.getAction().indexOf("APPENDVARBYNAEM_") == 0) {
								var targetVariableName:String = contentTrigger.getArgument(1, 1);
								var sourceVariableName:String = contentTrigger.getArgument(1, 2);
								if (targetVariableName != null && targetVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(targetVariableName) && (options == null || !options.ignoreVariable(targetVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared target variable (" + targetVariableName + ") in a DialogueActions trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Target variables are allowed to be created without declaration, but this is bad form. By declaring them in initial_settings you prevent debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MINOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
								if (sourceVariableName != null && sourceVariableName.indexOf("*") == -1) {
									if (!validateVariableAsString(sourceVariableName) && (options == null || !options.ignoreVariable(sourceVariableName))) { //is not declared!
										issueSnippet = StringFunctions.getSnippet(l.getRaw(), contentTrigger.getLineSegment());
										issueMessage = "Undeclared source variable (" + sourceVariableName + ") in a DialogueActions trigger (" + contentTrigger.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(contentTrigger.getLineSegment()) + ", near \"" + issueSnippet + "\"." + linebreak;
										issueMessage += "Either your source variable is undeclared (bug) or you're creating it with triggers at runtime (bad form if you already know the name beforehand). Declaring your variable in initial_settings prevents debugging headaches when someone bypasses your variable creation." + linebreak;
										issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
										issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
										addIssue(issue);
										issueMessage = "";
									}
								} //else: Deal with variables that are insertions. Annoying.
							}
						}
					}
				} else if (contentPiece is VariableInsertion) {
					var contentVariableInsertion:VariableInsertion = contentPiece as VariableInsertion;
					if (!validateVariableAsString(contentVariableInsertion.getContent()) && (options == null || !options.ignoreVariable(contentVariableInsertion.getVariable().getName()))) {
						//Create Issue - Undeclared variable in variable insertion
						var insertionInfo:Array = getInsertionInfo(l.getRaw(), contentVariableInsertion);
						issueSnippet = StringFunctions.getSnippet(l.getRaw(), insertionInfo[1]);
						issueMessage = "Undeclared variable (" + contentVariableInsertion.getVariable().getName() + ") in a variable insertion (" + contentVariableInsertion.getLineSegment() + ") on line " + l.getLineNumber() + ", col " + insertionInfo[0] + ", near \"" + issueSnippet + "\"." + linebreak;
						if (l.getRaw().indexOf("%2A") != -1) {
							issueMessage += "Using %2A to circumvent SDT from parsing a variable is not possible. Consider defining a variable with an asterisk as its value instead." + linebreak;
						}
						if (contentVariableInsertion.getLineSegment() == "**") {
							issueMessage += "This seems to be an empty variable - if it is a variable. Keep in mind it's not possible to nest variables." + linebreak;
						}
						issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
						addIssue(issue);
						issueMessage = "";
					}
				} else if (contentPiece is VariableSubstitution) {
					//var contentVariableSubstitution:VariableSubstitution = contentPiece as VariableSubstitution;
					//Is a var, but is not the vars we're looking for.
				}
			}
			if (l.getLineName() != "initial_settings") {
				var setAttribute:LineAttribute = l.getLineAttribute("\"set\""); //V2 of the checker keeps the double quotes, so you can see if it was properly parsed.
				var checkAttribute:LineAttribute = l.getLineAttribute("\"check\"");
				if (setAttribute != null) {
					var setAttributeValues:Array = setAttribute.getValue() as Array;
					if (setAttributeValues != null) {
						for (var i:uint = 0, isize:uint = setAttributeValues.length; i < isize; i++) {
							var VR:VariableReference = setAttributeValues[i];
							var varName:String = StringFunctions.stripQuotesIfNeeded(VR.getName());
							if (!validateVariableAsString(varName) && (options == null || !options.ignoreVariable(varName))) {
								//Create Issue - Undeclared variable in Set line-attribute
								issueSnippet = StringFunctions.getSnippet(l.getRaw(), varName, l.getRaw().indexOf("\"set\""));
								issueMessage = "Undeclared variable (" + varName + ") in line-attribute set on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(varName, l.getRaw().indexOf("\"set\"")) + ", near \"" + issueSnippet + "\"." + linebreak;
								issueMessage += "To fix this, add the variable to the initial_settings line of your dialogue." + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
								addIssue(issue);
								issueMessage = "";
							}
						}
					}
				}
				if (checkAttribute != null) {
					var checkAttributeValues:Array = checkAttribute.getValue() as Array;
					if (checkAttributeValues != null) {
						for (var i:uint = 0, isize:uint = checkAttributeValues.length; i < isize; i++) {
							var VR:VariableReference = checkAttributeValues[i];
							var varName:String = StringFunctions.stripQuotesIfNeeded(VR.getName());
							if (!validateVariableAsString(varName) && (options == null || !options.ignoreVariable(varName))) {
								//Create Issue - Undeclared variable in Check line-attribute
								issueSnippet = StringFunctions.getSnippet(l.getRaw(), varName, l.getRaw().indexOf("\"check\""));
								issueMessage = "Undeclared variable (" + varName + ") in line-attribute check on line " + l.getLineNumber() + ", col " + l.getRaw().indexOf(varName, l.getRaw().indexOf("\"check\"")) + ", near \"" + issueSnippet + "\"." + linebreak;
								issueMessage += "To fix this, add the variable to the initial_settings line of your dialogue." + linebreak;
								issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
								issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
								addIssue(issue);
								issueMessage = "";
							}
						}
					}
				}
			} else {
				var initialSettingsAttributes:Array = l.getLineAttributes();
				for (var i:uint = 0, isize:uint = initialSettingsAttributes.length; i < isize; i++) {
					var lineAttribute:LineAttribute = initialSettingsAttributes[i];
					var variableName:String = lineAttribute.getName();
					//var variable:Variable = dialogue.getVariable(variableName);
					if (usedVariables.indexOf(variableName) == -1 && (options == null || !options.ignoreVariable(variableName))) {
						//Create issue - Unused variable!
						issueSnippet = StringFunctions.getSnippet(l.getRaw(), variableName + ":");
						issueMessage = "Unused variable (" + variableName + ") in initial_settings line, linenumber " + l.getLineNumber() + ", col " + l.getRaw().indexOf(variableName + ":") + ", near \"" + issueSnippet + "\"." + linebreak;
						if (l.getRaw().length > 500 && !options.getSetting("NoLineTruncation")) {
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw().substr(0, 499) + " ... truncated." + linebreak + "Add #DialogueChecker-NoLineTruncation# at the top of your dialogue to prevent this (may crash checker with really long lines)." + linebreak + linebreak;
						} else {
							issueMessage += "Line " + l.getLineNumber() + ": " + l.getRaw() + linebreak + linebreak;
						}
						
						issue = new Issue(Severity.MAJOR, l, issueSnippet, issueMessage, "Variables");
						addIssue(issue);
						issueMessage = "";
					}
				}
			}
		}
		
		private function getVariablesFromTrigger(t:Trigger):Array {
			var triggercontent:String = t.getLineSegment();
			triggercontent = triggercontent.substring(1, triggercontent.length - 1);
			var inAsterisks:Boolean = false;
			var vars:Array = new Array();
			while (triggercontent != "") {
				if (triggercontent.indexOf("*") != -1) {
					if (inAsterisks) {
						vars.push(triggercontent.substr(0, triggercontent.indexOf("*")));
					}
					triggercontent = triggercontent.substr(triggercontent.indexOf("*") + 1);
					inAsterisks = !inAsterisks;
				} else {
					triggercontent = "";
				}
			}
			return vars;
		}
		
		private function checkVariable(v:Variable):Boolean {
			return checkVariableAsString(v.getName());
		}
		
		/**
		 * Adds variable to used variables list if not in used variables list already. Checks if variable is declared.
		 * @param	v Variable's name.
		 * @return true if variable is declared or provided by mod
		 */
		private function checkVariableAsString(v:String):Boolean {
			if (v == null) {
				return false;
			}
			var quotedVariableName:String = StringFunctions.addQuotes(v);
			ArrayFunctions.addIfNotExists(usedVariables, quotedVariableName);
			
			if (declaredVariables.indexOf(quotedVariableName) != -1) {
				return true;
			} else {
				return Data.isModVariable(v);
			}
		}
		
		private function checkVariableInsertion(v:VariableInsertion):Boolean {
			return checkVariableAsString(v.getContent());
		}
		
		private function validateVariable(v:Variable):Boolean {
			return validateVariableAsString(v.getName());
		}
		
		private function validateVariableAsString(v:String):Boolean {
			var stringQuationSpecialCharacters:Array = new Array("+", "-", "*", "%", "\\", "/", "!", "=", ">", "<", "|", "&", "(", ")");
			for (var i:uint = 0, isize:uint = stringQuationSpecialCharacters.length; i < isize; i++) {
				if (v.indexOf(stringQuationSpecialCharacters[i]) != -1) {
					return true; //It's possibly a special variable.
				}
			}
			
			var quotedVariableName:String = StringFunctions.addQuotes(v);
			
			if (usedVariables.indexOf(quotedVariableName) == -1) {
				return false; //Unused variable. This can only happen in initial_settings line. All other variables have been added, anyway.
			}
			if (declaredVariables.indexOf(quotedVariableName) != -1) {
				return true; //A variable is used, and it's declared. Validated.
			}
			if (Data.isModVariable(v)) { //If the variable is used in a mod, it's valid. If it's not used in a mod, it's not declared anywhere - thus a mistake was made. Or the checker needs updating.
				addDependencyForVariable(v);
				return true;
			}
			return false; //not special, is used, not declared, not a mod variable. That's a mistake.
		}
		
		private function addDependencyForVariable(v:String):void {
			var dependency:String = Data.getDependencyOfModVariable(v);
			if (dependency != ""){
				dialogueChecker.addVariableDependency(dependency, v);
			}
		}
		
		private function getInsertionInfo(line:String, variableInsertion:VariableInsertion):Array {
			var a:Array = new Array();
			var variableName:String = variableInsertion.getContent();
			var astast:int = line.indexOf("*" + variableName + "*");
			var perc2aperc2a:int = line.indexOf("%2A" + variableName + "%2A");
			var astperc2a:int = line.indexOf("*" + variableName + "%2A");
			var perc2aast:int = line.indexOf("%2A" + variableName + "*");
			var index:int = Math.max(astast, perc2aperc2a, perc2aast, astperc2a);
			a[0] = index;
			if (astast == index) {
				a[1] = "*" + variableName + "*";
			}
			if (perc2aperc2a == index) {
				a[1] = "%2A" + variableName + "%2A";
			}
			if (astperc2a == index) {
				a[1] = "*" + variableName + "%2A";
			}
			if (perc2aast == index) {
				a[1] = "%2A" + variableName + "*";
			}
			return a;
		}
		
		private function findInsertionIndex(line:String, variableInsertion:VariableInsertion):int {
			var variableName:String = variableInsertion.getContent();
			var astast:int = line.indexOf("*" + variableName + "*");
			var perc2aperc2a:int = line.indexOf("%2A" + variableName + "%2A");
			var astperc2a:int = line.indexOf("*" + variableName + "%2A");
			var perc2aast:int = line.indexOf("%2A" + variableName + "*");
			return Math.max(astast, perc2aperc2a, perc2aast, astperc2a);
		}
	}

}