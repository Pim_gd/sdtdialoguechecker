package {
	/**
	 * The Severity enumeration is used to determine the severity of an Issue.
	 * There are three levels/categories of severity:
	 * 	 - SEVERE, for instances in which the issue has to be fixed, or the Dialogue will cause SDT to crash or otherwise experience uncaught exceptions;
	 * 	 - MAJOR, for instances in which the Dialogue does not display as intended.
	 * 		Since the DialogueChecker does not have the ability to read the intention of the author of a dialogue, the DialogueChecker assumes that:
	 * 			- All lines in a dialogue that are not commented out were meant to be played. (Unused lines are bad)
	 *			- All content in a line of a dialogue was meant to be displayed. (Failed substitutes are bad)
	 *			- All lines in a dialogue were meant to function correctly within SDT. (SDT should not fail to parse any lines)
	 *		In the event that the application of these assumptions to a dialogue fails,
	 * 		the cause of the failure will be labeled an issue of MAJOR severity, unless the cause is already labeled as a SEVERE issue.
	 *	 - MINOR, for instances in which the issue does not entail an error or meets the requirements for SEVERE or MAJOR severity.
	 *		Issues with a MINOR severity tend to entail grammatical mistakes, unnecessary sections of a dialogue or line, or syntactic styling of dialogue lines.
	 * These three levels can each be seen as MUST (Severe), SHOULD (Major) and COULD (Minor) - "be fixed".
	 * In case the DialogueChecker is unable to parse a dialogue or sections of a Dialogue, issues are automatically treated as SEVERE, due to the possibility of the affected sections to contain SEVERE issues.
	 * @author Pimgd
	 */
	public class Severity extends Enum {
		private static const enums:Array = Enum.loadValuesForEnum(Severity);
		public static const SEVERE:Severity = enums["SEVERE"];
		public static const MAJOR:Severity = enums["MAJOR"];
		public static const MINOR:Severity = enums["MINOR"];
		
		public function Severity(val:uint, ID:String) {
			super(val, ID);
		}
		
		public static function getAllEnums():Array {
			var tempArray:Array = new Array();
			for each (var severity:Severity in enums) {
				tempArray[severity.getValue()] = severity;
			}
			return tempArray;
		}
		
		public function getIDStandardCapitalization():String {
			var ownID:String = getID();
			ownID = ownID.toLowerCase();
			ownID = ownID.charAt(0).toUpperCase() + ownID.substr(1);
			return ownID;
		}
	}

}