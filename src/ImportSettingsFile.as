package 
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class ImportSettingsFile 
	{
		private var author:String;
		private var version:uint;
		private var mod:String;
		
		private var variables:Array;
		private var simpleTriggers:Array;
		private var complexTriggers:Array;
		private var lines:Array;
		
		public function ImportSettingsFile(parsedJsonObject:Object) {
			author = throwIfUndefined(parsedJsonObject.author, "No \"author\" attribute set for importSettingsFile");
			version = throwIfUndefined(parsedJsonObject.version, "No \"version\" attribute set for importSettingsFile - this checker supports version 1 only");
			mod = throwIfUndefined(parsedJsonObject.mod, "No \"mod\" attribute set for importSettingsFile - what is the file supposed to support?");
			variables = parsedJsonObject.variables;
			simpleTriggers = parsedJsonObject.simpleTriggers;
			complexTriggers = parsedJsonObject.complexTriggers;
			if (version >= 2) {
				lines = parsedJsonObject.lines;
			}
			else
			{
				lines = new Array();
			}
		}
		
		private function throwIfUndefined(input:*, errorMessage:String = "One of the settings in an import settings file is undefined."):* {
			if (input == undefined){
				throw new Error(errorMessage);
			}
			return input;
		}
		
		/**
		 * Called "blindly" because it doesn't check for conflicts, duplicates or impossible values (if those existed).
		 * Basically, no input validation yet.
		 */
		public function applySettingsBlindly():void {
			for each (var value:String in variables) {
				Data.addModVariable(value, mod);
			}
			
			for each (var simpleTriggerKey:String in simpleTriggers) {
				TriggerType.registerSimpleTriggerMapping(simpleTriggerKey, mod);
			}
			
			for each (var complexTriggerKey:String in complexTriggers) {
				TriggerType.registerComplexTriggerMapping(complexTriggerKey, mod);
			}
			
			for each (var lineName:String in lines) {
				Data.addCustomLine(lineName, mod);
			}
		}
		
		public function getAuthor():String {
			return author;
		}
		
		public function getVersion():uint {
			return version;
		}
		
		public function getMod():String {
			return mod;
		}
		
		public function printDetails(options:Options):String {
			return mod + " by " + author + " - import file version "+version;
		}
		
	}

}