Build Release Procedure for DialogueChecker
1: Remove "dev" from version number in Main.as.
2: Run the DialogueChecker and verify that it works to some degree (plonk in a couple dialogues)
3: Go to the bin folder and duplicate the compiled SWF
4: Rename the copy to "SDTDialogueCheckerAS3<Version>.swf"
5: Go to FastSWF and upload the new version of the DialogueChecker. Name is "SDTDialogueChecker <Version>"
6: View the uploaded SWF and bookmark it in the SDT bookmark folder
7: Copy the SWF to Packaging Station, and add the new Documentation in there as well.
8: Make a .zip for uploading.
9: Upload the .zip to Bitbucket.
10: Remove the dev version from Bitbucket, if any.
11: Go to DialogueChecker thread TWICE (one to modify, the other to reference posts)
12: Modify OP of topic.
13: Change Subject to current date and new version.
14: Change version number to new version.
15: Change FastSWF link.
16: Uncheck old attachment and select new one.
17: Update "things that the checker checks"
18: Update ideaslist
19: Publish the post
20: Make a new post announcing that the new version is here, with the features
21: Commit version number change, tag commit with version number.
22: Go to Main.as, up the version number by 1, add "dev" to version number.
23: Commit that as well, so any new commits and builds will be dev builds again.

