For licensing see License.txt.

This is a Dialogue Checker for SuperDeepThroat (NSFW). 
SDT is a flash game that contains, amongst other things, the functionality allowing a user to create their own dialogues. 
This DialogueChecker attempts to find scripting mistakes in user-made dialogues in order to try and raise the overall standard of community-produced dialogues.

In this root:
- Documentation 
   contains a few .txt files containing dialogue writer and modder documentation.
   Also contains UML files (created with WhiteStarUML) created when I designed the structure of the code.
- src
   sources. Also a wayward variableparser.txt... I used it to get all the variables from DialogueActions.

On the Downloads page, you can find old sources and builds for v1.xx and v2.xx versions.